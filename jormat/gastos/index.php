<?php
include ("../conectar.php");

$cadena_busqueda=$_GET["cadena_busqueda"];

if (!isset($cadena_busqueda)) { $cadena_busqueda=""; } else { $cadena_busqueda=str_replace("",",",$cadena_busqueda); }

if ($cadena_busqueda<>"") {
	$array_cadena_busqueda=split("~",$cadena_busqueda);
	$codubicacion=$array_cadena_busqueda[1];
	$nombre=$array_cadena_busqueda[2];
} else {
	$id="";
	$descripcion="";
}

?>
<html>
	<head>
		<title>Familias</title>
		<link href="../estilos/estilos.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		
		var cursor;
		if (document.all) {
		// Está utilizando EXPLORER
		cursor='hand';
		} else {
		// Está utilizando MOZILLA/NETSCAPE
		cursor='pointer';
		}
		
		function inicio() {
			document.getElementById("form_busqueda").submit();
		}
		function nuevo_gasto() {
			location.href="nueva_gasto.php";
		}
		
		function limpiar_busqueda() {
			document.getElementById("descripcion").value="";
		}
		
		
		
		function buscar() {
			var cadena;
			cadena=hacer_cadena_busqueda();
			document.getElementById("cadena_busqueda").value=cadena;
			if (document.getElementById("iniciopagina").value=="") {
				document.getElementById("iniciopagina").value=1;
			} else {
				document.getElementById("iniciopagina").value=document.getElementById("paginas").value;
			}
			document.getElementById("form_busqueda").submit();
		}
		
		function paginar() {
			document.getElementById("iniciopagina").value=document.getElementById("paginas").value;
			document.getElementById("form_busqueda").submit();
		}
		
		function hacer_cadena_busqueda() {
			var codubicacion=document.getElementById("id").value;
			var nombre=document.getElementById("descripcion").value;
			var cadena="";
			cadena="~"+id+"~"+descripcion+"~";
			return cadena;
			}
		</script>
	</head>
	<body onLoad="inicio()">
		<div id="pagina">
			<div id="zonaContenido">
				<div align="center">
				<div id="tituloForm" class="header">Buscar GASTO </div>
				<div id="frmBusqueda">
				<form id="form_busqueda" name="form_busqueda" method="post" action="rejilla.php" target="frame_rejilla">
					<table class="fuente8" width="98%" cellspacing=0 cellpadding=3 border=0>					
						<tr>
							<td width="16%" class="Menu">Descripcion</td>
							<td width="68%"><input id="descripcion" name="descripcion" type="text" class="cajaGrande" maxlength="50" value="<?php echo $descripcion?>"></td>
							<td width="5%">&nbsp;</td>
							<td width="5%">&nbsp;</td>
						</tr>
					</table>
			  </div>
			 	<div id="botonBusqueda">
					<table width="357">
                      <tr align="right" >
                        <td width="140" height="57"><input name="submit" type="submit" id="registrar" title="Buscar" onClick="buscar()" onMouseOver="style.cursor=cursor" value="Buscar"/></td>
                        <td width="61"><input name="submit" type="submit" id="registrar" title="Limpiar" onClick="limpiar_busqueda()" onMouseOver="style.cursor=cursor" value="Limpiar"/></td>
                        <td width="61"><input name="submit" type="submit" id="registrar" title="Nuevo" onClick="nuevo_gasto()" onMouseOver="style.cursor=cursor" value="Nuevo"/></td>
                        <td width="75">&nbsp;</td>
                      </tr>
                    </table>
				  </div>
			  <div id="lineaResultado">
			  <table class="fuente8" width="80%" cellspacing=0 cellpadding=3 border=0>
			  	<tr>
				<td width="50%" align="left" class="Menu">encontradas 
				  <input id="filas" type="text" class="cajaPequena" NAME="filas" maxlength="5" readonly></td>
				<td width="50%" align="right"><select name="paginas" id="paginas" class="comboPequeno" onChange="paginar()">
		          </select></td>
			  </table>
				</div>
			   <div id="cabeceraResultado" class="header">
					 GASTOS </div>
				<div id="frmResultado">
				<table class="fuente8" width="100%" cellspacing=0 cellpadding=3 border=0 ID="Table1">
						<tr class="cabeceraTabla">
							<td width="8%"></td>
							<td width="26%">DESCRIPCION</td>
							<td width="42%">FECHA </td>
							<td width="20%">VALOR</td>
							<td width="2%">&nbsp;</td>
							<td width="2%">&nbsp;</td>
						</tr>
				</table>
				</div>
				<input type="hidden" id="iniciopagina" name="iniciopagina">
				<input type="hidden" id="cadena_busqueda" name="cadena_busqueda">
			</form>
				<div id="lineaResultado">
					<iframe width="100%" height="250" id="frame_rejilla" name="frame_rejilla" frameborder="0">
						<ilayer width="100%" height="250" id="frame_rejilla" name="frame_rejilla"></ilayer>
					</iframe>
				</div>
			</div>
		  </div>			
		</div>
	</body>
</html>
