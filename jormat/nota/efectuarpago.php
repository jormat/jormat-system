<?php
session_start();
if($_SESSION["usuario"]){
?>
<html>
<head>
<title>Pago Mostrador Venta</title>
<link href="../estilos/estilos.css" type="text/css" rel="stylesheet">
<link href="../calendario/calendar-blue.css" rel="stylesheet" type="text/css">
<script type="text/JavaScript" language="javascript" src="../calendario/calendar.js"></script>
<script type="text/JavaScript" language="javascript" src="../calendario/lang/calendar-sp.js"></script>
<script type="text/JavaScript" language="javascript" src="../calendario/calendar-setup.js"></script>

<script>

var cursor;
		if (document.all) {
		// Está utilizando EXPLORER
		cursor='hand';
		} else {
		// Está utilizando MOZILLA/NETSCAPE
		cursor='pointer';
		}
		
function actualizarimporte() {
	var importe=document.getElementById("importe").value;
	var importevale=document.getElementById("importevale").value;
	if( importevale.search('[^0-9.]') == -1 ) 
		{
			var resta=parseInt(importe-importevale);
			var valor=Math.round(resta*100)/100 ;
			document.getElementById("apagar").value=valor;
		} else {
			alert ("El total del vale no es correcto.");
		}		
}

function actualizarimportedevolver() {
	var importe=document.getElementById("importe").value;
	var pagado=document.getElementById("pagado").value;
	if( pagado.search('[^0-9.]') == -1 ) 
		{
			var resta=parseInt(pagado-importe);
			var valor=Math.round(resta*100)/100 ;
			document.getElementById("adevolver").value=valor;
		} else {
			alert ("El total pagado no es correcto.");
		}		
}

function imprimir(codfactura) {
	var pagado=document.getElementById("pagado").value;
	var adevolver=document.getElementById("adevolver").value;
	location.href="../fpdf/imprimir_ticket_html.php?codfactura=" + codfactura + "&pagado=" + pagado + "&adevolver=" + adevolver;
}

function enviar() {
	document.getElementById("formulario").submit();
}
</script>
</head>
<?php include ("../conectar.php"); 

$codfactura=$_GET["codfactura"];
$codcliente=$_GET["codcliente"];
$todo=$_GET["importe"];
$importe=str_replace(',','',$todo);

$sel_clientes="SELECT nombre FROM clientes WHERE codcliente='$codcliente'";
$rs_clientes=mysql_query($sel_clientes);
$nombre_cliente=mysql_result($rs_clientes,0,"nombre");

?>
<body>
<div id="pagina">
<div id="zonaContenido">
<div id="tituloForm2" class="header">COBRO </div>
<div id="frmBusqueda2">
<div align="center">
<form id="formulario" name="formulario" method="post" action="guardar_cobro.php" target="frame_datos">
					<table class="fuente8" width="98%" cellspacing=0 cellpadding=2 border=0>
						<tr>
							<td width="110" class="Menu">C&oacute;digo Factura </td>
				        	<td width="1593"><input NAME="codfactura" type="text" class="cajaPequena" id="codfactura" size="15" maxlength="15" value="<?php echo $codfactura?>" readonly>						</tr>
						<tr>
							<td class="Menu">Nombre Cliente</td>
						    <td><input NAME="nombre_cliente" type="text" class="cajaGrande" id="nombre_cliente" size="45" maxlength="45" value="<?php echo $nombre_cliente?>" readonly></td>
			            </tr>
						<tr>
							<td class="Menu">Total</td>
						    <td class="Menu2"><input NAME="importe" type="text" disabled class="cajaPequena" id="importe"  value="<?php echo $importe?>" readonly> 
						      $</td>
			            </tr>
						<tr>
							<td class="Menu">Total a pagar</td>
						    <td class="Menu2"><input NAME="apagar" type="text" class="cajaPequena" id="apagar"  value="<?php echo $importe?>"> 
						      $</td>
			            </tr>
						<tr>
							<td class="Menu">Efectivo</td>
						    <td class="Menu2"><input NAME="pagado" type="text" class="cajaPequena" id="pagado" size="10" maxlength="10"> 
						      $<img src="../img/notification_done.png" name="Image2" id="Image2" width="20" height="20" border="0"  onMouseOver="this.style.cursor='pointer'" title="Cancelar" onClick="actualizarimportedevolver()"></td>
			            </tr>
						<tr>
							<td class="Menu">vuelto</td>
						    <td class="Menu2"><input NAME="adevolver" type="text" class="cajaPequena" id="adevolver" size="10" maxlength="10" readonly> 
						      $</td>
			            </tr>
						<?php
						$query_fp="SELECT * FROM formapago WHERE borrado=0 ORDER BY nombrefp ASC";
						$res_fp=mysql_query($query_fp);
						$contador=0; ?>
						<tr>
							<td class="Menu">Forma de pago</td>
						    <td><select id="formapago" name="formapago" class="comboGrande">
								<?php
								while ($contador < mysql_num_rows($res_fp)) { 
									if (mysql_result($res_fp,$contador,"codformapago") ==1) { ?>
								<option value="<?php echo mysql_result($res_fp,$contador,"codformapago")?>" selected="selected"><?php echo mysql_result($res_fp,$contador,"nombrefp")?></option> 
								<?php } else { ?>
								<option value="<?php echo mysql_result($res_fp,$contador,"codformapago")?>"><?php echo mysql_result($res_fp,$contador,"nombrefp")?></option>
								<?php 
									}
									$contador++;
								} ?>				
								</select></td>
			            </tr>
						<?php $hoy=date("d/m/Y"); ?>
						<tr>
							<td class="Menu">Fecha de pago</td>
						    <td><input NAME="fechacobro" type="text" class="cajaPequena" id="fechacobro" size="10" maxlength="10" value="<?php echo $hoy?>" readonly> <img src="../img/calendario.png" name="Image1" id="Image1" width="16" height="16" border="0"  onMouseOver="this.style.cursor='pointer'">
        <script type="text/javascript">
					Calendar.setup(
					  {
					inputField : "fechacobro",
					ifFormat   : "%d/%m/%Y",
					button     : "Image1"
					  }
					);
		</script></td>
			            </tr>
					</table>										
			  </div>
  <br><br>
			  <div align="center">
			  <img src="../img/notification_done.png" width="62" height="50" onClick="enviar()" title="Aceptar" onMouseOver="style.cursor=cursor"><img src="../img/notification_error.png" width="62" height="50" onClick="window.close()" title="Cancelar" onMouseOver="style.cursor=cursor">
		  </div>
			  <input id="codfactura" name="codfactura" value="<?php echo $codfactura?>" type="hidden">
			  <input id="codcliente" name="codcliente" value="<?php echo $codcliente?>" type="hidden">
			  <input id="importe" name="importe" value="<?php echo $importe?>" type="hidden">
</form>
			  </div>
			  </div>
			  </div>
			  </div>
<iframe id="frame_datos" name="frame_datos" width="0%" height="0" frameborder="0">
	<ilayer width="0" height="0" id="frame_datos" name="frame_datos"></ilayer>
</iframe>
</body>
</html>
<?php
}else
{
	echo "<script type='text/javascript'>
		alert('Usted no tiene permiso de administrador');
		window.location='../index.html';
	</script>";
}
?>