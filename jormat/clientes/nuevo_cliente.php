<?php 

session_start();
if($_SESSION["usuario"]){

include ("../conectar.php"); ?>
<html>
	<head>
		<title>Principal</title>
		<link href="../estilos/estilos.css" type="text/css" rel="stylesheet">
        <link href="style.css" media="screen" rel="stylesheet" type="text/css" />
<link href="flora.css" media="screen" rel="stylesheet" type="text/css" />
<link href="flora_tabs.css" media="screen" rel="stylesheet" type="text/css" />

<script src="../scripts/jquery.js" type="text/javascript"></script>
<script src="../scripts/jquery_rut.js" type="text/javascript"></script>
<script src="../scripts/jquery_validate.js" type="text/javascript"></script>
<script src="../scripts/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">
	
        
    
	
    
function cancelar() {
			location.href="index.php";
		}

		
		var cursor;
		if (document.all) {
		// Está utilizando EXPLORER
		cursor='hand';
		} else {
		// Está utilizando MOZILLA/NETSCAPE
		cursor='pointer';
		}
		
		function limpiar() {
			document.getElementById("formulario").reset();
		}
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' El valor debe ser numerico .\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' es Necesario\n'; }
    } if (errors) alert('Complete Campos Obligatorios :\n\n'+errors);
    document.MM_returnValue = (errors == '');
} }

        </script>
        <script type="text/javascript">
        $(document).ready(function(){
// Demo 1

$('#Rut').Rut({ 
  on_error: function(){ alert('Rut incorrecto'); } 
});

$("#content > ul").tabs();
});
</script>
	</head>
	<body>
		<div id="pagina">
			<div id="zonaContenido">
				<div align="center">
				<div id="tituloForm" class="header">INSERTAR CLIENTE </div>
				<div id="frmBusqueda">
				<form id="formulario" name="formulario" method="post" action="guardar_cliente.php">
					<table class="fuente8" width="98%" cellspacing=0 cellpadding=3 border=0>
						<tr>
							<td width="13%" class="Menu">Nombre</td>
						    <td><input NAME="Nombre" type="text" class="cajaGrande" id="nombre" size="45" maxlength="45"></td>
					        <td width="39%" rowspan="11" align="right" valign="top"><ul id="lista-errores"></ul>
				            <img src="../img/agregar.png" width="148" height="139"></td>
						</tr>
						<tr>
						  <td class="Menu">Rut</td>
  
						  <td>
                          
                          <input id="Rut" type="text" class="cajaPequena" NAME="Rut" maxlength="15"></td>
					  </tr>
						<tr>
						  <td class="Menu">Direcci&oacute;n</td>
						  <td><input NAME="adireccion" type="text" class="cajaGrande" id="direccion" size="45" maxlength="45"></td>
				      </tr>
						<tr>
						  <td class="Menu">Ciudad</td>
						  <td><input NAME="alocalidad" type="text" class="cajaGrande" id="localidad" size="35" maxlength="35"></td>
				      </tr>
					  <?php
					  	$query_provincias="SELECT * FROM provincias ORDER BY nombreprovincia ASC";
						$res_provincias=mysql_query($query_provincias);
						$contador=0;
					  ?>
						<tr>
							<td width="13%" class="Menu">Region</td>
							<td><select id="cboProvincias" name="cboProvincias" class="comboGrande">
							<option value="0">Seleccione una provincia</option>
								<?php
								while ($contador < mysql_num_rows($res_provincias)) { ?>
								<option value="<?php echo mysql_result($res_provincias,$contador,"codprovincia")?>"><?php echo mysql_result($res_provincias,$contador,"nombreprovincia")?></option>
								<?php $contador++;
								} ?>				
								</select>							</td>
				        </tr>
						<?php
					  	$query_formapago="SELECT * FROM formapago WHERE borrado=0 ORDER BY nombrefp ASC";
						$res_formapago=mysql_query($query_formapago);
						$contador=0;
					  ?>
						<?php
					  	$query_entidades="SELECT * FROM entidades WHERE borrado=0 ORDER BY nombreentidad ASC";
						$res_entidades=mysql_query($query_entidades);
						$contador=0;
					  ?>
						<tr>
							<td class="Menu">Giro </td>
							<td><input id="codpostal" type="text" class="cajaPequena" NAME="acodpostal" ></td>
					    </tr>
						<tr>
							<td class="Menu">Telefono</td>
							<td><input id="telefono" name="atelefono" type="text" class="cajaPequena" ></td>
					    </tr>
						<tr>
							<td class="Menu">Empresa </td>
							<td><input id="movil" name="amovil" type="text" class="cajaPequena" ></td>
					    </tr>
						<tr>
							<td class="Menu">Correo electr&oacute;nico  </td>
							<td><input NAME="aemail" type="text" class="cajaGrande" id="email" size="35" maxlength="35"></td>
					    </tr>
												<tr>
							<td class="Menu">Direcci&oacute;n web </td>
							<td><input NAME="aweb" type="text" class="cajaGrande" id="web" size="45" maxlength="45"></td>
					    </tr>
					</table>
			  </div>
<div id="botonBusqueda">
  <label>
    <input title="Aceptar" alt=" conton comprar " src="../img/notification_done.png" type="image" onClick="MM_validateForm('nombre','','R','Rut','','R');return document.MM_returnValue" value="Enviar" width="62" height="50"/>
  </label>
  <img src="../img/file.png" width="62" height="50" onClick="limpiar()" title="Limpiar" onMouseOver="style.cursor=cursor">
					<img src="../img/notification_error.png" width="62" height="50" onClick="cancelar()" title="Cancelar" onMouseOver="style.cursor=cursor">
<input id="accion" name="accion" value="alta" type="hidden">
					<input id="id" name="Zid" value="" type="hidden">
			  </div>
			  </form>
			  </div>
		  </div>
		</div>
	</body>
</html>
<?php
}else
{
	echo "<script type='text/javascript'>
		alert('Usted no tiene permiso de administrador');
		window.location='../index.html';
	</script>";
}
?>
