<?php

session_start();
if($_SESSION["usuario"]){
include ("../conectar.php");
if (isset($_GET["cadena_busqueda"]))
$cadena_busqueda=$_GET["cadena_busqueda"];

if (!isset($cadena_busqueda)) { $cadena_busqueda=""; } else { $cadena_busqueda=str_replace("",",",$cadena_busqueda); }

if ($cadena_busqueda<>"") {
	$array_cadena_busqueda=split("~",$cadena_busqueda);
	$codfamilia=$array_cadena_busqueda[1];
	$nombre=$array_cadena_busqueda[2];
} else {
	$codfamilia="";
	$nombre="";
}

?>
<html>
	<head>
		<title>Sectores</title>
		<link href="../estilos/estilos.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		
		var cursor;
		if (document.all) {
		// Está utilizando EXPLORER
		cursor='hand';
		} else {
		// Está utilizando MOZILLA/NETSCAPE
		cursor='pointer';
		}
		
		function inicio() {
			document.getElementById("form_busqueda").submit();
		}
		
		function nueva_familia() {
			location.href="nueva_familia.php";
		}

		function imprimir() {
			var codfamilia=document.getElementById("codfamilia").value;
			var nombre=document.getElementById("nombre").value;
			window.open("../fpdf/familias.php?codfamilia="+codfamilia+"&nombre="+nombre);
		}
		function buscar() {
			var cadena;
			cadena=hacer_cadena_busqueda();
			document.getElementById("cadena_busqueda").value=cadena;
			if (document.getElementById("iniciopagina").value=="") {
				document.getElementById("iniciopagina").value=1;
			} else {
				document.getElementById("iniciopagina").value=document.getElementById("paginas").value;
			}
			document.getElementById("form_busqueda").submit();
		}
		
		function paginar() {
			document.getElementById("iniciopagina").value=document.getElementById("paginas").value;
			document.getElementById("form_busqueda").submit();
		}
		
		function hacer_cadena_busqueda() {
			var codfamilia=document.getElementById("codfamilia").value;
			var nombre=document.getElementById("nombre").value;
			var cadena="";
			cadena="~"+codfamilia+"~"+nombre+"~";
			return cadena;
			}
			
		function limpiar() {
			document.getElementById("form_busqueda").reset();
		}
		</script>
	</head>
	<body onLoad="inicio()">
		<div id="pagina">
			<div id="zonaContenido">
				<div align="center">
				<div id="tituloForm" class="header">Buscar Sector</div>
				<div id="frmBusqueda">
				<form id="form_busqueda" name="form_busqueda" method="post" action="rejilla.php" target="frame_rejilla">
					<table class="fuente8" width="98%" cellspacing=0 cellpadding=3 border=0>					
						<tr>
							<td width="16%" class="Menu">Codigo de Sector </td>
							<td width="68%"><input id="codfamilia" type="text" class="cajaPequena" NAME="codfamilia" maxlength="3" value="<?php echo $codfamilia?>"></td>
							<td width="5%">&nbsp;</td>
							<td width="5%">&nbsp;</td>
							<td width="6%" align="right"></td>
						</tr>
						<tr>
							<td class="Menu">Nombre</td>
							<td><input id="nombre" name="nombre" type="text" class="cajaGrande" maxlength="20" value="<?php echo $nombre?>"></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
			  </div>
			 	<div id="botonBusqueda">
			 	  <table width="197" align="right">
			 	    <tr>
			 	      <td width="189" align="right"><img src="../img/impresora.jpg" alt="" width="79" height="58" title="Imprimir" onClick="imprimir()" onMouseOver="style.cursor=cursor"></td>
		 	        </tr>
		 	      </table>
			 	  <table width="357">
			 	    <tr align="right" >
			 	      <td width="100" height="83"><input type="submit" id="registrar" value="Buscar" title="Buscar" onClick="buscar()" onMouseOver="style.cursor=cursor"/></td>
			 	      <td width="100"><input type="submit" id="registrar" value="Limpiar" title="Limpiar" onClick="limpiar()" onMouseOver="style.cursor=cursor"/></td>
			 	      <td width="112"><input type="submit" id="registrar" value="Nuevo" title="Nuevo" onClick="nueva_familia()" onMouseOver="style.cursor=cursor"/></td>
			 	      <td width="25">&nbsp;</td>
		 	        </tr>
		 	      </table>
			 	</div>							
			  <div id="lineaResultado">
			  <table class="fuente8" width="80%" cellspacing=0 cellpadding=3 border=0>
			  	<tr>
				<td width="50%" align="left" class="Menu">&nbsp; encontrados
<input id="filas" type="text" class="cajaPequena2" NAME="filas" maxlength="5" readonly></td>
				<td width="50%" align="right" class="Menu">
				  <select name="paginas" id="paginas" class="comboPequeno" onChange="paginar()">
		          </select></td>
			  </table>
				</div>
				<div id="cabeceraResultado" class="header">SECTORES</div>
				<div id="frmResultado">
				<table class="fuente8" width="100%" cellspacing=0 cellpadding=3 border=0 ID="Table1">
						<tr class="cabeceraTabla">
							<td width="12%">ITEM</td>
							<td width="20%">CODIGO</td>
							<td width="50%">NOMBRE </td>
							<td width="6%">&nbsp;</td>
							<td width="6%">&nbsp;</td>
							<td width="6%">&nbsp;</td>
						</tr>
				</table>
				</div>
				<input type="hidden" id="iniciopagina" name="iniciopagina">
				<input type="hidden" id="cadena_busqueda" name="cadena_busqueda">
			</form>
				<div id="lineaResultado">
					<iframe width="100%" height="250" id="frame_rejilla" name="frame_rejilla" frameborder="0">
						<ilayer width="100%" height="250" id="frame_rejilla" name="frame_rejilla"></ilayer>
					</iframe>
				</div>
			</div>
		  </div>			
		</div>
	</body>
</html>
<?php
}else
{
	echo "<script type='text/javascript'>
		alert('Usted no tiene permiso de administrador');
		window.location='../index.html';
	</script>";
}
?>