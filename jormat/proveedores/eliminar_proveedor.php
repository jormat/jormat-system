<?php
session_start();
if($_SESSION["usuario"] == admin )
{
include ("../conectar.php"); 

$codproveedor=$_GET["codproveedor"];
$cadena_busqueda=$_GET["cadena_busqueda"];

$query="SELECT * FROM proveedores WHERE codproveedor='$codproveedor'";
$rs_query=mysql_query($query);

?>

<html>
	<head>
		<title>Principal</title>
		<link href="../estilos/estilos.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		
		var cursor;
		if (document.all) {
		// Está utilizando EXPLORER
		cursor='hand';
		} else {
		// Está utilizando MOZILLA/NETSCAPE
		cursor='pointer';
		}
		
		function aceptar(codproveedor) {
			location.href="guardar_proveedor.php?codproveedor=" + codproveedor + "&accion=baja" + "&cadena_busqueda=<?php echo $cadena_busqueda?>";
		}
		
		function cancelar() {
			location.href="index.php?cadena_busqueda=<?php echo $cadena_busqueda?>";
		}
		
		</script>
	</head>
	<body>
		<div id="pagina">
			<div id="zonaContenido">
				<div align="center">
				<div id="tituloForm" class="header">Eliminar Proveedor </div>
				<div id="frmBusqueda">
					<table class="fuente8" width="98%" cellspacing=0 cellpadding=3 border=0>
						<tr>
							<td width="15%"><label class="Menu">Codigo</label></td>
							<td width="43%" class="Menu2"><?php echo $codproveedor?></td>
							<td width="21%" rowspan="4" align="right" >&nbsp;</td>
							<td width="21%" rowspan="6" align="right" ><img src="../img/user_delete.png" width="128" height="128"></td>
					    </tr>
						<tr>
							<td width="15%"><label class="Menu">Nombre</label></td>
						    <td width="43%" class="Menu2"><?php echo mysql_result($rs_query,0,"nombre")?></td>
					    </tr>
						<tr>
						  <td><label class="Menu">Rut</label></td>
						  <td class="Menu2"><?php echo mysql_result($rs_query,0,"nif")?></td>
					  </tr>
						<tr>
						  <td height="24"><label class="Menu">Direccion</label></td>
						  <td class="Menu2"><?php echo mysql_result($rs_query,0,"direccion")?></td>
					  </tr>
						<tr>
						  <td><label class="Menu">Ciudad</label></td>
						  <td colspan="2" class="Menu2"><?php echo mysql_result($rs_query,0,"localidad")?></td>
					  </tr>
					  <?php
					  	$codprovincia=mysql_result($rs_query,0,"codprovincia");
						if ($codprovincia<>0) {
							$query_provincias="SELECT * FROM provincias WHERE codprovincia='$codprovincia'";
							$res_provincias=mysql_query($query_provincias);
							$nombreprovincia=mysql_result($res_provincias,0,"nombreprovincia");
						} else {
							$nombreprovincia="Sin determinar";
						}
					  ?>
						<tr>
							<td width="15%"><label class="Menu">Region</label></td>
							<td width="64%" colspan="2" class="Menu2"><?php echo $nombreprovincia?></td>
						</tr>
						<?php
						$codentidad=mysql_result($rs_query,0,"codentidad");
						if ($codentidad<>0) {
							$query_entidades="SELECT * FROM entidades WHERE codentidad='$codentidad'";
							$res_entidades=mysql_query($query_entidades);
							$nombreentidad=mysql_result($res_entidades,0,"nombreentidad");
						} else {
							$nombreentidad="Sin determinar";
						}
					  ?>
						<tr>
							<td width="15%"><label class="Menu">Entidad Bancaria</label></td>
							<td width="85%" colspan="3" class="Menu2"><?php echo $nombreentidad?></td>
					    </tr>
						<tr>
							<td><label class="Menu">Cuenta Corriente</label></td>
							<td colspan="3" class="Menu2"><?php echo mysql_result($rs_query,0,"cuentabancaria")?></td>
						</tr>
						<tr>
							<td><label class="Menu">Giro</label></td>
							<td colspan="3" class="Menu2"><?php echo mysql_result($rs_query,0,"codpostal")?></td>
						</tr>
						<tr>
							<td><label class="Menu">Telefono 1</label>
							</td>
							<td class="Menu2"><?php echo mysql_result($rs_query,0,"telefono")?></td>
						</tr>
						<tr>
							<td><label class="Menu">Telefono 2</label></td>
							<td colspan="3" class="Menu2"><?php echo mysql_result($rs_query,0,"movil")?></td>
						</tr>
						<tr>
							<td><label class="Menu">E-mail</label></td>
							<td colspan="3" class="Menu2"><?php echo mysql_result($rs_query,0,"email")?></td>
						</tr>
												<tr>
							<td><label class="Menu">Sitio Web</label></td>
							<td colspan="3" class="Menu2"><?php echo mysql_result($rs_query,0,"web")?></td>
						</tr>
					</table>
			  </div>
				<div id="botonBusqueda">
					<img src="../img/notification_done.png" width="62" height="50" title="Aceptar" onClick="aceptar(<?php echo $codproveedor?>)" onMouseOver="style.cursor=cursor">
					<img src="../img/notification_error.png" width="62" height="50" onClick="cancelar()" title="Cancelar" onMouseOver="style.cursor=cursor">
			  </div>
			 </div>
		  </div>
		</div>
	</body>
</html>
<?php
}else
{
	echo "<script type='text/javascript'>
		alert('Usted no tiene permiso de administrador');
		window.location='./index.php';
	</script>";
}
?>