<?php


session_start();
if($_SESSION["usuario"] == admin){
include ("../conectar.php");
if (isset($_GET["cadena_busqueda"]))
$cadena_busqueda=$_GET["cadena_busqueda"];

if (!isset($cadena_busqueda)) { $cadena_busqueda=""; } else { $cadena_busqueda=str_replace("",",",$cadena_busqueda); }

if ($cadena_busqueda<>"") {
	$array_cadena_busqueda=split("~",$cadena_busqueda);
	$codproveedor=$array_cadena_busqueda[1];
	$nombre=$array_cadena_busqueda[2];
	$nif=$array_cadena_busqueda[3];
	$provincia=$array_cadena_busqueda[4];
	$localidad=$array_cadena_busqueda[5];
	$telefono=$array_cadena_busqueda[6];
} else {
	$codproveedor="";
	$nombre="";
	$nif="";
	$provincia="";
	$localidad="";
	$telefono="";
}

?>
<html>
	<head>
		<title>Proveedores</title>
		<link href="../estilos/estilos.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		
		function inicio() {
			document.getElementById("form_busqueda").submit();
		}
		
		function nuevo_proveedor() {
			location.href="nuevo_proveedor.php";
		}
		
		var cursor;
		if (document.all) {
		// Está utilizando EXPLORER
		cursor='hand';
		} else {
		// Está utilizando MOZILLA/NETSCAPE
		cursor='pointer';
		}
		
		function imprimir() {
			var codproveedor=document.getElementById("codproveedor").value;
			var nombre=document.getElementById("nombre").value;
			var nif=document.getElementById("nif").value;			
			var provincia=document.getElementById("cboProvincias").value;
			var localidad=document.getElementById("localidad").value;
			var telefono=document.getElementById("telefono").value;
			window.open("../fpdf/proveedores.php?codproveedor="+codproveedor+"&nombre="+nombre+"&nif="+nif+"&provincia="+provincia+"&localidad="+localidad+"&telefono="+telefono);
		}
		
		function buscar() {
			var cadena;
			cadena=hacer_cadena_busqueda();
			document.getElementById("cadena_busqueda").value=cadena;
			if (document.getElementById("iniciopagina").value=="") {
				document.getElementById("iniciopagina").value=1;
			} else {
				document.getElementById("iniciopagina").value=document.getElementById("paginas").value;
			}
			document.getElementById("form_busqueda").submit();
		}
		
		function paginar() {
			document.getElementById("iniciopagina").value=document.getElementById("paginas").value;
			document.getElementById("form_busqueda").submit();
		}
		
		function hacer_cadena_busqueda() {
			var codproveedor=document.getElementById("codproveedor").value;
			var nombre=document.getElementById("nombre").value;
			var nif=document.getElementById("nif").value;			
			var provincia=document.getElementById("cboProvincias").value;
			var localidad=document.getElementById("localidad").value;
			var telefono=document.getElementById("telefono").value;
			var cadena="";
			cadena="~"+codproveedor+"~"+nombre+"~"+nif+"~"+provincia+"~"+localidad+"~"+telefono+"~";
			return cadena;
			}
			
		function limpiar() {
			document.getElementById("form_busqueda").reset();
		}
		
		var miPopup
		function abreVentana(){
			miPopup = window.open("ventana_proveedores.php","miwin","width=700,height=380,scrollbars=yes");
			miPopup.focus();
		}
		
		function validarproveedor(){
			var codigo=document.getElementById("codproveedor").value;
			miPopup = window.open("comprobarproveedor.php?codproveedor="+codigo,"frame_datos","width=700,height=80,scrollbars=yes");

		}
		</script>
    <style type="text/css">
<!--
.der {
	text-align: right;
}
-->
    </style>
	</head>
<body onLoad="inicio()">
		<div id="pagina">
			<div id="zonaContenido">
			<div align="center">
				<div id="tituloForm" class="header">Buscar Proveedor</div>
				<div id="frmBusqueda">
				<form id="form_busqueda" name="form_busqueda" method="post" action="rejilla.php" target="frame_rejilla">
					<table class="fuente8" width="98%" cellspacing=0 cellpadding=3 border=0>					
						<tr>
							<td width="142" class="Menu">Codigo de proveedor </td>
							<td width="592"><input id="codproveedor" type="text" class="cajaPequena" NAME="codproveedor" maxlength="10" value="<?php echo $codproveedor?>">  <img src="../img/search.png" width="16" height="16" onClick="abreVentana()" title="Buscar Proveedor" onMouseOver="style.cursor=cursor"> <img src="../img/notification_done.png" width="16" height="16" onClick="validarproveedor()" title="Validar Proveedor" onMouseOver="style.cursor=cursor"></td>
							<td colspan="3" rowspan="3" align="right"><img src="../img/file_search.png" width="86" height="86"></td>
						</tr>
						<tr>
							<td class="Menu">Nombre</td>
							<td><input id="nombre" name="nombre" type="text" class="cajaGrande" maxlength="45" value="<?php echo $nombre?>"></td>
						</tr>
						<tr>
						  <td height="30" class="Menu">Rut</td>
						  <td><input id="nif" type="text" class="cajaPequena" NAME="nif" maxlength="15" value="<?php echo $nif?>"></td>
					  </tr>
						<?php
					  	$query_provincias="SELECT * FROM provincias ORDER BY nombreprovincia ASC";
						$res_provincias=mysql_query($query_provincias);
						$contador=0;
					  ?>
					  <tr>
						  <td class="Menu">Direccion</td>
						  <td><input id="localidad" type="text" class="cajaGrande" NAME="localidad" maxlength="45" value="<?php echo $localidad?>"></td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
					  </tr>
						<tr>
						  <td class="Menu">Tel&eacute;fono</td>
						  <td><input id="telefono" type="text" class="cajaPequena" NAME="telefono" maxlength="15" value="<?php echo $telefono?>"></td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
					  </tr>
					</table>
			  </div>
			 	<div id="botonBusqueda" >
			 	  <table width="200" align="right">
			 	    <tr>
			 	      <td align="right">&nbsp;</td>
		 	        </tr>
		 	      </table>
			 	  <table width="357">
                  <tr align="right" >
                    <td width="140" height="57"><input type="submit" id="registrar" value="Buscar" title="Buscar" onClick="buscar()" onMouseOver="style.cursor=cursor"/></td>
                    <td width="61">
                    <input type="submit" id="registrar" value="Limpiar" title="Limpiar" onClick="limpiar()" onMouseOver="style.cursor=cursor"/></td>
                    <td width="61">
                    <input type="submit" id="registrar" value="Nuevo" title="Nuevo" onClick="nuevo_proveedor()" onMouseOver="style.cursor=cursor"/></td>
                    <td width="75">&nbsp;</td>
                  </tr>
                </table>
			 	</div>
			  <div id="lineaResultado">
			  <table class="fuente8" width="74%" cellspacing=0 cellpadding=3 border=0>
			  	<tr>
				<td width="408" align="left" class="Menu">  &nbsp;encontrados
<input id="filas" type="text" class="cajaPequena2" NAME="filas" maxlength="5" readonly></td>
				<td width="292" align="right" class="Menu"><select name="paginas" id="paginas" class="comboPequeno" onChange="paginar()">
			      </select>
                  </td>
			  </table>
				</div>
				<div id="cabeceraResultado" class="header">
					PROVEEDORES </div>
				<div id="frmResultado">
				<table class="fuente8" width="100%" cellspacing=0 cellpadding=3 border=0 ID="Table1">
						<tr class="cabeceraTabla">
							<td width="8%">ITEM</td>
							<td width="16%">CODIGO</td>
							<td width="28%">NOMBRE </td>
							<td width="13%">Rut</td>
							<td width="19%">TELEFONO</td>
							<td width="5%">&nbsp;</td>
							<td width="5%">&nbsp;</td>
							<td width="6%">&nbsp;</td>
						</tr>
				</table>
				</div>
				<input type="hidden" id="iniciopagina" name="iniciopagina">
				<input type="hidden" id="cadena_busqueda" name="cadena_busqueda">
			</form>
				<div id="lineaResultado">
					<iframe width="100%" height="250" id="frame_rejilla" name="frame_rejilla" frameborder="0">
						<ilayer width="100%" height="250" id="frame_rejilla" name="frame_rejilla"></ilayer>
					</iframe>
					<iframe id="frame_datos" name="frame_datos" width="0" height="0" frameborder="0">
					<ilayer width="0" height="0" id="frame_datos" name="frame_datos"></ilayer>
					</iframe>
				</div>
			</div>
		  </div>			
		</div>
	 </body>
     </html>
<?php
}else
{
	echo "<script type='text/javascript'>
		alert('Usted no tiene permiso de administrador');
		window.history.back();
	</script>";
}
?>