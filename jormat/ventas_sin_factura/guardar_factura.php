<?php
session_start();
if($_SESSION["usuario"]){
include ("../conectar.php"); 
include ("../funciones/fechas.php"); 

$accion=$_POST["accion"];
if (!isset($accion)) { $accion=$_GET["accion"]; }

$codboletatmp=$_POST["codboletatmp"];
$codcliente=$_POST["codcliente"];
$orden=$_POST["orden"];
$fecha=explota($_POST["fecha"]);
$codboleta=$_POST["codboleta"];
$iva=$_POST["iva"];
$minimo=0;

if ($accion=="alta") {
	$query_operacion="INSERT INTO boletas (codboleta, fecha, iva, codcliente,orden, estado, borrado) VALUES ('', '$fecha', '$iva', '$codcliente','$orden', '1', '0')";					
	$rs_operacion=mysql_query($query_operacion);
	$codboleta=mysql_insert_id();
	if ($rs_operacion) { $mensaje="La boleta ha sido ingresada correctamente"; }
	$query_tmp="SELECT * FROM bolelineatmp WHERE codboleta='$codboletatmp' ORDER BY numlinea ASC";
	$rs_tmp=mysql_query($query_tmp);
	$contador=0;
	$baseimponible=0;
	while ($contador < mysql_num_rows($rs_tmp)) {
		$codfamilia=mysql_result($rs_tmp,$contador,"codfamilia");
		$numlinea=mysql_result($rs_tmp,$contador,"numlinea");
		$codigo=mysql_result($rs_tmp,$contador,"codigo");
		$cantidad=mysql_result($rs_tmp,$contador,"cantidad");
		$precio=mysql_result($rs_tmp,$contador,"precio");
		$importe=mysql_result($rs_tmp,$contador,"importe");
		$baseimponible=$baseimponible+$importe;
		$dcto=mysql_result($rs_tmp,$contador,"dcto");
		$sel_insertar="INSERT INTO bolelinea (codboleta,numlinea,codfamilia,codigo,cantidad,precio,importe,dcto) VALUES 
		('$codboleta','$numlinea','$codfamilia','$codigo','$cantidad','$precio','$importe','$dcto')";
		$rs_insertar=mysql_query($sel_insertar);		
		$sel_articulos="UPDATE articulos SET stock=(stock-'$cantidad') WHERE codarticulo='$codigo' AND codfamilia='$codfamilia'";
		$rs_articulos=mysql_query($sel_articulos);
		$sel_minimos = "SELECT stock,stock_minimo,descripcion FROM articulos where codarticulo='$codigo' AND codfamilia='$codfamilia'";
		$rs_minimos= mysql_query($sel_minimos);
		if ((mysql_result($rs_minimos,0,"stock") < mysql_result($rs_minimos,0,"stock_minimo")) or (mysql_result($rs_minimos,0,"stock") <= 0))
	   		{ 
		  		$mensaje_minimo=$mensaje_minimo . " " . mysql_result($rs_minimos,0,"descripcion")."<br>";
				$minimo=1;
   			};
		$contador++;
	}
	$baseimpuestos=$baseimponible*($iva/100);
	$preciototal=$baseimponible+$baseimpuestos;
	//$preciototal=number_format($preciototal,2);	
	$sel_act="UPDATE boletas SET totalfactura='$preciototal' WHERE codboleta='$codboleta'";
	$rs_act=mysql_query($sel_act);
	$baseimpuestos=0;
	$baseimponible=0;
	$preciototal=0;
	$cabecera1="Inicio >> Ventas &gt;&gt; Nueva boleta ";
	$cabecera2="INSERTAR boleta ";
}

if ($accion=="modificar") {
	$codboleta=$_POST["codboleta"];
	$act_albaran="UPDATE boletas SET codcliente='$codcliente', fecha='$fecha', iva='$iva' WHERE codboleta='$codboleta'";
	$rs_albaran=mysql_query($act_albaran);
	$sel_lineas = "SELECT codigo,codfamilia,cantidad FROM bolelinea WHERE codboleta='$codboleta' order by numlinea";
	$rs_lineas = mysql_query($sel_lineas);
	$contador=0;
	while ($contador < mysql_num_rows($rs_lineas)) {
		$codigo=mysql_result($rs_lineas,$contador,"codigo");
		$codfamilia=mysql_result($rs_lineas,$contador,"codfamilia");
		$cantidad=mysql_result($rs_lineas,$contador,"cantidad");
		$sel_actualizar="UPDATE `articulos` SET stock=(stock+'$cantidad') WHERE codarticulo='$codigo' AND codfamilia='$codfamilia'";
		$rs_actualizar = mysql_query($sel_actualizar);
		$contador++;
	}
	$sel_borrar = "DELETE FROM bolelinea WHERE codboleta='$codboleta'";
	$rs_borrar = mysql_query($sel_borrar);
	$sel_lineastmp = "SELECT * FROM bolelineatmp WHERE codboleta='$codboletatmp' ORDER BY numlinea";
	$rs_lineastmp = mysql_query($sel_lineastmp);
	$contador=0;
	$baseimponible=0;
	while ($contador < mysql_num_rows($rs_lineastmp)) {
		$numlinea=mysql_result($rs_lineastmp,$contador,"numlinea");
		$codigo=mysql_result($rs_lineastmp,$contador,"codigo");
		$codfamilia=mysql_result($rs_lineastmp,$contador,"codfamilia");
		$cantidad=mysql_result($rs_lineastmp,$contador,"cantidad");
		$precio=mysql_result($rs_lineastmp,$contador,"precio");
		$importe=mysql_result($rs_lineastmp,$contador,"importe");
		$baseimponible=$baseimponible+$importe;
		$dcto=mysql_result($rs_lineastmp,$contador,"dcto");
	
		$sel_insert = "INSERT INTO bolelinea (codboleta,numlinea,codigo,codfamilia,cantidad,precio,importe,dcto) 
		VALUES ('$codboleta','','$codigo','$codfamilia','$cantidad','$precio','$importe','$dcto')";
		$rs_insert = mysql_query($sel_insert);
		
		$sel_actualiza="UPDATE articulos SET stock=(stock-'$cantidad') WHERE codarticulo='$codigo' AND codfamilia='$codfamilia'";
		$rs_actualiza = mysql_query($sel_actualiza);
		$sel_bajominimo = "SELECT codarticulo,codfamilia,stock,stock_minimo,descripcion FROM articulos WHERE codarticulo='$codigo' AND codfamilia='$codfamilia'";
		$rs_bajominimo= mysql_query($sel_bajominimo);
		$stock=mysql_result($rs_bajominimo,0,"stock");
		$stock_minimo=mysql_result($rs_bajominimo,0,"stock_minimo");
		$descripcion=mysql_result($rs_bajominimo,0,"descripcion");
		
		if (($stock < $stock_minimo) or ($stock <= 0))
		   { 
			  $mensaje_minimo=$mensaje_minimo . " " . $descripcion."<br>";
			  $minimo=1;
		   };
		$contador++;
	}
	$baseimpuestos=$baseimponible*($iva/100);
	$preciototal=$baseimponible+$baseimpuestos;
	//$preciototal=number_format($preciototal,2);	
	$sel_act="UPDATE boletas SET totalfactura='$preciototal' WHERE codboleta='$codboleta'";
	$rs_act=mysql_query($sel_act);
	$baseimpuestos=0;
	$baseimponible=0;
	$preciototal=0;
	if ($rs_query) { $mensaje="Los datos de la boleta han sido modificados correctamente"; }
	$cabecera1="Inicio >> Ventas &gt;&gt; Modificar boleta ";
	$cabecera2="MODIFICAR boleta ";
}

if ($accion=="baja") {
	$codboleta=$_GET["codboleta"];
	$query="UPDATE boletas SET borrado=1 WHERE codboleta='$codboleta'";
	$rs_query=mysql_query($query);
	$query="SELECT * FROM bolelinea WHERE codboleta='$codboleta' ORDER BY numlinea ASC";
	$rs_tmp=mysql_query($query);
	$contador=0;
	$baseimponible=0;
	while ($contador < mysql_num_rows($rs_tmp)) {
		$codfamilia=mysql_result($rs_tmp,$contador,"codfamilia");
		$codigo=mysql_result($rs_tmp,$contador,"codigo");
		$cantidad=mysql_result($rs_tmp,$contador,"cantidad");
		$sel_articulos="UPDATE articulos SET stock=(stock+'$cantidad') WHERE codarticulo='$codigo' AND codfamilia='$codfamilia'";
		$rs_articulos=mysql_query($sel_articulos);
		$contador++;
	}
	if ($rs_query) { $mensaje="La boleta ha sido eliminada correctamente"; }
	$cabecera1="Inicio >> Ventas &gt;&gt; Eliminar boleta";
	$cabecera2="ELIMINAR boleta";
	$query_mostrar="SELECT * FROM boletas WHERE codboleta='$codboleta'";
	$rs_mostrar=mysql_query($query_mostrar);
	$codcliente=mysql_result($rs_mostrar,0,"codcliente");
	$fecha=mysql_result($rs_mostrar,0,"fecha");
	$iva=mysql_result($rs_mostrar,0,"iva");
}

?>

<html>
	<head>
		<title>Principal</title>
		<link href="../estilos/estilos.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		var cursor;
		if (document.all) {
		// Está utilizando EXPLORER
		cursor='hand';
		} else {
		// Está utilizando MOZILLA/NETSCAPE
		cursor='pointer';
		}
		
		function aceptar() {
			location.href="index.php";
		}
		
	function imprimir(codboleta) {
			window.open("../fpdf/imprimir_factura.php?codfactura="+codboleta);
		}
		
		</script>
	</head>
	<body>
		<div id="pagina">
			<div id="zonaContenido">
				<div align="center">
				<div id="tituloForm" class="header">BOLETA </div>
				<div id="frmBusqueda">
					<table class="fuente8" width="98%" cellspacing=0 cellpadding=3 border=0>
						<tr>
							<td width="15%"></td>
							<td width="85%" colspan="2" class="mensaje"><?php echo $mensaje;?></td>
					    </tr>
						<?php if ($minimo==1) { ?>
						<!-- <tr>
							<td width="15%"></td>
							<td width="43%" class="mensajeminimo">Los siguientes repuestos est&aacute;n bajo m&iacute;nimo:<br><?php echo $mensaje_minimo;?></td>
							<td width="42%" class="mensajeminimo"><img src="../img/important.png" width="59" height="49"></td>
					    </tr> -->
						<?php } 
						 $sel_cliente="SELECT * FROM clientes WHERE nif='$codcliente'"; 
						  $rs_cliente=mysql_query($sel_cliente); ?>
						<tr>
							<td width="15%" class="Menu">Cliente</td>
							<td width="85%" colspan="2" class="Menu2"><?php echo mysql_result($rs_cliente,0,"nombre");?></td>
					    </tr>
						<tr>
							<td width="15%" class="Menu">rut</td>
						    <td width="85%" colspan="2" class="Menu2"><?php echo mysql_result($rs_cliente,0,"nif");?></td>
					    </tr>
						<tr>
						  <td class="Menu">Direcci&oacute;n</td>
						  <td colspan="2" class="Menu2"><?php echo mysql_result($rs_cliente,0,"direccion"); ?></td>
					  </tr>
						<tr>
						  <td class="Menu">C&oacute;digo de boleta</td>
						  <td colspan="2" class="Menu2"><?php echo $codboleta?></td>
					  </tr>
                    <!--   <tr>
						  <td class="Menu">Orden de compra</td>
						  <td colspan="2" class="Menu2"><?php echo $orden?></td>
					  </tr> -->
					  <tr>
						  <td class="Menu">Fecha</td>
						  <td colspan="2" class="Menu2"><?php echo implota($fecha)?></td>
					  </tr>
					  <tr>
						  <td class="Menu">IVA</td>
						  <td colspan="2" class="Menu2"><?php echo $iva?> %</td>
					  </tr>
					  <tr>
						  <td></td>
						  <td colspan="2"></td>
					  </tr>
				  </table>
					 <table class="fuente8" width="98%" cellspacing=0 cellpadding=3 border=0 ID="Table1">
					  <tr class="cabeceraTabla">
							<td width="5%">ITEM</td>
							<td width="25%">REFERENCIA</td>
							<td width="30%">DESCRIPCION</td>
							<td width="10%">CANTIDAD</td>
							<td width="10%">PRECIO</td>
							<td width="10%">DCTO %</td>
							<td width="10%">total</td>
						</tr>
					</table>
					<table class="fuente8" width="98%" cellspacing=0 cellpadding=3 border=0 ID="Table1">
					  <?php $sel_lineas="SELECT bolelinea.*,articulos.*,familias.nombre as nombrefamilia FROM bolelinea,articulos,familias WHERE bolelinea.codboleta='$codboleta' AND bolelinea.codigo=articulos.codarticulo AND bolelinea.codfamilia=articulos.codfamilia AND articulos.codfamilia=familias.codfamilia ORDER BY bolelinea.numlinea ASC";
$rs_lineas=mysql_query($sel_lineas);
						for ($i = 0; $i < mysql_num_rows($rs_lineas); $i++) {
							$numlinea=mysql_result($rs_lineas,$i,"numlinea");
							$codfamilia=mysql_result($rs_lineas,$i,"codfamilia");
							$nombrefamilia=mysql_result($rs_lineas,$i,"nombrefamilia");
							$codarticulo=mysql_result($rs_lineas,$i,"codarticulo");
							$descripcion=mysql_result($rs_lineas,$i,"descripcion");
							$referencia=mysql_result($rs_lineas,$i,"referencia");
							$cantidad=mysql_result($rs_lineas,$i,"cantidad");
							$precio=mysql_result($rs_lineas,$i,"precio");
							$importe=mysql_result($rs_lineas,$i,"importe");
							$baseimponible=$baseimponible+$importe;
							$descuento=mysql_result($rs_lineas,$i,"dcto");
							if ($i % 2) { $fondolinea="itemParTabla"; } else { $fondolinea="itemImparTabla"; } ?>
									<tr class="<?php echo $fondolinea?>">
										<td width="5%" class="aCentro"><?php echo $i+1?></td>
										<td width="25%" class="aleft"><?php echo $referencia?></td>
										<td width="30%" class="aleft"><?php echo $descripcion?></td>
										<td width="10%" class="aCentro"><?php echo $cantidad?></td>
										<td width="10%" class="aCentro"><?php echo $precio?></td>
										<td width="10%" class="aCentro"><?php echo $descuento?></td>
										<td width="10%" class="aCentro"><?php echo $importe?></td>
									</tr>
					<?php } ?>
					</table>
			  </div>
				  <?php
				  $baseimpuestos=$baseimponible*($iva/100);
			      $preciototal=$baseimponible+$baseimpuestos;
			      $preciototal=number_format($preciototal,0);
			  	  ?>
					<div id="frmBusqueda">
					<table width="25%" border=0 align="right" cellpadding=3 cellspacing=0 class="fuente8" bgcolor="#CCCCCC">
						<tr>
							<td width="15%" class="aleft" >neto</td>
							<td width="15%" class="aleft"><?php echo number_format($baseimponible,0);?>$</td>
						</tr>
						<tr>
							<td width="15%" class="aleft">IVA</td>
							<td width="15%" class="aleft"><?php echo number_format($baseimpuestos,0);?>$</td>
						</tr>
						<tr>
							<td width="15%" class="aleft">Total</td>
							<td width="15%" class="aleft"><?php echo $preciototal?>$</td>
						</tr>
					</table>
			  </div>
				<div id="botonBusqueda">
					<div align="center">
					  <img src="../img/notification_done.png" width="62" height="50" onClick="aceptar()" title="Aceptar" onMouseOver="style.cursor=cursor">&nbsp;&nbsp;&nbsp;
					   <img src="../img/impresora.jpg" width="79" height="58" title="Imprimir" onClick="window.print();" onMouseOver="style.cursor=cursor">
            </div>
					</div>
			  </div>
		  </div>
		</div>
	</body>
</html>
<?php
}else
{
	echo "<script type='text/javascript'>
		alert('Usted no tiene permiso de administrador');
		window.location='../index.html';
	</script>";
}
?>
