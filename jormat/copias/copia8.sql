-- MySQL dump 10.11
--
-- Host: localhost    Database: codeka
-- ------------------------------------------------------
-- Server version	5.0.51b-community-nt-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `albalinea`
--

DROP TABLE IF EXISTS `albalinea`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albalinea` (
  `codalbaran` int(11) NOT NULL default '0',
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) default NULL,
  `codigo` varchar(15) character set utf8 default NULL,
  `cantidad` float NOT NULL default '0',
  `precio` float NOT NULL default '0',
  `importe` float NOT NULL default '0',
  `dcto` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albalinea`
--

LOCK TABLES `albalinea` WRITE;
/*!40000 ALTER TABLE `albalinea` DISABLE KEYS */;
/*!40000 ALTER TABLE `albalinea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albalineap`
--

DROP TABLE IF EXISTS `albalineap`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albalineap` (
  `codalbaran` varchar(20) NOT NULL default '0',
  `codproveedor` int(5) NOT NULL default '0',
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) default NULL,
  `codigo` varchar(15) default NULL,
  `cantidad` float NOT NULL default '0',
  `precio` float NOT NULL default '0',
  `importe` float NOT NULL default '0',
  `dcto` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`codproveedor`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albalineap`
--

LOCK TABLES `albalineap` WRITE;
/*!40000 ALTER TABLE `albalineap` DISABLE KEYS */;
/*!40000 ALTER TABLE `albalineap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albalineaptmp`
--

DROP TABLE IF EXISTS `albalineaptmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albalineaptmp` (
  `codalbaran` int(11) NOT NULL default '0',
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) default NULL,
  `codigo` varchar(15) default NULL,
  `cantidad` float NOT NULL default '0',
  `precio` float NOT NULL default '0',
  `importe` float NOT NULL default '0',
  `dcto` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albalineaptmp`
--

LOCK TABLES `albalineaptmp` WRITE;
/*!40000 ALTER TABLE `albalineaptmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albalineaptmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albalineatmp`
--

DROP TABLE IF EXISTS `albalineatmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albalineatmp` (
  `codalbaran` int(11) NOT NULL default '0',
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) default NULL,
  `codigo` varchar(15) character set utf8 default NULL,
  `cantidad` float NOT NULL default '0',
  `precio` float NOT NULL default '0',
  `importe` float NOT NULL default '0',
  `dcto` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albalineatmp`
--

LOCK TABLES `albalineatmp` WRITE;
/*!40000 ALTER TABLE `albalineatmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albalineatmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albaranes`
--

DROP TABLE IF EXISTS `albaranes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albaranes` (
  `codalbaran` int(11) NOT NULL auto_increment,
  `codfactura` int(11) NOT NULL default '0',
  `fecha` date NOT NULL default '0000-00-00',
  `iva` tinyint(4) NOT NULL default '0',
  `codcliente` int(5) default '0',
  `estado` varchar(1) character set utf8 default '1',
  `totalalbaran` float NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albaranes`
--

LOCK TABLES `albaranes` WRITE;
/*!40000 ALTER TABLE `albaranes` DISABLE KEYS */;
/*!40000 ALTER TABLE `albaranes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albaranesp`
--

DROP TABLE IF EXISTS `albaranesp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albaranesp` (
  `codalbaran` varchar(20) NOT NULL default '0',
  `codproveedor` int(5) NOT NULL default '0',
  `codfactura` varchar(20) default NULL,
  `fecha` date NOT NULL default '0000-00-00',
  `iva` tinyint(4) NOT NULL default '0',
  `estado` varchar(1) default '1',
  `totalalbaran` float NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`codproveedor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albaranesp`
--

LOCK TABLES `albaranesp` WRITE;
/*!40000 ALTER TABLE `albaranesp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albaranesp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albaranesptmp`
--

DROP TABLE IF EXISTS `albaranesptmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albaranesptmp` (
  `codalbaran` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`codalbaran`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Temporal de albaranes de proveedores para controlar acceso s';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albaranesptmp`
--

LOCK TABLES `albaranesptmp` WRITE;
/*!40000 ALTER TABLE `albaranesptmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albaranesptmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albaranestmp`
--

DROP TABLE IF EXISTS `albaranestmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albaranestmp` (
  `codalbaran` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`codalbaran`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Temporal de albaranes para controlar acceso simultaneo';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albaranestmp`
--

LOCK TABLES `albaranestmp` WRITE;
/*!40000 ALTER TABLE `albaranestmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albaranestmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articulos`
--

DROP TABLE IF EXISTS `articulos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `articulos` (
  `codarticulo` int(10) NOT NULL auto_increment,
  `codfamilia` int(5) NOT NULL,
  `referencia` varchar(20) NOT NULL,
  `descripcion` text NOT NULL,
  `impuesto` int(11) default NULL,
  `codproveedor1` int(5) NOT NULL default '1',
  `codproveedor2` int(5) NOT NULL,
  `descripcion_corta` varchar(30) NOT NULL,
  `codubicacion` int(3) NOT NULL,
  `stock` int(10) NOT NULL,
  `stock_minimo` int(8) NOT NULL,
  `aviso_minimo` varchar(1) NOT NULL default '0',
  `datos_producto` varchar(200) NOT NULL,
  `fecha_alta` date NOT NULL default '0000-00-00',
  `codembalaje` int(3) NOT NULL,
  `unidades_caja` int(8) NOT NULL,
  `precio_ticket` varchar(1) NOT NULL default '0',
  `modificar_ticket` varchar(1) NOT NULL default '0',
  `observaciones` text NOT NULL,
  `precio_compra` int(10) default NULL,
  `precio_almacen` int(10) default NULL,
  `precio_tienda` int(10) default NULL,
  `precio_pvp` int(10) default NULL,
  `precio_iva` int(10) default NULL,
  `codigobarras` varchar(15) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codarticulo`)
) ENGINE=MyISAM AUTO_INCREMENT=166 DEFAULT CHARSET=utf8 COMMENT='Articulos';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `articulos`
--

LOCK TABLES `articulos` WRITE;
/*!40000 ALTER TABLE `articulos` DISABLE KEYS */;
INSERT INTO `articulos` VALUES (1,1,'0000900050','bomba cebadora petroleo',0,0,0,'',0,40,4,'1','','2011-11-16',0,0,'','','importacion deirecta',NULL,NULL,2429,5500,0,'8400000000017','foto1.jpg','0'),(2,1,'3874100031','cruceta cardan 57 x 152 mm',0,0,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion tructec',NULL,NULL,27882,51547,NULL,'8400000000024','foto2.jpg','0'),(3,1,'0152508303','Disco de embriague Actros 400 mm',0,0,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion E truck',NULL,NULL,74351,156200,NULL,'8400000000031','foto3.jpg','0'),(4,1,'5411300008','kit de compresor Actros mp1 100 mm ',0,0,0,'',1,8,1,'1','','2011-11-16',0,0,'','','importacion E truck',NULL,NULL,39034,92437,0,'8400000000048','foto4.jpg','0'),(5,1,'5411800009','Filtro de aceite para actros',0,0,0,'',0,40,5,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,6733,13025,0,'8400000000055','foto5.jpg','0'),(6,1,'9415010282','manguera rayador actros',0,0,0,'',1,2,1,'1','','2011-11-16',0,0,'','','Imporatcion Trucktec',NULL,NULL,8301,18262,NULL,'8400000000062','foto6.jpg','0'),(7,1,'9415010382','manguera  radiador ',0,1,0,'',1,2,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,8024,16807,NULL,'8400000000079','foto7.jpg','0'),(8,1,'0058202142','Motor limpia parabrisas actros',0,1,0,'',1,2,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,41506,104637,NULL,'8400000000086','foto8.jpg','0'),(9,1,'4410705533','juego caneria corto 2631',0,1,0,'',1,5,2,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,13610,37815,0,'8400000000093','foto9.jpg','0'),(10,1,'6204900365','flexible de escape 90 x 295 mm',0,1,0,'',1,3,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,18447,35294,0,'8400000000109','foto10.jpg','0'),(11,1,'4030740059','empaquetadura bomba inyectora serie 400',0,1,0,'',1,10,3,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,876,4400,NULL,'8400000000116','foto11.jpg','0'),(12,1,'4420740059','empaquetadura bomba inyectura serie 400',0,1,0,'',1,10,2,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,692,4400,NULL,'8400000000123','foto12.jpg','0'),(13,1,'5410531230','guia de valvula actros ',0,1,0,'',1,36,5,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,940,2500,NULL,'8400000000130','foto13.jpg','0'),(14,1,'4411300220','placa compresor sk',0,1,0,'',0,2,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,59031,109243,NULL,'8400000000147','foto14.jpg','0'),(15,1,'4410701133','juego caneria larga 2631',0,1,0,'',1,5,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,14869,37815,NULL,'8400000000154','foto15.jpg','0'),(16,1,'9403500835','kit reten actros con pista',0,1,0,'',1,10,2,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,33000,56302,NULL,'8400000000161','foto16.jpg','0'),(17,1,'0024204920','pastillas frenos actros',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,40584,71428,NULL,'8400000000178','foto17.jpg','0'),(18,1,'0002004722','viscoso ventilador actros',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,115296,212605,NULL,'8400000000185','foto18.jpg','0'),(19,1,'6253250219','calza paquete  resorte',0,1,0,'',1,2,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,32282,117647,NULL,'8400000000192','foto19.jpg','0'),(20,1,'0012606757','valvula gamma',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','0022950407',NULL,NULL,56265,84034,NULL,'8400000000208','foto20.jpg','0'),(21,1,'0001539520','sensor',0,1,0,'',1,1,0,'0','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,59953,110836,NULL,'8400000000215','foto21.jpg','0'),(22,1,'0011532120','sensor',0,1,0,'',1,1,1,'0','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,44273,81848,NULL,'8400000000222','foto22.jpg','0'),(23,1,'0024600880','bomba de direccion actros',0,1,0,'',1,10,2,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,106072,210000,NULL,'8400000000239','foto23.jpg','0'),(24,1,'3463530072','tuerca pinon de ataque 55 mm',0,1,0,'',1,10,2,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,2121,5500,NULL,'8400000000246','foto24.jpg','0'),(25,1,'0089976145','orring bomba inyectora chico',0,1,0,'',1,50,5,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,83,3361,NULL,'8400000000253','foto25.jpg','0'),(26,1,'0000925208','tapa filtro petroleo actros',0,1,0,'',1,6,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,4704,8655,NULL,'8400000000260','foto26.jpg','0'),(27,1,'5420140422','empaquetadura de carter actros 502',0,1,0,'',1,6,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,6826,15126,NULL,'8400000000277','foto27.jpg','0'),(28,1,'3553530158','guardapolvo diferencial',0,1,0,'',1,2,1,'0','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,4704,14000,NULL,'8400000000284','foto28.jpg','0'),(29,1,'0004711230','tapa estanque actros',0,1,0,'',1,10,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,4150,12185,NULL,'8400000000291','foto29.jpg','0'),(30,1,'0005453013','chapa contacto clavo',0,1,0,'',1,10,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,5200,9244,NULL,'8400000000307','foto30.jpg','0'),(31,1,'0002952818','servo embrague actros',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,104091,192437,NULL,'8400000000314','foto31.jpg','0'),(32,1,'5410101120','empaquetaduras descarbo. culata actros',0,1,0,'',1,16,2,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,18123,26890,NULL,'8400000000321','foto32.jpg','0'),(33,1,'0004300969','filtro secador apu',0,1,0,'',1,20,2,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,7899,15966,NULL,'8400000000338','foto33.jpg','0'),(34,1,'4011840025','filtro aceite corto',0,1,0,'',1,20,2,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,4800,8824,NULL,'8400000000345','foto34.jpg','0'),(35,1,'4411300008','kit compresor 100 mm sin pestana',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,39034,92436,NULL,'8400000000352','foto35.jpg','0'),(36,1,'5419970992','correa ventilador actros 9 pk 2835',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,11942,22078,NULL,'8400000000369','foto36.jpg','0'),(37,1,'0152507903','disco embrague actros 400 mm',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,74351,151260,NULL,'8400000000376','foto37.jpg','0'),(38,1,'6253250319','calza paquete resorte actros',0,1,0,'',1,2,1,'0','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,32528,117647,NULL,'8400000000383','foto38.jpg','0'),(39,1,'0004700469','filtro purificador con soporte y cebador',0,1,0,'',1,2,1,'0','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,87625,110000,NULL,'8400000000390','foto39.jpg','0'),(40,1,'9413000104','pedal acelerador actros completo',0,1,0,'',1,1,0,'0','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,142967,302521,NULL,'8400000000406','foto40.jpg','0'),(41,1,'0020945282','manguera intercoler actros 1938',0,1,0,'',1,2,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,41506,62000,NULL,'8400000000413','foto41.jpg','0'),(42,1,'6564100131','cruceta cardan 68 mm',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,41506,98000,NULL,'8400000000420','foto42.jpg','0'),(43,1,'3893251062','pista bogue',0,1,0,'',1,6,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,12677,75000,NULL,'8400000000437','foto43.jpg','0'),(44,1,'3953200067','guardapolvo bogue',0,1,0,'',1,6,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,7038,36000,NULL,'8400000000444','foto44.jpg','0'),(45,1,'4030320309','pista ciguenal trasera actros y 400',0,1,0,'',1,10,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,4647,11764,NULL,'8400000000451','foto45.jpg','0'),(46,1,'9423530117','guardapolvo diferencial actros',0,1,0,'',1,2,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,4704,15000,NULL,'8400000000468','foto46.jpg','0'),(47,1,'5411400163','reparacion freno motor actros',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,21675,46218,NULL,'8400000000475','foto47.jpg','0'),(48,1,'3814100231','cruceta cardan 48 x 126 mm',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,23981,35000,NULL,'8400000000482','foto48.jpg','0'),(49,1,'0002520646','kit desembrague',0,1,0,'',1,2,1,'0','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,17063,30000,NULL,'8400000000499','foto49.jpg','0'),(50,1,'0143517','reparacion caja direccion actros',0,1,0,'',1,3,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,23234,54621,NULL,'8400000000505','foto50.jpg','0'),(51,1,'5412001870','tensor correa actros',0,1,0,'',1,6,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,50730,100000,NULL,'8400000000512','foto51.jpg','0'),(52,1,'0143551','juego empaquetadura completo g155 g210 g240',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,73789,120000,NULL,'8400000000529','foto52.jpg','0'),(53,1,'5410350214','adaptador ventilador actros',0,1,0,'',1,1,1,'0','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,32282,71000,NULL,'8400000000536','foto53.jpg','0'),(54,1,'0143004','reparacion laina bomba inyectora 355',0,1,0,'',1,10,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,4427,10000,NULL,'8400000000543','foto54.jpg','0'),(55,1,'9423172012','buje cabina actros',0,1,0,'',1,10,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,25826,60000,0,'8400000000550','foto55.jpg','0'),(56,1,'5419900501','perno culata actros 18 x 210 mm',0,1,0,'',1,12,2,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,2490,5500,NULL,'8400000000567','foto56.jpg','0'),(57,1,'0020945582','manguera intercooler actros',0,1,0,'',1,2,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,41506,82000,NULL,'8400000000574','foto57.jpg','0'),(58,1,'9425010582','manguera conexion radiador',0,1,0,'',1,1,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,14757,29000,NULL,'8400000000581','foto58.jpg','0'),(59,1,'4420709633','juego caneria 442la larga',0,1,0,'',1,1,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,142967,28000,NULL,'8400000000598','foto59.jpg','0'),(60,1,'4420708133','juego caneria 442la corta',0,1,0,'',1,1,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,142967,28000,NULL,'8400000000604','foto60.jpg','0'),(61,1,'6564110012','rodamiento cardan 70 mm actros',0,1,0,'',1,6,2,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,57186,100000,NULL,'8400000000611','foto61.jpg','0'),(62,1,'4572000501','bomba de agua 2638',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,64604,115000,NULL,'8400000000628','foto62.jpg','0'),(63,1,'0004771302','filtro racor actros largo',0,1,0,'',1,40,5,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,15220,25000,NULL,'8400000000635','foto63.jpg','0'),(64,1,'0000900650','bomba elevadora 402 sin turbo',0,1,0,'',1,2,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,15680,29400,NULL,'8400000000642','foto64.jpg','0'),(65,1,'5410900151','filtro petroleo actros',0,1,0,'',1,20,5,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,7083,14200,NULL,'8400000000659','foto65.jpg','0'),(66,1,'9423560715','rueda polar sin reten',0,1,0,'',1,30,5,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,4611,8000,NULL,'8400000000666','foto66.jpg','0'),(67,1,'4420310027','pista ciguenal delantera actros',0,1,0,'',1,10,2,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,5257,11764,NULL,'8400000000673','foto67.jpg','0'),(68,1,'0049815801','rodamiento interior eje piloto',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,8067,23530,0,'8400000000680','foto68.jpg','0'),(69,1,'942501068','manguera radiador',0,1,0,'',1,5,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,3873,8500,NULL,'8400000000697','foto69.jpg','0'),(70,1,'54222001','bomba de agua actros',0,1,0,'',1,8,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,66410,165000,NULL,'8400000000703','foto70.jpg','0'),(71,1,'442130008','kit compresor 90 mm por aire',0,1,0,'',1,4,1,'1','','2011-11-16',0,0,'','','importacion trucktec',NULL,NULL,22136,54621,NULL,'8400000000710','foto71.jpg','0'),(72,1,'30400','VALVULA MIDLAN',0,2,0,'',1,4,1,'1','','2011-11-17',0,0,'','','COMPRA EN PLAZA',NULL,NULL,24990,31932,NULL,'8400000000727','foto72.jpg','0'),(73,1,'100675','perno culata corto scania 111',0,3,0,'',1,4,1,'1','','2011-11-17',0,0,'','','compre en plaza',NULL,NULL,3136,4500,NULL,'8400000000734','foto73.jpg','0'),(74,1,'3464201738','chicharra freno izq.actros',0,4,0,'',1,50,2,'1','','2011-11-17',0,0,'','','compra en plaza',NULL,NULL,11400,22000,NULL,'8400000000741','foto74.jpg','0'),(75,1,'3464201838','chicharra de freno der.actros',0,4,0,'',1,50,2,'1','','2011-11-17',0,0,'','','compra en plaza',NULL,NULL,11400,22000,NULL,'8400000000758','foto75.jpg','0'),(76,1,'0028200656','foco visera actros blanco',0,4,0,'',1,4,2,'1','','2011-11-17',0,0,'','','compra en plaza',NULL,NULL,4425,7893,NULL,'8400000000765','foto76.jpg','0'),(77,1,'0192506003','disco embreague 430mm x 18 estrias 2638',0,4,0,'',1,1,0,'0','','2011-11-17',0,0,'','','compra en plaza',NULL,NULL,111000,155462,NULL,'8400000000772','foto77.jpg','0'),(78,1,'5410101072','varilla medir aceite actros',0,4,0,'',1,1,0,'0','','2011-11-17',0,0,'','','compra en plaza',NULL,NULL,18600,26890,NULL,'8400000000789','foto78.jpg','0'),(79,1,'145240','reten entrada diferencial  145 ford cargo',0,5,0,'',1,1,0,'0','','2011-11-17',0,0,'','','compra en plaza',NULL,NULL,8000,13445,NULL,'8400000000796','foto79.jpg','0'),(80,1,'240145','kit golillas diferencial 145 ford cargo',0,5,0,'',1,2,0,'0','','2011-11-17',0,0,'','','compra en plaza',NULL,NULL,9000,15126,NULL,'8400000000802','foto80.jpg','0'),(81,1,'2212033','perno rueda maza americano 22x120 con tuerca',0,2,0,'',1,40,5,'1','','2011-11-17',0,0,'','','compra en plaza',NULL,NULL,3391,7143,NULL,'8400000000819','foto81.jpg','0'),(82,1,'229832','perno rueda remolque randon con tuerca',0,2,0,'',1,40,5,'1','','2011-11-17',0,0,'','','compra en plaza',NULL,NULL,3273,5462,NULL,'8400000000826','foto82.jpg','0'),(83,1,'3515','valvula protectora 6 vias sin sensor',0,6,0,'',1,3,1,'1','','2011-11-17',0,0,'','','compra en plaza',NULL,NULL,26180,63025,NULL,'8400000000833','foto83.jpg','0'),(84,1,'0004301315','secador aire actros',0,6,0,'',1,2,1,'1','','2011-11-17',0,0,'','','compra en plaza',NULL,NULL,40336,117647,NULL,'8400000000840','foto84.jpg','0'),(85,1,'0001801709','filtro aceite 906',0,1,0,'',0,10,0,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,5026,8400,NULL,'8400000000857','foto85.jpg','0'),(86,1,'0159974747','reten piñon ataque actros ',0,1,0,'',0,4,1,'1','','2011-11-17',0,0,'','','importacion trukctec',NULL,NULL,9638,39495,NULL,'8400000000864','foto86.jpg','0'),(87,1,'4021300220','reparacion compresor 402 antiguo',0,1,0,'',1,20,2,'1','','2011-11-17',0,0,'','','importacion trukctec',NULL,NULL,4289,11764,NULL,'8400000000871','foto87.jpg','0'),(88,1,'0000902150','bomba elebadora 422 A',0,1,0,'',1,2,1,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,15956,29412,NULL,'8400000000888','foto88.jpg','0'),(89,1,'0000902190','bonba elebadora universal',0,1,0,'',1,10,2,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,5073,16806,NULL,'8400000000895','foto89.jpg','0'),(90,1,'0004770002','vaso con soporte y filtro petroleo',0,1,0,'',1,10,2,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,3689,10084,NULL,'8400000000901','foto90.jpg','0'),(91,1,'3852681474','reparacion rotula palanca cambio',0,0,0,'',0,4,1,'1','','2011-11-17',0,0,'','','importacion trukctec',NULL,NULL,6862,19328,NULL,'8400000000918','foto91.jpg','0'),(92,1,'9413230050','buje corbata actros 73x100 ojo de 19',0,1,0,'',1,4,1,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,23059,46218,NULL,'8400000000925','foto92.jpg','0'),(93,1,'3434100631','cruceta cardan 53x135',0,1,0,'',1,4,1,'1','','2011-11-17',0,0,'','','importacion  trucktec',NULL,NULL,27671,37815,NULL,'8400000000932','foto93.jpg','0'),(94,1,'0000740515','valvula sobreprecion bomba inyectora',0,1,0,'',1,10,1,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,1383,4369,NULL,'8400000000949','foto94.jpg','0'),(95,1,'0032037975','termostato 71º actros',0,1,0,'',1,10,1,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,5073,11764,NULL,'8400000000956','foto95.jpg','0'),(96,1,'0032037375','termostato 83º actros',0,1,0,'',1,20,2,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,5718,12521,NULL,'8400000000963','foto96.jpg','0'),(97,1,'0035445032','rele intermitente limpia parabrizas',0,1,0,'',1,4,1,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,21214,74790,NULL,'8400000000970','foto97.jpg','0'),(98,1,'0022950407','cilindro embrague inferior aleman',0,1,0,'',1,4,1,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,25093,37956,NULL,'8400000000987','foto98.jpg','0'),(99,1,'5410102133','empaquetadura tapa distribucion actros',0,1,0,'',1,6,2,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,7620,14286,NULL,'8400000000994','foto99.jpg','0'),(100,1,'0002522145','espejo prensa chico 1924',0,1,0,'',1,2,1,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,6622,15126,NULL,'8400000001007','foto100.jpg','0'),(101,1,'6214900065','flexible escape 115x 295 mm',0,1,0,'',1,3,1,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,16141,40336,NULL,'8400000001014','foto101.jpg','0'),(102,1,'6204900465','flexible escape 105x 295 mm',0,1,0,'',1,3,1,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,20242,40336,NULL,'8400000001021','foto102.jpg','0'),(103,1,'0143078','reparacion bonba elevadora con pre filtro',0,1,0,'',1,30,2,'1','','2011-11-17',0,0,'','','importacio trucktec',NULL,NULL,1181,4200,NULL,'8400000001038','foto103.jpg','0'),(104,1,'6179931110','resorte patin freno',0,1,0,'',1,10,2,'1','','2011-11-17',0,0,'','','importacion trucktec',NULL,NULL,922,2941,NULL,'8400000001045','foto104.jpg','0'),(105,1,'0022504115','rodamiento embreague actros',0,3,0,'',1,2,1,'1','','2011-11-18',0,0,'','','compra en plaza',NULL,NULL,49343,82353,NULL,'8400000001052','foto105.jpg','0'),(106,1,'20568396','valvula pedalera volvo ',0,3,0,'',1,2,1,'1','','2011-11-18',0,0,'','','compra en plaza',NULL,NULL,30000,117647,0,'8400000001069','foto106.jpg','0'),(107,1,'247','pulmon americano simple 7\"',0,7,0,'',1,10,2,'1','','2011-11-18',0,0,'','','compra en plaza',NULL,NULL,7140,11764,NULL,'8400000001076','foto107.jpg','0'),(108,1,'0020942404','filtro de aire 331305',0,4,0,'',1,1,0,'0','','2011-11-18',0,0,'','','compra en plaza',NULL,NULL,51750,66386,NULL,'8400000001083','foto108.jpg','0'),(109,1,'0001310011','anillo compresor 90mm',0,3,0,'',1,2,1,'1','','2011-11-18',0,0,'','','compra en plaza',NULL,NULL,5162,8319,NULL,'8400000001090','foto109.jpg','0'),(110,1,'26365','kit bujes guardapolvo palanca cambio 1318',0,3,0,'',1,1,0,'0','','2011-11-18',0,0,'','','compra en plaza',NULL,NULL,4404,5462,NULL,'8400000001106','foto110.jpg','0'),(111,1,'9425011682','manguera radiador actros',0,1,0,'',1,1,0,'0','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,6438,14163,NULL,'8400000001113','foto111.jpg','0'),(112,1,'3522540135','buje placa embreague horquilla',0,3,0,'',1,1,0,'0','','2011-11-18',0,0,'','','compra en plaza',NULL,NULL,4030,6500,NULL,'8400000001120','foto112.jpg','0'),(113,1,'4021300320','placa compresor 402',0,1,0,'',1,4,1,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,16602,31933,NULL,'8400000001137','foto113.jpg','0'),(114,1,'5411801020','caneria aceite actros ',0,1,0,'',1,1,0,'0','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,15220,38000,NULL,'8400000001144','foto114.jpg','0'),(115,1,'5410181112','manguera retorno aceite',0,1,0,'',1,2,0,'0','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,4381,10900,0,'8400000001151','foto115.jpg','0'),(116,1,'5410180712','manguera retorno aceite actros',0,1,0,'',1,2,0,'0','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,3966,9915,0,'8400000001168','foto116.jpg','0'),(117,1,'9425010782','manguera radiador actros',0,1,0,'',1,1,0,'0','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,5534,13835,NULL,'8400000001175','foto117.jpg','0'),(118,1,'0025531005','cilindro levante cabina actros',0,1,0,'',1,2,1,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,68775,149580,NULL,'8400000001182','foto118.jpg','0'),(119,1,'5410140722','empaquetadura de carter actros 501',0,1,0,'',1,6,1,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,5349,11764,NULL,'8400000001199','foto119.jpg','0'),(120,1,'9425011535','manguera radiador actros',0,1,0,'',1,5,1,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,7378,15966,NULL,'8400000001205','foto120.jpg','0'),(121,1,'4030705133','caneria inyector nº 3 402',0,1,0,'',1,10,2,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,2306,6302,NULL,'8400000001212','foto121.jpg','0'),(122,1,'4030702533','caneria inyector nº6 402',0,1,0,'',1,10,2,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,2306,6302,NULL,'8400000001229','foto122.jpg','0'),(123,1,'4030704933','caneria inyector nº 1 402',0,1,0,'',1,10,2,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,2306,6302,NULL,'8400000001236','foto123.jpg','0'),(124,1,'4030706033','caneria inyector nº 2 402',0,1,0,'',1,10,2,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,2306,6302,NULL,'8400000001243','foto124.jpg','0'),(125,1,'4030702733','caneria inyector nº 7 402',0,1,0,'',1,10,2,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,2306,6302,NULL,'8400000001250','foto125.jpg','0'),(126,1,'4030702633','caneria inyector nº 7 402',0,1,0,'',1,10,2,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,2306,6302,NULL,'8400000001267','foto126.jpg','0'),(127,1,'4030702833','caneria inyector nº8 402',0,1,0,'',1,10,2,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,2306,6302,NULL,'8400000001274','foto127.jpg','0'),(128,1,'4030707433','caneria inyector nº4 402',0,1,0,'',1,10,2,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,2306,6032,NULL,'8400000001281','foto128.jpg','0'),(129,1,'0053266300','amortiguador trasero actros ',0,1,0,'',1,8,2,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,46400,75630,NULL,'8400000001298','foto129.jpg','0'),(130,1,'6552501513','horquilla embraque actros con pasador y buje',0,1,0,'',1,4,1,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,55763,116806,NULL,'8400000001304','foto130.jpg','0'),(131,1,'9402500413','horquilla embreaque actros ancha con pasador',0,1,0,'',1,1,0,'0','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,65353,136975,NULL,'8400000001311','foto131.jpg','0'),(132,1,'3553560915','pista maza trasera 110x 145',0,1,0,'',1,4,2,'1','','2011-11-18',0,0,'','','importacion tracktec',NULL,NULL,7738,15966,NULL,'8400000001328','foto132.jpg','0'),(133,1,'0022950406','cilindro superior embreaque actros',0,1,0,'',1,6,1,'1','','2011-11-18',0,0,'','','importacion trucktec',NULL,NULL,24164,52100,NULL,'8400000001335','foto133.jpg','0'),(134,1,'1315298001','empaquetadura caja cambio 16S221',0,8,0,'',1,1,0,'0','','2011-11-19',0,0,'','','compra en plaza',NULL,NULL,42000,75630,NULL,'8400000001342','foto134.jpg','0'),(135,1,'3512','gobernador sin secador 2638',0,6,0,'',0,1,0,'0','','2011-11-19',0,0,'','','compra en plaza',NULL,NULL,19040,36000,NULL,'8400000001359','foto135.jpg','0'),(136,1,'0374301001','disco embrague 2631 meyle sin pre amortiguacion',0,4,0,'',1,1,0,'1','','2011-11-19',0,0,'','','compra en plaza',NULL,NULL,114675,155462,NULL,'8400000001366','foto136.jpg','0'),(137,1,'5411420180','empaquetadura escape actros ',0,4,0,'',1,12,6,'1','','2011-11-19',0,0,'','','compra en plaza',NULL,NULL,1200,2521,NULL,'8400000001373','foto137.jpg','0'),(138,1,'0003262681','buje barra estabilizadora delantera ',0,4,0,'',1,4,1,'1','','2011-11-19',0,0,'','','compra en plaza',NULL,NULL,9854,12185,NULL,'8400000001380','foto138.jpg','0'),(139,1,'0003238185','buje barra estabilizadora trasero',0,4,0,'',1,6,2,'1','','2011-11-19',0,0,'','','compra en plaza',NULL,NULL,9600,13445,NULL,'8400000001397','foto139.jpg','0'),(140,1,'0003235985','buje barra extabilizadora ',0,4,0,'',1,10,2,'1','','2011-11-19',0,0,'','','compra en plaza',NULL,NULL,3075,5462,NULL,'8400000001403','foto140.jpg','0'),(141,1,'0011311111','anillo compresor 85 mm 2428',0,3,0,'',1,1,0,'1','','2011-11-22',0,0,'','','compre en plaza',NULL,NULL,4800,8319,NULL,'8400000001410','foto141.jpg','0'),(142,1,'0011300415','reparacion compresor wabco 2428',0,3,0,'',1,1,0,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,32000,41933,NULL,'8400000001427','foto142.jpg','0'),(143,1,'6673170012','soporte cabina 812',0,3,0,'',1,4,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,7022,8823,NULL,'8400000001434','foto143.jpg','0'),(144,1,'1349840','pulmon suspencion scania s/4',0,9,0,'',0,3,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,33152,53782,0,'8400000001441','foto144.jpg','0'),(145,1,'80468','goma camisa 128 mm',0,4,0,'',1,8,6,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,5100,6218,NULL,'8400000001458','foto145.jpg','0'),(146,1,'4021300020','reparacion compresor doble 422 A',0,4,0,'',1,2,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,13800,21008,NULL,'8400000001465','foto146.jpg','0'),(147,1,'0079975948','oring bogue ',0,4,0,'',1,4,2,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,1785,4725,NULL,'8400000001472','foto147.jpg','0'),(148,1,'08445','reten diferencial 72x105x13',0,4,0,'',1,2,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,5110,11765,NULL,'8400000001489','foto148.jpg','0'),(149,1,'07747','guardapolvo diferencial 72x105x13',0,4,0,'',1,2,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,2790,11765,NULL,'8400000001496','foto149.jpg','0'),(150,1,'1210','pulmon tristop 24/30',0,4,0,'',1,10,2,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,22721,37815,NULL,'8400000001502','foto150.jpg','0'),(151,1,'1957','rodamiento empuje sachs bajo 87mm',0,4,0,'',1,2,0,'1','','2011-11-22',0,0,'','','compra en plaza ',NULL,NULL,96696,121849,0,'8400000001519','foto151.jpg','0'),(152,1,'1314432','rejilla protectora neblinero scania s/4',0,10,0,'',0,2,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,11300,18487,NULL,'8400000001526','foto152.jpg','0'),(153,1,'1401241','rejilla protectora foco mayor scania s/4',0,10,0,'',1,2,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,23420,32773,NULL,'8400000001533','foto153.jpg','0'),(154,1,'1337249','foco mayor scania s/4 con intermitente',0,10,0,'',1,1,0,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,48228,63025,NULL,'8400000001540','foto154.jpg','0'),(155,1,'14229910','neblinero scania s/4',0,10,0,'',1,1,0,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,22318,29412,NULL,'8400000001557','foto155.jpg','0'),(156,1,'276517','chicharra freno delantera izquierda',0,10,0,'',1,2,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,20144,26050,NULL,'8400000001564','foto156.jpg','0'),(157,1,'1096643','espejo cunetero volvo fh12',0,11,0,'',1,2,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,34534,54622,NULL,'8400000001571','foto157.jpg','0'),(158,1,'5421','filtro petroleo ff5421',0,5,0,'',1,20,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,9900,11260,NULL,'8400000001588','foto158.jpg','0'),(159,1,'1242','filtro purificador petroleo fs1242',0,5,0,'',1,2,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,6900,8319,NULL,'8400000001595','foto159.jpg','0'),(160,1,'16015','filtro aceite lf 16015',0,5,0,'',1,2,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,6900,8320,NULL,'8400000001601','foto160.jpg','0'),(161,1,'7998','fitro aire con flitro interior ap 7998 9150',0,5,0,'',1,2,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,20000,23529,NULL,'8400000001618','foto161.jpg','0'),(162,1,'8.3','solenoide parada ',0,5,0,'',1,1,0,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,110000,124780,NULL,'8400000001625','foto162.jpg','0'),(163,1,'1620462','cilindro embrague volvo fl10',0,4,0,'',1,5,1,'1','','2011-11-22',0,0,'','','cdompra en plaza',NULL,NULL,10040,16723,NULL,'8400000001632','foto163.jpg','0'),(164,1,'257772','gobernador aire varga tipo bendix',0,3,0,'',1,2,1,'1','','2011-11-22',0,0,'','','compre en plaza',NULL,NULL,21136,21848,NULL,'8400000001649','foto164.jpg','0'),(165,1,'3864100031','cruceta cardan 48x161 mm',0,3,0,'',1,2,1,'1','','2011-11-22',0,0,'','','compra en plaza',NULL,NULL,23984,29411,NULL,'8400000001656','foto165.jpg','0');
/*!40000 ALTER TABLE `articulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artpro`
--

DROP TABLE IF EXISTS `artpro`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `artpro` (
  `codarticulo` varchar(15) NOT NULL,
  `codfamilia` int(3) NOT NULL,
  `codproveedor` int(5) NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY  (`codarticulo`,`codfamilia`,`codproveedor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `artpro`
--

LOCK TABLES `artpro` WRITE;
/*!40000 ALTER TABLE `artpro` DISABLE KEYS */;
/*!40000 ALTER TABLE `artpro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `clientes` (
  `codcliente` int(5) NOT NULL auto_increment,
  `nombre` varchar(45) NOT NULL,
  `nif` varchar(12) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `codprovincia` int(2) NOT NULL default '0',
  `localidad` varchar(35) NOT NULL,
  `codformapago` int(2) NOT NULL default '0',
  `codentidad` int(2) NOT NULL default '0',
  `cuentabancaria` varchar(20) NOT NULL,
  `codpostal` varchar(5) NOT NULL,
  `telefono` varchar(14) NOT NULL,
  `movil` varchar(14) NOT NULL,
  `email` varchar(35) NOT NULL,
  `web` varchar(45) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codcliente`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cobros`
--

DROP TABLE IF EXISTS `cobros`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `cobros` (
  `id` int(11) NOT NULL auto_increment,
  `codfactura` int(11) NOT NULL,
  `codcliente` int(5) NOT NULL,
  `importe` int(11) default NULL,
  `codformapago` int(2) NOT NULL,
  `numdocumento` varchar(30) NOT NULL,
  `fechacobro` date NOT NULL default '0000-00-00',
  `observaciones` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Cobros de facturas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `cobros`
--

LOCK TABLES `cobros` WRITE;
/*!40000 ALTER TABLE `cobros` DISABLE KEYS */;
/*!40000 ALTER TABLE `cobros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `embalajes`
--

DROP TABLE IF EXISTS `embalajes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `embalajes` (
  `codembalaje` int(3) NOT NULL auto_increment,
  `nombre` varchar(30) default NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codembalaje`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Embalajes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `embalajes`
--

LOCK TABLES `embalajes` WRITE;
/*!40000 ALTER TABLE `embalajes` DISABLE KEYS */;
/*!40000 ALTER TABLE `embalajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entidades`
--

DROP TABLE IF EXISTS `entidades`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `entidades` (
  `codentidad` int(2) NOT NULL auto_increment,
  `nombreentidad` varchar(50) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codentidad`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Entidades Bancarias';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `entidades`
--

LOCK TABLES `entidades` WRITE;
/*!40000 ALTER TABLE `entidades` DISABLE KEYS */;
/*!40000 ALTER TABLE `entidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factulinea`
--

DROP TABLE IF EXISTS `factulinea`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `factulinea` (
  `codfactura` int(11) NOT NULL,
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `cantidad` int(11) default NULL,
  `precio` int(11) default NULL,
  `importe` int(11) default NULL,
  `dcto` tinyint(4) NOT NULL,
  PRIMARY KEY  (`codfactura`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='lineas de facturas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `factulinea`
--

LOCK TABLES `factulinea` WRITE;
/*!40000 ALTER TABLE `factulinea` DISABLE KEYS */;
/*!40000 ALTER TABLE `factulinea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factulineap`
--

DROP TABLE IF EXISTS `factulineap`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `factulineap` (
  `codfactura` varchar(20) NOT NULL default '',
  `codproveedor` int(5) NOT NULL,
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `cantidad` int(11) default NULL,
  `precio` int(11) default NULL,
  `importe` int(11) default NULL,
  `dcto` tinyint(4) NOT NULL,
  PRIMARY KEY  (`codfactura`,`codproveedor`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='lineas de facturas de proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `factulineap`
--

LOCK TABLES `factulineap` WRITE;
/*!40000 ALTER TABLE `factulineap` DISABLE KEYS */;
/*!40000 ALTER TABLE `factulineap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factulineaptmp`
--

DROP TABLE IF EXISTS `factulineaptmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `factulineaptmp` (
  `codfactura` int(11) NOT NULL,
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `cantidad` int(11) default NULL,
  `precio` int(11) default NULL,
  `importe` int(11) default NULL,
  `dcto` tinyint(4) NOT NULL,
  PRIMARY KEY  (`codfactura`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='lineas de facturas de proveedores temporal';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `factulineaptmp`
--

LOCK TABLES `factulineaptmp` WRITE;
/*!40000 ALTER TABLE `factulineaptmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `factulineaptmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factulineatmp`
--

DROP TABLE IF EXISTS `factulineatmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `factulineatmp` (
  `codfactura` int(11) NOT NULL,
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `cantidad` int(11) default NULL,
  `precio` int(11) default NULL,
  `importe` int(11) default NULL,
  `dcto` tinyint(4) NOT NULL,
  PRIMARY KEY  (`codfactura`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Temporal de linea de facturas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `factulineatmp`
--

LOCK TABLES `factulineatmp` WRITE;
/*!40000 ALTER TABLE `factulineatmp` DISABLE KEYS */;
INSERT INTO `factulineatmp` VALUES (4,1,1,'1',1,4622,4622,0),(5,1,1,'2',1,51547,51547,0),(11,1,1,'1',1,5500,5500,0),(8,1,1,'17',1,71428,71428,0),(18,1,1,'85',1,8400,6720,20),(19,1,1,'85',1,8400,8400,0),(20,1,1,'85',1,8400,7392,12),(22,1,1,'32',1,26890,26890,0),(24,1,1,'23',1,210000,210000,0),(26,1,1,'85',1,8400,8400,0),(28,1,1,'130',1,116806,116806,0),(33,1,1,'10',1,35294,35294,0),(29,2,1,'129',1,75630,75630,0),(38,1,1,'63',1,25000,25000,0),(39,1,1,'5',1,13025,13025,0),(42,1,1,'158',1,11260,11260,0),(42,2,1,'159',1,8319,8319,0),(42,3,1,'160',1,8320,8320,0),(43,1,1,'158',1,11260,11260,0),(43,2,1,'159',1,8319,8319,0),(43,3,1,'161',1,23529,23529,0);
/*!40000 ALTER TABLE `factulineatmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturas`
--

DROP TABLE IF EXISTS `facturas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facturas` (
  `codfactura` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL,
  `iva` int(8) NOT NULL default '0',
  `codcliente` int(5) NOT NULL,
  `estado` varchar(1) NOT NULL default '0',
  `totalfactura` int(11) default NULL,
  `fechavencimiento` date NOT NULL default '0000-00-00',
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codfactura`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='facturas de ventas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facturas`
--

LOCK TABLES `facturas` WRITE;
/*!40000 ALTER TABLE `facturas` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturasp`
--

DROP TABLE IF EXISTS `facturasp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facturasp` (
  `codfactura` varchar(20) NOT NULL default '',
  `codproveedor` int(5) NOT NULL,
  `fecha` date NOT NULL,
  `iva` tinyint(4) NOT NULL,
  `estado` varchar(1) NOT NULL default '0',
  `totalfactura` int(11) default NULL,
  `fechapago` date NOT NULL default '0000-00-00',
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codfactura`,`codproveedor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='facturas de compras a proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facturasp`
--

LOCK TABLES `facturasp` WRITE;
/*!40000 ALTER TABLE `facturasp` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturasp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturasptmp`
--

DROP TABLE IF EXISTS `facturasptmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facturasptmp` (
  `codfactura` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL,
  PRIMARY KEY  (`codfactura`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='temporal de facturas de proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facturasptmp`
--

LOCK TABLES `facturasptmp` WRITE;
/*!40000 ALTER TABLE `facturasptmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturasptmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturastmp`
--

DROP TABLE IF EXISTS `facturastmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facturastmp` (
  `codfactura` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL,
  PRIMARY KEY  (`codfactura`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='temporal de facturas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facturastmp`
--

LOCK TABLES `facturastmp` WRITE;
/*!40000 ALTER TABLE `facturastmp` DISABLE KEYS */;
INSERT INTO `facturastmp` VALUES (1,'0000-00-00'),(2,'0000-00-00'),(3,'0000-00-00'),(4,'0000-00-00'),(5,'0000-00-00'),(6,'0000-00-00'),(7,'0000-00-00'),(8,'0000-00-00'),(9,'0000-00-00'),(10,'0000-00-00'),(11,'0000-00-00'),(12,'0000-00-00'),(13,'0000-00-00'),(14,'0000-00-00'),(15,'0000-00-00'),(16,'0000-00-00'),(17,'0000-00-00'),(18,'0000-00-00'),(19,'0000-00-00'),(20,'0000-00-00'),(21,'0000-00-00'),(22,'0000-00-00'),(23,'0000-00-00'),(24,'0000-00-00'),(25,'0000-00-00'),(26,'0000-00-00'),(27,'0000-00-00'),(28,'0000-00-00'),(29,'0000-00-00'),(30,'0000-00-00'),(31,'0000-00-00'),(32,'0000-00-00'),(33,'0000-00-00'),(34,'0000-00-00'),(35,'0000-00-00'),(36,'0000-00-00'),(37,'0000-00-00'),(38,'0000-00-00'),(39,'0000-00-00'),(40,'0000-00-00'),(41,'0000-00-00'),(42,'0000-00-00'),(43,'0000-00-00'),(44,'0000-00-00');
/*!40000 ALTER TABLE `facturastmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familias`
--

DROP TABLE IF EXISTS `familias`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `familias` (
  `codfamilia` int(5) NOT NULL auto_increment,
  `nombre` varchar(20) default NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codfamilia`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='familia de articulos';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `familias`
--

LOCK TABLES `familias` WRITE;
/*!40000 ALTER TABLE `familias` DISABLE KEYS */;
INSERT INTO `familias` VALUES (1,'Sector 01','0');
/*!40000 ALTER TABLE `familias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formapago`
--

DROP TABLE IF EXISTS `formapago`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `formapago` (
  `codformapago` int(2) NOT NULL auto_increment,
  `nombrefp` varchar(40) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codformapago`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Forma de pago';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `formapago`
--

LOCK TABLES `formapago` WRITE;
/*!40000 ALTER TABLE `formapago` DISABLE KEYS */;
/*!40000 ALTER TABLE `formapago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `impuestos`
--

DROP TABLE IF EXISTS `impuestos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `impuestos` (
  `codimpuesto` int(3) NOT NULL auto_increment,
  `nombre` varchar(20) default NULL,
  `valor` float NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codimpuesto`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='tipos de impuestos';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `impuestos`
--

LOCK TABLES `impuestos` WRITE;
/*!40000 ALTER TABLE `impuestos` DISABLE KEYS */;
/*!40000 ALTER TABLE `impuestos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `librodiario`
--

DROP TABLE IF EXISTS `librodiario`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `librodiario` (
  `id` int(8) NOT NULL auto_increment,
  `fecha` date NOT NULL default '0000-00-00',
  `tipodocumento` varchar(1) NOT NULL,
  `coddocumento` varchar(20) NOT NULL,
  `codcomercial` int(5) NOT NULL,
  `codformapago` int(2) NOT NULL,
  `numpago` varchar(30) NOT NULL,
  `total` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Movimientos diarios';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `librodiario`
--

LOCK TABLES `librodiario` WRITE;
/*!40000 ALTER TABLE `librodiario` DISABLE KEYS */;
/*!40000 ALTER TABLE `librodiario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagos`
--

DROP TABLE IF EXISTS `pagos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pagos` (
  `id` int(11) NOT NULL auto_increment,
  `codfactura` varchar(20) NOT NULL,
  `codproveedor` int(5) NOT NULL,
  `importe` int(11) default NULL,
  `codformapago` int(2) NOT NULL,
  `numdocumento` varchar(30) NOT NULL,
  `fechapago` date default '0000-00-00',
  `observaciones` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Pagos de facturas a proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pagos`
--

LOCK TABLES `pagos` WRITE;
/*!40000 ALTER TABLE `pagos` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `proveedores` (
  `codproveedor` int(5) NOT NULL auto_increment,
  `nombre` varchar(45) NOT NULL,
  `nif` varchar(12) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `codprovincia` int(2) NOT NULL,
  `localidad` varchar(35) NOT NULL,
  `codentidad` int(2) NOT NULL,
  `cuentabancaria` varchar(20) NOT NULL,
  `telefono` varchar(14) NOT NULL,
  `movil` varchar(14) NOT NULL,
  `email` varchar(35) NOT NULL,
  `web` varchar(45) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  `codpostal` varchar(255) default NULL,
  PRIMARY KEY  (`codproveedor`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='Proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES (1,'Trucktec','1111111111','alemania',0,'hamburgo',0,'123365478','124565','124565','','','0','exportadores'),(2,'MGS REPUESTOS','76200860-2','SAN PABLO 2203',0,'SANTIAGO',0,'0020036110','2499033','6965923','VENTAS@MGSREPUESTOS.CL','100212000','0','COMERCIALIZACION'),(3,'comercial ecuador','7253395-k','ave, ecuador 4287',0,'SANTIAGO',0,'03229741','02-6771820','02-6771800','cecuador@comercialecuador.cl','www.comercialecuador.cl','0','importadora de repuestos'),(4,'rene smok landeros','5202390-4','padre alberto hurtado 366',0,'SANTIAGO',0,'5202390-4','027788912','027788912','ventas@renesmok.cl','www.renesmok.cl','0','importadoda de repuestos'),(5,'dimbel ltda','76367550-5','mapocho 3901',0,'SANTIAGO',0,'76367550-5','02-7927700','02-7248211','repuestos3901@vtr.net','','0','importadora de repuestos'),(6,'importadora y comercial frent part ltda','77507050-1','av.lib.bdo.o\'higgins 4110',0,'SANTIAGO',0,'77507050-1','779267','779267','','','0','importadora'),(7,'union tecnica automotriz s. a .','92379000-4','calle caletera 3420',0,'concepcion',0,'9237900-4','41-2279186','41-2399226','','','0','importadora de repuestos'),(8,'importadora mer-ben','86025900-1','tucapel 1465',0,'concepcion',0,'86025900-1','41-2212303','41-2224779','','','0','importadora'),(9,'landero e hijo limitada','85799700-k','av. padre alberto hurtado 351',0,'SANTIAGO',0,'85799700-k','7764120','7764224','ventas@landero.cl','','0','importadora'),(10,'comercial rihe limitada','79703410-k','av. libertador bernardo o´higins 4196',0,'SANTIAGO',0,'79703410-k','7797014','7797014','','','0','importadora'),(11,'macho diesel limitada','78620710-6','av. 5 de abril 3731',0,'SANTIAGO',0,'78620810-6','7793227','7793227','','','0','importadora');
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `provincias` (
  `codprovincia` int(2) NOT NULL auto_increment,
  `nombreprovincia` varchar(47) NOT NULL default '',
  PRIMARY KEY  (`codprovincia`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Provincias';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `provincias`
--

LOCK TABLES `provincias` WRITE;
/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ubicaciones`
--

DROP TABLE IF EXISTS `ubicaciones`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ubicaciones` (
  `codubicacion` int(3) NOT NULL auto_increment,
  `nombre` varchar(50) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codubicacion`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Ubicaciones';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ubicaciones`
--

LOCK TABLES `ubicaciones` WRITE;
/*!40000 ALTER TABLE `ubicaciones` DISABLE KEYS */;
INSERT INTO `ubicaciones` VALUES (1,'Bandeja 7','0');
/*!40000 ALTER TABLE `ubicaciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-11-22 23:20:36
