-- MySQL dump 10.11
--
-- Host: localhost    Database: codeka
-- ------------------------------------------------------
-- Server version	5.0.51b-community-nt-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `albalinea`
--

DROP TABLE IF EXISTS `albalinea`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albalinea` (
  `codalbaran` int(11) NOT NULL default '0',
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) default NULL,
  `codigo` varchar(15) character set utf8 default NULL,
  `cantidad` float NOT NULL default '0',
  `precio` float NOT NULL default '0',
  `importe` float NOT NULL default '0',
  `dcto` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albalinea`
--

LOCK TABLES `albalinea` WRITE;
/*!40000 ALTER TABLE `albalinea` DISABLE KEYS */;
/*!40000 ALTER TABLE `albalinea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albalineap`
--

DROP TABLE IF EXISTS `albalineap`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albalineap` (
  `codalbaran` varchar(20) NOT NULL default '0',
  `codproveedor` int(5) NOT NULL default '0',
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) default NULL,
  `codigo` varchar(15) default NULL,
  `cantidad` float NOT NULL default '0',
  `precio` float NOT NULL default '0',
  `importe` float NOT NULL default '0',
  `dcto` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`codproveedor`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albalineap`
--

LOCK TABLES `albalineap` WRITE;
/*!40000 ALTER TABLE `albalineap` DISABLE KEYS */;
/*!40000 ALTER TABLE `albalineap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albalineaptmp`
--

DROP TABLE IF EXISTS `albalineaptmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albalineaptmp` (
  `codalbaran` int(11) NOT NULL default '0',
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) default NULL,
  `codigo` varchar(15) default NULL,
  `cantidad` float NOT NULL default '0',
  `precio` float NOT NULL default '0',
  `importe` float NOT NULL default '0',
  `dcto` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albalineaptmp`
--

LOCK TABLES `albalineaptmp` WRITE;
/*!40000 ALTER TABLE `albalineaptmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albalineaptmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albalineatmp`
--

DROP TABLE IF EXISTS `albalineatmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albalineatmp` (
  `codalbaran` int(11) NOT NULL default '0',
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) default NULL,
  `codigo` varchar(15) character set utf8 default NULL,
  `cantidad` float NOT NULL default '0',
  `precio` float NOT NULL default '0',
  `importe` float NOT NULL default '0',
  `dcto` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albalineatmp`
--

LOCK TABLES `albalineatmp` WRITE;
/*!40000 ALTER TABLE `albalineatmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albalineatmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albaranes`
--

DROP TABLE IF EXISTS `albaranes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albaranes` (
  `codalbaran` int(11) NOT NULL auto_increment,
  `codfactura` int(11) NOT NULL default '0',
  `fecha` date NOT NULL default '0000-00-00',
  `iva` tinyint(4) NOT NULL default '0',
  `codcliente` int(5) default '0',
  `estado` varchar(1) character set utf8 default '1',
  `totalalbaran` float NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albaranes`
--

LOCK TABLES `albaranes` WRITE;
/*!40000 ALTER TABLE `albaranes` DISABLE KEYS */;
/*!40000 ALTER TABLE `albaranes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albaranesp`
--

DROP TABLE IF EXISTS `albaranesp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albaranesp` (
  `codalbaran` varchar(20) NOT NULL default '0',
  `codproveedor` int(5) NOT NULL default '0',
  `codfactura` varchar(20) default NULL,
  `fecha` date NOT NULL default '0000-00-00',
  `iva` tinyint(4) NOT NULL default '0',
  `estado` varchar(1) default '1',
  `totalalbaran` float NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`codproveedor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albaranesp`
--

LOCK TABLES `albaranesp` WRITE;
/*!40000 ALTER TABLE `albaranesp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albaranesp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albaranesptmp`
--

DROP TABLE IF EXISTS `albaranesptmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albaranesptmp` (
  `codalbaran` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`codalbaran`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Temporal de albaranes de proveedores para controlar acceso s';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albaranesptmp`
--

LOCK TABLES `albaranesptmp` WRITE;
/*!40000 ALTER TABLE `albaranesptmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albaranesptmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albaranestmp`
--

DROP TABLE IF EXISTS `albaranestmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albaranestmp` (
  `codalbaran` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`codalbaran`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Temporal de albaranes para controlar acceso simultaneo';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albaranestmp`
--

LOCK TABLES `albaranestmp` WRITE;
/*!40000 ALTER TABLE `albaranestmp` DISABLE KEYS */;
INSERT INTO `albaranestmp` VALUES (1,'2011-10-20');
/*!40000 ALTER TABLE `albaranestmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articulos`
--

DROP TABLE IF EXISTS `articulos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `articulos` (
  `codarticulo` int(10) NOT NULL auto_increment,
  `codfamilia` int(5) NOT NULL,
  `referencia` varchar(20) NOT NULL,
  `descripcion` text NOT NULL,
  `impuesto` int(11) default NULL,
  `codproveedor1` int(5) NOT NULL default '1',
  `codproveedor2` int(5) NOT NULL,
  `descripcion_corta` varchar(30) NOT NULL,
  `codubicacion` int(3) NOT NULL,
  `stock` int(10) NOT NULL,
  `stock_minimo` int(8) NOT NULL,
  `aviso_minimo` varchar(1) NOT NULL default '0',
  `datos_producto` varchar(200) NOT NULL,
  `fecha_alta` date NOT NULL default '0000-00-00',
  `codembalaje` int(3) NOT NULL,
  `unidades_caja` int(8) NOT NULL,
  `precio_ticket` varchar(1) NOT NULL default '0',
  `modificar_ticket` varchar(1) NOT NULL default '0',
  `observaciones` text NOT NULL,
  `precio_compra` int(10) default NULL,
  `precio_almacen` int(10) default NULL,
  `precio_tienda` int(10) default NULL,
  `precio_pvp` int(10) default NULL,
  `precio_iva` int(10) default NULL,
  `codigobarras` varchar(15) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codarticulo`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='Articulos';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `articulos`
--

LOCK TABLES `articulos` WRITE;
/*!40000 ALTER TABLE `articulos` DISABLE KEYS */;
INSERT INTO `articulos` VALUES (1,1,'azsxdvfgb','dxcfvgbn',5,1,0,'dxcfvgjn',1,-3,1,'1','fcvgbn','2011-10-12',1,5,'0','','fcvgb',23,345,45,5000,89,'8400000000017','foto1.jpg','1'),(2,1,'ddd','dddd',5,0,0,'cccccc',1,0,0,'0','','0000-00-00',1,0,'0','0','',0,0,0,600,0,'8400000000024','foto2.jpg','1'),(3,1,'dfdf','sdfs',0,4,0,'',1,0,0,'0','','0000-00-00',0,0,'','','',NULL,NULL,50,50,0,'8400000000031','foto3.jpg','1'),(4,2,'dfdfd','fdfdfd',0,4,0,'',1,9,2,'1','','2011-10-20',0,0,'','','123',NULL,NULL,23,700,NULL,'8400000000048','foto4.jpg','1'),(5,2,'wef','efr',0,4,0,'',1,-7,5,'1','','2011-10-03',0,0,'','','sdfgdfgdfg',NULL,NULL,23,0,NULL,'8400000000055','foto5.jpg','1'),(6,1,'dffde','locura',0,6,0,'',1,-12,2,'0','','2011-10-20',0,0,'','','wert',NULL,NULL,35,3,0,'8400000000062','foto6.jpg','1'),(7,1,'12','qwdesdsd',0,4,0,'',1,2,2,'1','','2011-10-04',0,0,'','','wdswds',NULL,NULL,1213,2345,NULL,'8400000000079','foto7.jpg','1'),(8,3,'rodamiento','asdfg',0,6,0,'',1,-4,3,'1','','2011-10-26',0,0,'','','kmjdhfdks',NULL,NULL,500,1200,0,'8400000000086','foto8.jpg','1'),(9,3,'1234567','stevia de las mercedes y wea',0,0,0,'',1,2,2,'0','','2011-10-13',0,0,'','','dcsdcsdcsdc',NULL,NULL,90,500,0,'8400000000093','foto9.jpg','0'),(10,3,'azsxdvgjmk,','deftgyujk',0,8,0,'',1,-2,12,'1','','2011-10-20',0,0,'','','wsecybhj',NULL,NULL,1213,1230,NULL,'8400000000109','foto10.jpg','1'),(11,3,'12345','repuesto camion tolba de la mercedez',0,6,0,'',1,18,3,'1','','2011-10-28',0,0,'','','cada diez de tres',NULL,NULL,5000,70000,NULL,'8400000000116','foto11.jpg','1'),(12,3,'0040942404','filtro aire actros',0,10,0,'',1,-37,2,'1','','2011-10-20',0,0,'','','este es chino',NULL,NULL,30000,50000,0,'8400000000123','foto12.jpg','0'),(13,4,'swedrtfgyhu','sdrtfgyu',0,6,0,'',1,20,2,'1','','2011-10-27',0,0,'','','azesxcybh',NULL,NULL,5000,70000,NULL,'8400000000130','foto13.jpg','0'),(14,4,'dxcfghj','dxfcfvg',0,6,0,'',1,0,0,'0','','0000-00-00',0,0,'','','',NULL,NULL,0,0,NULL,'8400000000147','foto14.jpg','1'),(15,4,'sedtfygujik','dxcfgvjnmk',0,0,0,'',1,0,0,'0','','0000-00-00',0,0,'','','',NULL,NULL,0,0,NULL,'8400000000154','foto15.jpg','1'),(16,3,'drfgvhjn','dfcghjnk',0,0,0,'',1,-4,0,'0','','0000-00-00',0,0,'','','',NULL,NULL,0,0,NULL,'8400000000161','foto16.jpg','0'),(17,4,'asdxcfvgbhnj','dxfvj',0,6,0,'',1,10,12,'1','','2011-10-20',0,0,'','','dertgyj',NULL,NULL,7890,909876,NULL,'8400000000178','foto17.jpg','0'),(18,4,'1234568999','marihuana ke sea de las mercedez',0,8,0,'',1,23,2,'1','','2011-10-27',0,0,'','','es chino',NULL,NULL,34000,50000,NULL,'8400000000185','foto18.jpg','0'),(19,4,'123456','sdfgthj',0,0,0,'',1,0,0,'1','','0000-00-00',0,0,'','','',NULL,NULL,0,0,NULL,'8400000000192','foto19.jpg','1'),(20,0,'','',0,0,0,'',0,0,0,'1','','0000-00-00',0,0,'','','',NULL,NULL,0,0,NULL,'8400000000208','foto20.jpg','1'),(21,0,'dfghj','dfvgbn',0,0,0,'',0,60,20,'1','','2011-11-09',0,0,'','','',NULL,NULL,2345,324567,NULL,'8400000000215','foto21.jpg','1'),(22,4,'sdfcvbn','dffcvbhnj',0,23,0,'',1,102,20,'1','','2011-11-10',0,0,'','','aswdrtgjnmk,',NULL,NULL,80000,100000,NULL,'8400000000222','foto22.jpg','0'),(23,4,'000000','espe3ctorante jarabe palto miel',0,0,0,'',0,27,1,'1','','2011-11-18',0,0,'','','',NULL,NULL,80000,90000,NULL,'8400000000239','foto23.jpg','0'),(24,3,'eeeeeeeeee','pila 333 a',0,23,0,'',1,-2,30,'1','','2011-11-09',0,0,'','','kiero saber loke estas haciendo',NULL,NULL,60000,600000,NULL,'8400000000246','foto24.jpg','0'),(25,4,'sgfhffdsgh','dfgdfg',0,23,0,'',1,895,600,'1','','2011-11-10',0,0,'','','swectvgynhj',NULL,NULL,70000,700000,NULL,'8400000000253','foto25.jpg','0'),(26,4,'09898762','rodamiento para camion',0,23,0,'',1,30,5,'0','','2011-11-10',0,0,'','','este producto es chino',NULL,NULL,20000,50000,NULL,'8400000000260','foto26.jpg','0');
/*!40000 ALTER TABLE `articulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artpro`
--

DROP TABLE IF EXISTS `artpro`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `artpro` (
  `codarticulo` varchar(15) NOT NULL,
  `codfamilia` int(3) NOT NULL,
  `codproveedor` int(5) NOT NULL,
  `precio` int(11) default NULL,
  PRIMARY KEY  (`codarticulo`,`codfamilia`,`codproveedor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `artpro`
--

LOCK TABLES `artpro` WRITE;
/*!40000 ALTER TABLE `artpro` DISABLE KEYS */;
/*!40000 ALTER TABLE `artpro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `clientes` (
  `codcliente` int(5) NOT NULL auto_increment,
  `nombre` varchar(45) NOT NULL,
  `nif` varchar(12) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `codprovincia` int(2) NOT NULL default '0',
  `localidad` varchar(35) NOT NULL,
  `codformapago` int(2) NOT NULL default '0',
  `codentidad` int(2) NOT NULL default '0',
  `cuentabancaria` varchar(20) NOT NULL,
  `codpostal` varchar(5) NOT NULL,
  `telefono` varchar(14) NOT NULL,
  `movil` varchar(14) NOT NULL,
  `email` varchar(35) NOT NULL,
  `web` varchar(45) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codcliente`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COMMENT='Clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'jaime','1234566-3','frgbhnj',1,'sxdvfg',0,0,'12345','2345','1234','1234','12345','1234','0'),(2,'raja','12345','wsetfrgvybhnj',3,'xdcfvg',0,0,'123456','12345','123456','1234','123456','123456','0'),(3,'dcc','cdc','dcdc',3,'dcdc',0,0,'','dcdc','cdc','cdc','cdc','cdc','1'),(4,'sdsd','sdsd','sdsd',2,'dsdsd',0,0,'','dsd','12345','2345','sdsds','dsdsd','1'),(5,'marihuana','16394698-2','bbbj',0,'nnnnn',0,0,'','nbnbn','jjjj','jjjjjj','fffff','ffffff','0'),(6,'armonil','1234566-3','fvfv',0,'vfv',0,0,'','vfvfv','vfvf','vfvffv','vfvfv','vfvfvfv','0'),(7,'zetapon','lococ','ccc',0,'xcxc',0,0,'','xcxc','cxcx','cxcxc','xcxc','cxcc','0'),(8,'alberto xapaspurri demaisaod esteril','13456879-2','los quimbayas 428',0,'los angeles',0,0,'','gbbbh','bbhbh','adentu','vgvgvgvg','vvgggvgvggv','0'),(9,'ramiro arias','q2345678','wsedfvtgbj',8,'derfgvbnjk',0,0,'','diseñ','234567','adentu',' 0','dfghj','1'),(10,'marcelo sakjdjhskjd','1234567','defghj',3,'sdfghjn',0,0,'','looka','23456','fgh','esdrftygy','dfghj','0'),(11,'ddfgfd','cvxcvx','cvcvxv',15,'xvxv',0,0,'','cvxcv','xcvxcv','cvxv','cvxv','cxvvxvx','0'),(12,'dcsc','sfzcz','xczc',0,'zxczcz',0,0,'','xczc','xzczc','zxczc','xczc','zxczczczcczczcz','1'),(13,'aswdrftgy','edfgbhnj','edfgyh',0,'dxcfvgbj',0,0,'','dfcvg','dfgvbhn','dfgh','dfgvhb','dcfgvbnj','0'),(14,'dxcfbhnjm','fdghbnj','1234567',2,'wsedrfg',0,0,'','sdcfv','1234567','sdxcfvgbjnhkm','dfgvjkmn,','dcfvgbnhjm','0'),(15,'szdfvjn','dxfghbjn','fghjnmk,',2,'fghjnkm',0,0,'','dxfgh','fcghjnmk','fghjmk','dfghjkm','dfghjk,','0'),(16,'dxcfgjn','dfghjkl','cdfvgjnmk',11,'fdghjklñ',0,0,'','dfghj','dfghjnkm','fcghjnkm','fghjkl','dxfghjkl','0'),(17,'swdrtgyuk','drftgyuk','drftgyujk',2,'rftgyuhijk',0,0,'','fdgbh','rfghujk','dfghujk','dcfvgnjmk','drftgjk','1'),(18,'pablo san martin','15953827-3','doctor cossio 490',8,'los angeles',0,0,'','indep','74729444','jormat','pablojormat@hotmail.com','www.repuestosjormat.cl','0'),(19,'jaime','','',0,'',0,0,'','','','','','','0'),(20,'jota','','',0,'',0,0,'','','','','','','1'),(21,'marcelo de las mercedez','','',0,'',0,0,'','','','','','','0'),(22,'rosario','245349','dxcfvgbh',3,'los angeles',0,0,'','','','','','adentu','0'),(23,'','','xdcfvgbhn',0,'',0,0,'','','','','','','1'),(24,'jasime','16396196','',0,'',0,0,'','','','','','','0'),(25,'rosamel','12345634','',0,'',0,0,'','','','','','','0'),(26,'roberto cereceda','123456','',0,'',0,0,'','','','','','','0'),(27,'awsderftgh','123456','',0,'',0,0,'','','','','','','0'),(28,'longi','esperanza','marihuancoa',0,'',0,0,'','','','','','','0'),(29,'xcvb','cvbn','',0,'',0,0,'','','','','','','0'),(30,'renata ramos','locura','',0,'',0,0,'','','','','','','0'),(31,'hjkl','arriba','',0,'',0,0,'','','','','','','1'),(32,'javier pascal','longavi','vbnm,',3,'gbhnjm,',0,0,'','loco ','ertyui','dfvgbhnjm','','','0'),(33,'dcdcdc','','',0,'',0,0,'','','','','','','1'),(34,'ccvcc','xxx','',0,'',0,0,'','','','','','','1'),(35,'xxcxc','xxxx','',0,'',0,0,'','','','','','','0'),(36,'ffvg','dfd','',0,'',0,0,'','','','','','','0'),(37,'','df','',0,'',0,0,'','','','','','','1'),(38,'','','',0,'',0,0,'','','','','','','1'),(39,'','hjhh','',0,'',0,0,'','','','','','','1'),(40,'','','',0,'',0,0,'','','','','','','1'),(41,'','','',0,'',0,0,'','','','','','','1'),(42,'aswdfghj','123456','swdfghj',0,'',0,0,'','','','','','','0'),(43,'perez','1234567','sdxfvbn',0,'dxcvbnj',0,0,'','','','','','','0'),(44,'dxfghjn','1234567dcfvg','',0,'',0,0,'','','','','','','0'),(45,'dvdvf','ccvcvc','cvcv',0,'dfvdfd',0,0,'','','','','','','0'),(46,'jaime salazar','fcgbhn','',0,'',0,0,'','','','','','','0'),(47,'jota oses','16.396.196-2','',0,'',0,0,'','','','','','','0'),(48,'dfdfd','dfd-f','',0,'',0,0,'','','','','','','0');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cobros`
--

DROP TABLE IF EXISTS `cobros`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `cobros` (
  `id` int(11) NOT NULL auto_increment,
  `codfactura` int(11) NOT NULL,
  `codcliente` int(5) NOT NULL,
  `importe` int(11) default NULL,
  `codformapago` int(2) NOT NULL,
  `numdocumento` varchar(30) NOT NULL,
  `fechacobro` date NOT NULL default '0000-00-00',
  `observaciones` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='Cobros de facturas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `cobros`
--

LOCK TABLES `cobros` WRITE;
/*!40000 ALTER TABLE `cobros` DISABLE KEYS */;
INSERT INTO `cobros` VALUES (1,2,1,6,1,'','2011-10-18',''),(2,1,1,3000,1,'','2011-10-18',''),(3,1,1,2000,1,'','2011-10-18',''),(4,1,1,3000,1,'','2011-10-18',''),(5,1,1,6967,1,'','2011-10-18',''),(6,2,1,5,1,'','2011-10-18',''),(7,4,1,139,1,'','2011-10-20',''),(8,9,2,400,1,'2','2011-10-20','sdsd'),(9,10,1,0,1,'','2011-10-20',''),(10,13,1,0,1,'','2011-10-21',''),(11,14,1,0,1,'','2011-10-21',''),(12,3,1,8120,1,'2','2011-10-21',''),(13,23,10,2790,1,'','2011-10-21',''),(14,25,1,18,1,'','2011-10-26',''),(15,26,7,8,1,'','2011-10-26',''),(16,26,7,8156,1,'','2011-10-26',''),(17,30,1,3,3,'','2011-10-27',''),(18,31,1,580,1,'','2011-10-27',''),(19,34,1,3,1,'','2011-10-27',''),(20,46,1,6,1,'','2011-10-27',''),(21,50,1,3,1,'','2011-10-28','se cancelo'),(22,50,1,0,1,'','2011-10-28',''),(23,54,1,83304,1,'','2011-10-28',''),(24,54,1,0,1,'','2011-10-28',''),(25,58,1,119,3,'','2011-10-28',''),(26,57,1,83300,1,'','2011-10-28',''),(27,4,1,139805,3,'','2011-10-28',''),(28,59,8,178,2,'','2011-10-29',''),(29,56,1,4,3,'','2011-10-29',''),(30,56,1,4,3,'','2011-10-29',''),(31,70,1234563,0,1,'','2011-10-29',''),(32,71,1234563,1,1,'','2011-10-29',''),(33,72,16394696,1,1,'','2011-10-29',''),(34,73,16394696,59,1,'','2011-10-29',''),(35,74,16394696,59,1,'','2011-10-29',''),(36,93,12345,1,1,'','2011-10-29',''),(37,94,1,1,1,'','2011-10-29',''),(38,95,7,84,1,'','2011-10-30',''),(39,166,8,226,1,'','2011-11-10',''),(40,168,1,0,1,'','2011-11-17',''),(41,169,1,714,1,'','2011-11-10',''),(42,170,10,714,1,'','2011-11-10',''),(43,171,6,119,1,'','2011-11-10',''),(44,171,6,118881,1,'','2011-11-10',''),(45,172,1,0,3,'','2011-11-10',''),(46,172,1,0,1,'','2011-11-10',''),(47,173,1,59500,3,'','2011-11-10',''),(48,177,1,119,1,'','2011-11-16',''),(49,197,1,59,1,'','2011-11-21',''),(50,201,5,58000,3,'','2011-11-21','wert'),(51,200,1,59500,3,'','2011-11-21','fcvbnjmk'),(52,204,1,59,1,'','2011-11-21',''),(53,205,1,59,1,'','2011-11-21',''),(54,210,1,58000,1,'','2011-11-21',''),(55,211,1,59500,3,'','2011-11-21',''),(56,212,1,773500,1,'','2011-11-21',''),(57,224,0,59500,1,'','2011-11-23',''),(58,225,10,714000,1,'','2011-11-23','');
/*!40000 ALTER TABLE `cobros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `embalajes`
--

DROP TABLE IF EXISTS `embalajes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `embalajes` (
  `codembalaje` int(3) NOT NULL auto_increment,
  `nombre` varchar(30) default NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codembalaje`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Embalajes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `embalajes`
--

LOCK TABLES `embalajes` WRITE;
/*!40000 ALTER TABLE `embalajes` DISABLE KEYS */;
INSERT INTO `embalajes` VALUES (1,'cincuenta','0');
/*!40000 ALTER TABLE `embalajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entidades`
--

DROP TABLE IF EXISTS `entidades`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `entidades` (
  `codentidad` int(2) NOT NULL auto_increment,
  `nombreentidad` varchar(50) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codentidad`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Entidades Bancarias';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `entidades`
--

LOCK TABLES `entidades` WRITE;
/*!40000 ALTER TABLE `entidades` DISABLE KEYS */;
INSERT INTO `entidades` VALUES (5,'Banco Credito Inversiones','0'),(1,'Banco Chile','0'),(2,'Banco Bice','0'),(3,'Banco Santander','0'),(6,'Banco Itaú','0'),(7,'Banco Estado','0'),(8,'Banaco del Desarrollo','0'),(9,'marihuana','0');
/*!40000 ALTER TABLE `entidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factulinea`
--

DROP TABLE IF EXISTS `factulinea`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `factulinea` (
  `codfactura` int(11) NOT NULL,
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `cantidad` int(11) default NULL,
  `precio` int(11) default NULL,
  `importe` int(11) default NULL,
  `dcto` int(3) NOT NULL default '0',
  PRIMARY KEY  (`codfactura`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='lineas de facturas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `factulinea`
--

LOCK TABLES `factulinea` WRITE;
/*!40000 ALTER TABLE `factulinea` DISABLE KEYS */;
INSERT INTO `factulinea` VALUES (1,1,1,'1',1,890,890,0),(2,1,1,'1',1,6000,6000,0),(3,1,1,'1',1,7000,7000,0),(4,1,1,'1',1,58800,58800,0),(4,2,1,'1',1,58800,58800,0),(9,1,1,'1',1,345,345,0),(11,1,2,'4',1,0,0,0),(14,1,2,'5',1,0,0,0),(15,1,1,'6',176,3,528,0),(16,1,1,'6',1,3,3,0),(16,2,1,'6',3,3,9,0),(18,1,2,'5',1,0,0,0),(23,1,2,'5',1,0,0,0),(23,2,1,'7',1,2345,2345,0),(25,1,1,'6',5,3,15,0),(25,2,2,'5',7,0,0,0),(26,1,1,'6',1,3,3,0),(26,2,1,'7',3,2345,7035,0),(27,1,3,'8',8,1200,9600,0),(28,1,1,'6',1,3,3,0),(29,1,1,'6',1,3,3,0),(30,1,1,'6',1,3,3,0),(31,1,3,'9',1,500,500,0),(32,1,3,'8',1,1200,1200,0),(33,1,1,'6',1,3,3,0),(34,1,1,'6',1,3,3,0),(35,1,1,'6',1,3,3,0),(36,1,3,'10',1,1230,1230,0),(37,1,1,'6',1,3,3,0),(37,2,3,'10',1,1230,1230,0),(37,3,3,'8',1,1200,1200,0),(38,1,1,'6',1,3,3,0),(39,1,1,'6',1,3,3,0),(40,1,1,'6',1,3,3,0),(41,1,1,'7',1,2345,2345,0),(41,2,1,'7',1,2345,2345,0),(41,3,3,'9',1,500,500,0),(41,4,3,'9',1,500,500,0),(42,1,1,'6',1,3,3,0),(43,1,1,'6',1,3,3,0),(46,1,1,'6',1,3,3,0),(46,2,1,'7',1,2345,2345,0),(46,3,3,'8',1,1200,1200,0),(46,4,3,'10',1,1230,1230,0),(46,5,3,'9',1,500,500,0),(48,1,1,'6',1,3,3,0),(49,1,1,'6',1,3,3,0),(49,2,1,'7',1,2345,2345,0),(49,3,3,'8',1,1200,1200,0),(50,1,1,'6',1,3,3,0),(51,1,3,'11',1,70000,70000,0),(52,1,1,'7',1,2345,2345,0),(53,1,3,'10',1,1230,1230,0),(54,1,1,'6',1,3,3,0),(54,2,3,'11',1,70000,70000,0),(55,1,3,'11',1,70000,70000,0),(56,1,1,'6',1,3,3,0),(57,1,3,'11',1,70000,70000,0),(58,1,3,'12',2,50000,100000,0),(58,2,3,'9',1,500,500,0),(59,1,3,'12',3,50000,150000,0),(60,1,3,'8',1,1200,1200,0),(61,1,3,'12',1,50000,50000,0),(62,1,3,'8',1,1200,1200,0),(63,1,3,'8',1,1200,1200,0),(64,1,3,'8',1,1200,1200,0),(65,1,3,'11',1,70000,70000,0),(66,1,3,'12',1,50000,50000,0),(67,1,3,'10',1,1230,1230,0),(68,1,3,'8',1,1200,1200,0),(69,1,3,'8',1,1200,1200,0),(70,1,3,'16',1,0,0,0),(71,1,3,'8',1,1200,1200,0),(72,1,3,'8',1,1200,1200,0),(73,1,3,'12',1,50000,50000,0),(74,1,3,'12',1,50000,50000,0),(75,1,3,'8',1,1200,1200,0),(76,1,3,'8',1,1200,1200,0),(77,1,3,'8',1,1200,1200,0),(78,1,3,'11',1,70000,70000,0),(79,1,3,'12',1,50000,50000,0),(80,1,3,'8',1,1200,1200,0),(81,1,3,'10',1,1230,1230,0),(82,1,3,'8',1,1200,1200,0),(83,1,3,'9',1,500,500,0),(84,1,3,'12',1,50000,50000,0),(85,1,3,'9',1,500,500,0),(86,1,3,'10',1,1230,1230,0),(87,1,3,'10',1,1230,1230,0),(88,1,3,'10',1,1230,1230,0),(89,1,3,'12',1,50000,50000,0),(90,1,3,'10',1,1230,1230,0),(91,1,3,'10',1,1230,1230,0),(92,1,4,'13',1,70000,70000,0),(93,1,3,'10',1,1230,1230,0),(94,1,3,'8',1,1200,1200,0),(95,1,3,'10',1,1230,1230,0),(95,2,3,'11',1,70000,70000,0),(97,1,3,'12',1,50000,50000,0),(98,1,3,'16',1,0,0,0),(98,2,3,'12',1,50000,50000,0),(99,1,4,'13',1,70000,70000,0),(100,1,4,'13',1,70000,70000,0),(101,1,3,'16',1,0,0,0),(101,2,3,'9',1,500,500,0),(102,1,4,'22',1,100000,100000,0),(103,1,3,'12',1,50000,50000,0),(104,1,3,'12',1,50000,50000,0),(105,1,3,'24',1,600000,600000,0),(106,1,3,'12',1,50000,50000,0),(107,1,3,'12',1,50000,50000,0),(108,1,3,'12',1,50000,50000,0),(109,1,4,'25',1,700000,700000,0),(110,1,3,'12',1,50000,50000,0),(111,1,3,'12',1,50000,50000,0),(112,1,4,'22',1,100000,100000,0),(113,1,4,'22',1,100000,100000,0),(114,1,3,'24',1,600000,600000,0),(115,1,3,'12',1,50000,50000,0),(116,1,3,'12',1,50000,50000,0),(117,1,3,'16',1,0,0,0),(117,2,3,'24',1,600000,600000,0),(118,1,4,'22',1,100000,100000,0),(119,1,4,'22',1,100000,100000,0),(120,1,3,'12',1,50000,50000,0),(121,1,3,'12',1,50000,50000,0),(122,1,3,'12',1,50000,50000,0),(123,1,3,'24',1,600000,600000,0),(124,2,4,'22',1,100000,100000,0),(125,2,4,'22',1,100000,100000,0),(126,1,4,'22',1,100000,100000,0),(127,1,4,'22',1,100000,100000,0),(128,1,4,'22',1,100000,100000,0),(129,1,4,'22',1,100000,100000,0),(130,1,4,'17',1,909876,909876,0),(131,1,4,'22',1,100000,100000,0),(132,1,4,'25',1,700000,700000,0),(133,1,3,'24',1,600000,600000,0),(134,1,3,'12',1,50000,50000,0),(135,1,4,'22',1,100000,100000,0),(136,1,3,'24',1,600000,600000,0),(137,1,4,'23',1,90000,90000,0),(138,1,4,'13',1,70000,70000,0),(139,1,3,'9',1,500,500,0),(140,1,3,'24',1,600000,600000,0),(141,1,4,'25',1,700000,700000,0),(142,1,3,'12',1,50000,50000,0),(143,1,4,'22',1,100000,100000,0),(144,1,3,'24',1,600000,600000,0),(145,1,3,'12',1,50000,50000,0),(146,1,3,'12',1,50000,50000,0),(147,1,4,'13',1,70000,70000,0),(148,1,4,'23',1,90000,90000,0),(149,1,3,'12',1,50000,50000,0),(150,1,3,'12',1,50000,50000,0),(150,2,4,'23',1,90000,90000,0),(151,1,3,'12',1,50000,50000,0),(151,2,3,'24',1,600000,600000,0),(152,1,4,'22',1,100000,100000,0),(153,1,4,'25',1,700000,700000,0),(154,1,3,'24',1,600000,600000,0),(155,1,3,'24',1,600000,600000,0),(156,1,4,'17',1,909876,909876,0),(157,1,3,'12',1,50000,50000,0),(158,1,3,'12',1,50000,50000,0),(161,1,3,'24',1,600000,600000,0),(162,1,0,'',1,0,0,0),(163,1,0,'',1,0,0,0),(163,2,3,'12',1,50000,50000,0),(163,3,3,'12',1,50000,50000,0),(164,1,3,'24',1,600000,600000,0),(166,1,4,'22',1,100000,100000,0),(166,2,4,'22',1,100000,90000,10),(169,1,3,'24',1,600000,600000,0),(170,1,3,'24',1,600000,600000,0),(171,1,4,'22',1,100000,100000,0),(172,1,3,'24',1,600000,600000,0),(173,1,3,'12',1,50000,50000,0),(176,1,3,'12',1,50000,50000,0),(176,2,3,'9',1,500,450,10),(177,1,4,'22',1,100000,100000,0),(178,1,3,'12',1,50000,50000,0),(179,1,3,'12',1,50000,50000,0),(180,1,4,'22',1,100000,100000,0),(183,1,3,'12',1,50000,50000,0),(184,1,3,'12',1,50000,50000,0),(185,1,3,'12',1,50000,50000,0),(186,1,3,'12',1,50000,50000,0),(189,1,3,'12',1,50000,50000,0),(191,1,3,'12',1,50000,50000,0),(194,1,3,'12',1,50000,50000,0),(195,1,3,'12',1,50000,50000,0),(196,1,3,'12',1,50000,50000,0),(197,1,3,'12',1,50000,50000,0),(198,1,3,'12',1,50000,50000,0),(199,1,3,'12',1,50000,50000,0),(200,1,3,'12',1,50000,50000,0),(201,1,3,'12',1,50000,50000,0),(202,1,3,'12',1,50000,50000,0),(203,1,3,'12',1,50000,50000,0),(204,1,3,'12',1,50000,50000,0),(205,1,3,'12',1,50000,50000,0),(206,1,4,'23',1,90000,90000,0),(207,1,4,'25',1,700000,700000,0),(208,1,3,'12',1,50000,50000,0),(209,1,3,'12',1,50000,50000,0),(210,1,3,'12',1,50000,50000,0),(211,1,3,'12',1,50000,50000,0),(212,1,3,'12',1,50000,50000,0),(212,2,3,'24',1,600000,600000,0),(213,1,3,'12',1,50000,50000,0),(214,1,3,'24',1,600000,600000,0),(215,1,3,'12',1,50000,50000,0),(216,1,4,'22',1,100000,100000,0),(217,1,3,'24',1,600000,600000,0),(0,1,3,'12',1,50000,50000,0),(218,1,3,'24',1,600000,600000,0),(219,1,3,'24',1,600000,600000,0),(220,1,3,'12',1,50000,50000,0),(221,1,3,'12',1,50000,50000,0),(222,1,3,'12',1,50000,50000,0),(223,1,4,'23',1,90000,90000,0),(224,1,3,'12',1,50000,50000,0),(225,1,3,'24',1,600000,600000,0),(226,2,3,'12',1,50000,50000,0),(226,1,3,'12',1,50000,50000,0),(226,3,3,'12',1,50000,50000,0),(227,1,3,'12',1,50000,50000,0),(228,1,3,'24',1,600000,600000,0);
/*!40000 ALTER TABLE `factulinea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factulineap`
--

DROP TABLE IF EXISTS `factulineap`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `factulineap` (
  `codfactura` varchar(20) NOT NULL default '',
  `codproveedor` int(5) NOT NULL,
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `cantidad` int(11) default NULL,
  `precio` int(11) default NULL,
  `importe` int(11) default NULL,
  `dcto` int(3) NOT NULL default '0',
  PRIMARY KEY  (`codfactura`,`codproveedor`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='lineas de facturas de proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `factulineap`
--

LOCK TABLES `factulineap` WRITE;
/*!40000 ALTER TABLE `factulineap` DISABLE KEYS */;
/*!40000 ALTER TABLE `factulineap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factulineaptmp`
--

DROP TABLE IF EXISTS `factulineaptmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `factulineaptmp` (
  `codfactura` int(11) NOT NULL,
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `cantidad` int(11) default NULL,
  `precio` int(11) default NULL,
  `importe` int(11) default NULL,
  `dcto` int(3) NOT NULL default '0',
  PRIMARY KEY  (`codfactura`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='lineas de facturas de proveedores temporal';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `factulineaptmp`
--

LOCK TABLES `factulineaptmp` WRITE;
/*!40000 ALTER TABLE `factulineaptmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `factulineaptmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factulineatmp`
--

DROP TABLE IF EXISTS `factulineatmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `factulineatmp` (
  `codfactura` int(11) NOT NULL,
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `cantidad` int(11) default NULL,
  `precio` int(11) default NULL,
  `importe` int(11) default NULL,
  `dcto` int(3) NOT NULL default '0',
  PRIMARY KEY  (`codfactura`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Temporal de linea de facturas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `factulineatmp`
--

LOCK TABLES `factulineatmp` WRITE;
/*!40000 ALTER TABLE `factulineatmp` DISABLE KEYS */;
INSERT INTO `factulineatmp` VALUES (4,1,1,'1',1,890,890,0),(5,1,1,'1',1,6000,6000,0),(8,1,1,'1',1,890,890,0),(9,1,1,'1',1,890,890,0),(10,1,1,'1',1,890,890,0),(13,1,1,'1',1,7000,7000,0),(14,2,1,'1',1,58800,58800,0),(14,1,1,'1',1,58800,58800,0),(21,1,1,'1',1,345,345,0),(25,1,1,'2',1,600,600,0),(25,2,1,'1',2,5000,10000,0),(26,1,1,'3',1,0,0,0),(27,1,2,'4',1,0,0,0),(28,1,1,'6',1,2,2,0),(29,1,1,'2',1,600,600,0),(33,1,2,'5',1,0,0,0),(32,1,1,'6',176,3,528,0),(37,1,1,'6',1,3,3,0),(37,2,1,'6',3,3,9,0),(40,1,2,'5',1,0,0,0),(45,1,2,'5',1,0,0,0),(45,2,1,'7',1,2345,2345,0),(62,1,1,'6',1,3,3,0),(63,1,1,'6',5,3,15,0),(63,2,2,'5',7,0,0,0),(64,1,1,'6',1,3,3,0),(64,2,1,'7',3,2345,7035,0),(66,1,3,'8',8,1200,9600,0),(67,1,3,'8',1,1200,1200,0),(69,1,1,'6',1,3,3,0),(70,1,1,'6',1,3,3,0),(73,1,1,'6',1,3,3,0),(76,1,3,'8',8,1200,9600,0),(77,1,1,'6',1,3,3,0),(78,1,3,'9',1,500,500,0),(79,1,3,'8',1,1200,1200,0),(81,1,1,'6',1,3,3,0),(80,1,1,'6',1,3,3,0),(82,1,1,'6',1,3,3,0),(84,1,3,'10',1,1230,1230,0),(85,1,1,'6',1,3,3,0),(85,2,3,'10',1,1230,1230,0),(85,3,3,'8',1,1200,1200,0),(86,1,1,'6',1,3,3,0),(87,1,1,'6',1,3,3,0),(88,1,1,'6',1,3,3,0),(89,1,1,'7',1,2345,2345,0),(89,2,1,'7',1,2345,2345,0),(89,3,3,'9',1,500,500,0),(89,4,3,'9',1,500,500,0),(90,1,1,'6',1,3,3,0),(91,1,1,'6',1,3,3,0),(99,1,1,'6',1,3,3,0),(99,2,1,'7',1,2345,2345,0),(99,3,3,'8',1,1200,1200,0),(99,4,3,'10',1,1230,1230,0),(99,5,3,'9',1,500,500,0),(106,1,1,'6',1,3,3,0),(111,1,1,'6',1,3,3,0),(112,1,1,'6',1,3,3,0),(112,2,1,'7',1,2345,2345,0),(112,3,3,'8',1,1200,1200,0),(113,1,1,'6',1,3,3,0),(113,2,1,'7',1,2345,2345,0),(113,3,3,'8',1,1200,1200,0),(114,1,1,'6',1,3,3,0),(114,2,1,'7',1,2345,2345,0),(114,3,3,'8',1,1200,1200,0),(117,1,1,'6',1,3,3,0),(124,1,1,'6',1,3,3,0),(125,1,1,'6',1,3,3,0),(126,1,1,'6',1,3,3,0),(127,1,1,'6',1,3,3,0),(129,1,1,'6',1,3,3,0),(130,1,1,'6',1,3,3,0),(131,1,1,'6',1,3,3,0),(132,1,1,'6',1,3,3,0),(133,1,1,'6',1,3,3,0),(134,1,1,'6',1,3,3,0),(135,1,1,'6',1,3,3,0),(136,1,1,'6',1,3,3,0),(137,1,1,'6',1,3,3,0),(138,1,1,'6',1,3,3,0),(139,1,1,'6',1,3,3,0),(141,1,1,'6',1,3,3,0),(142,1,1,'6',1,3,3,0),(143,1,1,'6',1,3,3,0),(144,1,1,'6',1,3,3,0),(145,1,1,'6',1,3,3,0),(146,1,1,'6',1,3,3,0),(150,1,1,'6',1,3,3,0),(151,1,1,'6',1,3,3,0),(155,1,1,'6',1,3,3,0),(158,1,3,'11',1,70000,70000,0),(160,1,1,'6',1,3,3,0),(163,1,1,'7',1,2345,2345,0),(164,1,3,'10',1,1230,1230,0),(165,1,1,'6',1,3,3,0),(165,2,3,'11',1,70000,70000,0),(168,1,3,'11',1,70000,70000,0),(169,1,1,'6',1,3,3,0),(170,1,3,'11',1,70000,70000,0),(170,2,1,'7',1,2345,2345,0),(172,1,3,'12',2,50000,100000,0),(172,2,3,'9',1,500,500,0),(176,1,3,'12',3,50000,150000,0),(188,1,3,'8',1,1200,1200,0),(186,1,3,'12',1,50000,50000,0),(189,1,3,'8',1,1200,1200,0),(190,1,3,'8',1,1200,1200,0),(191,1,3,'8',1,1200,1200,0),(192,1,3,'11',1,70000,70000,0),(193,1,3,'12',1,50000,50000,0),(195,1,3,'10',1,1230,1230,0),(196,1,3,'8',1,1200,1200,0),(197,1,3,'8',1,1200,1200,0),(198,1,3,'16',1,0,0,0),(199,1,3,'8',1,1200,1200,0),(200,1,3,'8',1,1200,1200,0),(202,1,3,'8',1,1200,1200,0),(203,1,3,'8',1,1200,1200,0),(204,1,3,'8',1,1200,1200,0),(205,1,3,'12',1,50000,50000,0),(206,1,3,'12',1,50000,50000,0),(208,1,3,'8',1,1200,1200,0),(209,1,3,'8',1,1200,1200,0),(210,1,3,'8',1,1200,1200,0),(212,1,3,'11',1,70000,70000,0),(215,1,3,'12',1,50000,50000,0),(216,1,3,'8',1,1200,1200,0),(218,1,3,'10',1,1230,1230,0),(219,1,3,'8',1,1200,1200,0),(220,1,3,'9',1,500,500,0),(221,1,3,'12',1,50000,50000,0),(222,1,3,'9',1,500,500,0),(223,1,3,'10',1,1230,1230,0),(224,1,3,'10',1,1230,1230,0),(225,1,3,'10',1,1230,1230,0),(226,1,3,'12',1,50000,50000,0),(227,1,3,'10',1,1230,1230,0),(228,1,3,'10',1,1230,1230,0),(230,1,4,'13',1,70000,70000,0),(232,1,3,'10',1,1230,1230,0),(235,1,3,'8',1,1200,1200,0),(236,1,3,'10',1,1230,1230,0),(236,2,3,'11',1,70000,70000,0),(245,1,3,'12',1,50000,50000,0),(246,1,3,'16',1,0,0,0),(246,2,3,'12',1,50000,50000,0),(247,1,4,'13',1,70000,70000,0),(248,1,3,'16',1,0,0,0),(248,2,3,'9',1,500,500,0),(249,1,4,'22',1,100000,100000,0),(250,1,3,'12',1,50000,50000,0),(252,1,3,'12',1,50000,50000,0),(253,1,3,'24',1,600000,600000,0),(254,1,3,'12',1,50000,50000,0),(255,1,3,'12',1,50000,50000,0),(256,1,4,'25',1,700000,700000,0),(262,1,3,'12',1,50000,50000,0),(269,1,3,'12',1,50000,50000,0),(270,1,4,'22',1,100000,100000,0),(271,1,3,'24',1,600000,600000,0),(272,1,3,'12',1,50000,50000,0),(273,1,3,'16',1,0,0,0),(273,2,3,'24',1,600000,600000,0),(274,1,4,'22',1,100000,100000,0),(275,1,4,'22',1,100000,100000,0),(276,1,3,'12',1,50000,50000,0),(277,1,3,'12',1,50000,50000,0),(279,1,4,'22',1,100000,100000,0),(278,2,4,'22',1,100000,100000,0),(280,1,4,'22',1,100000,100000,0),(281,1,4,'17',1,909876,909876,0),(282,1,4,'22',1,100000,100000,0),(283,1,4,'25',1,700000,700000,0),(284,1,3,'24',1,600000,600000,0),(285,1,3,'12',1,50000,50000,0),(286,1,4,'22',1,100000,100000,0),(287,1,3,'24',1,600000,600000,0),(288,1,4,'23',1,90000,90000,0),(289,1,4,'13',1,70000,70000,0),(290,1,3,'9',1,500,500,0),(291,1,3,'24',1,600000,600000,0),(292,1,4,'25',1,700000,700000,0),(293,1,3,'12',1,50000,50000,0),(294,1,4,'22',1,100000,100000,0),(295,1,3,'24',1,600000,600000,0),(296,1,3,'12',1,50000,50000,0),(297,1,3,'12',1,50000,50000,0),(298,1,4,'13',1,70000,70000,0),(299,1,4,'23',1,90000,90000,0),(301,1,3,'12',1,50000,50000,0),(302,1,3,'12',1,50000,50000,0),(302,2,4,'23',1,90000,90000,0),(303,1,3,'12',1,50000,50000,0),(303,2,3,'24',1,600000,600000,0),(304,1,4,'22',1,100000,100000,0),(305,1,4,'25',1,700000,700000,0),(307,1,3,'24',1,600000,600000,0),(308,1,3,'24',1,600000,600000,0),(309,1,4,'17',1,909876,909876,0),(310,1,3,'12',1,50000,50000,0),(311,1,3,'12',1,50000,50000,0),(316,1,3,'24',1,600000,600000,0),(318,1,0,'',1,0,0,0),(318,2,3,'12',1,50000,50000,0),(318,3,3,'12',1,50000,50000,0),(318,4,0,'',1,0,0,0),(319,1,3,'24',1,600000,600000,0),(320,1,4,'22',1,100000,100000,0),(320,2,4,'22',1,100000,90000,10),(325,1,0,'',1,0,0,0),(325,2,0,'',1,0,0,0),(325,3,0,'',1,0,0,0),(325,4,0,'',1,0,0,0),(326,1,3,'24',1,600000,600000,0),(326,2,0,'',1,0,0,0),(327,1,3,'24',1,600000,600000,0),(328,1,3,'24',1,600000,600000,0),(330,1,4,'22',1,100000,100000,0),(332,1,3,'24',1,600000,600000,0),(333,1,3,'24',1,600000,600000,0),(336,1,3,'24',1,600000,600000,0),(339,1,3,'12',1,50000,50000,0),(350,1,3,'12',1,50000,-499950000,127),(351,1,3,'12',1,50000,50000,0),(351,2,3,'9',1,500,450,10),(352,1,4,'22',1,100000,100000,0),(354,1,3,'12',1,50000,50000,0),(355,1,4,'22',1,100000,100000,0),(376,1,3,'24',1,600000,600000,0),(379,1,3,'12',1,50000,50000,0),(383,1,3,'12',1,50000,50000,0),(388,1,3,'12',1,50000,50000,0),(392,1,3,'12',1,50000,50000,0),(396,1,3,'12',1,50000,50000,0),(397,1,3,'12',1,50000,50000,0),(398,1,3,'12',1,50000,50000,0),(399,1,3,'12',1,50000,50000,0),(400,1,3,'12',1,50000,50000,0),(401,1,3,'12',1,50000,50000,0),(402,1,3,'12',1,50000,50000,0),(404,1,3,'12',1,50000,50000,0),(406,1,3,'12',1,50000,50000,0),(407,1,3,'12',1,50000,50000,0),(408,1,3,'12',1,50000,50000,0),(410,1,3,'12',1,50000,50000,0),(411,1,4,'23',1,90000,90000,0),(412,1,4,'25',1,700000,700000,0),(413,1,3,'12',1,50000,50000,0),(415,1,3,'12',1,50000,50000,0),(419,1,3,'12',1,50000,50000,0),(420,1,3,'12',1,50000,50000,0),(421,1,3,'12',1,50000,50000,0),(422,1,3,'12',1,50000,50000,0),(422,2,3,'24',1,600000,600000,0),(424,1,3,'12',1,50000,50000,0),(432,1,3,'24',1,600000,600000,0),(434,1,3,'12',1,50000,50000,0),(435,1,4,'22',1,100000,100000,0),(437,1,3,'24',1,600000,600000,0),(439,1,3,'12',1,50000,50000,0),(443,1,3,'24',1,600000,600000,0),(445,1,3,'24',1,600000,600000,0),(446,1,3,'24',1,600000,600000,0),(447,1,3,'24',1,600000,600000,0),(450,1,3,'12',1,50000,50000,0),(457,1,3,'12',1,50000,50000,0),(462,1,3,'12',1,50000,50000,0),(463,1,4,'23',1,90000,90000,0),(465,1,3,'12',1,50000,50000,0),(466,1,3,'24',1,600000,600000,0),(469,1,3,'12',1,50000,45000,10),(472,2,3,'12',1,50000,50000,0),(472,1,3,'12',1,50000,50000,0),(473,1,3,'12',1,50000,50000,0),(473,2,3,'12',1,50000,50000,0),(473,3,3,'12',1,50000,50000,0),(482,1,3,'12',1,50000,50000,0),(483,1,3,'24',1,600000,600000,0);
/*!40000 ALTER TABLE `factulineatmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturas`
--

DROP TABLE IF EXISTS `facturas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facturas` (
  `codfactura` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL,
  `iva` int(8) NOT NULL default '0',
  `codcliente` int(5) NOT NULL,
  `estado` varchar(1) NOT NULL default '0',
  `totalfactura` int(11) default NULL,
  `fechavencimiento` date NOT NULL default '0000-00-00',
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codfactura`)
) ENGINE=MyISAM AUTO_INCREMENT=229 DEFAULT CHARSET=utf8 COMMENT='facturas de ventas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facturas`
--

LOCK TABLES `facturas` WRITE;
/*!40000 ALTER TABLE `facturas` DISABLE KEYS */;
INSERT INTO `facturas` VALUES (1,'2011-10-18',16,1,'2',1032,'0000-00-00','0'),(2,'2011-10-18',16,1,'2',6960,'0000-00-00','0'),(3,'2011-10-20',16,1,'2',8120,'0000-00-00','0'),(4,'2011-10-20',19,1,'2',139944,'0000-00-00','0'),(5,'2011-10-20',16,1,'1',0,'0000-00-00','0'),(6,'2011-10-20',16,1,'1',0,'0000-00-00','0'),(7,'2011-10-20',16,1,'1',0,'0000-00-00','0'),(8,'2011-10-20',16,1,'1',0,'0000-00-00','0'),(9,'2011-10-20',16,2,'2',400,'0000-00-00','0'),(10,'2011-10-20',16,1,'2',0,'0000-00-00','0'),(11,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(12,'2011-10-21',16,7,'1',0,'0000-00-00','0'),(13,'2011-10-21',16,1,'2',0,'0000-00-00','0'),(14,'2011-10-21',16,1,'2',0,'0000-00-00','0'),(15,'2011-10-21',16,1,'1',612,'0000-00-00','0'),(16,'2011-10-21',16,2,'1',14,'0000-00-00','0'),(17,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(18,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(19,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(20,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(21,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(22,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(23,'2011-10-21',19,10,'2',2791,'0000-00-00','0'),(24,'2011-10-25',16,1,'1',0,'0000-00-00','0'),(25,'2011-10-26',16,1,'2',18,'0000-00-00','0'),(26,'2011-10-26',16,7,'2',8164,'0000-00-00','0'),(27,'2011-10-26',16,7,'1',11136,'0000-00-00','0'),(28,'2011-10-27',16,1,'1',3,'0000-00-00','0'),(29,'2011-10-27',16,1,'1',3,'0000-00-00','0'),(30,'2011-10-27',16,1,'2',3,'0000-00-00','0'),(31,'2011-10-27',16,1,'2',580,'0000-00-00','0'),(32,'2011-10-27',16,1,'1',1392,'0000-00-00','0'),(33,'2011-10-27',16,1,'1',3,'0000-00-00','0'),(34,'2011-10-27',16,1,'2',3,'0000-00-00','0'),(35,'2011-10-27',16,1,'1',3,'0000-00-00','0'),(36,'2011-10-27',16,1,'1',1427,'0000-00-00','0'),(37,'2011-10-27',16,1,'1',2822,'0000-00-00','0'),(38,'2011-10-27',16,1,'1',3,'0000-00-00','0'),(39,'2011-10-27',16,1,'1',3,'0000-00-00','0'),(40,'2011-10-27',16,1,'1',3,'0000-00-00','0'),(41,'2011-10-27',16,1,'1',6600,'0000-00-00','0'),(42,'2011-10-27',16,1,'1',3,'0000-00-00','0'),(43,'2011-10-27',16,1,'1',3,'0000-00-00','0'),(44,'2011-10-27',19,1,'1',0,'0000-00-00','0'),(45,'2011-10-27',19,1,'1',0,'0000-00-00','0'),(46,'2011-10-27',19,1,'2',6281,'0000-00-00','1'),(47,'2011-10-26',16,1,'1',0,'0000-00-00','0'),(48,'2011-10-27',16,6,'1',3,'0000-00-00','0'),(49,'2011-10-27',16,5,'1',4116,'0000-00-00','0'),(50,'2011-10-28',16,1,'2',3,'0000-00-00','0'),(51,'2011-10-28',19,5,'1',83300,'0000-00-00','0'),(52,'2011-10-28',19,1,'1',2791,'0000-00-00','0'),(53,'2011-10-28',19,1,'1',1464,'0000-00-00','0'),(54,'2011-10-28',19,1,'2',83304,'0000-00-00','0'),(55,'2011-10-28',19,1,'1',83300,'0000-00-00','0'),(56,'2011-10-28',19,1,'2',4,'0000-00-00','0'),(57,'2011-10-28',19,1,'2',83300,'0000-00-00','0'),(58,'2011-10-28',19,1,'2',119595,'0000-00-00','0'),(59,'2011-10-29',19,8,'2',178500,'0000-00-00','0'),(60,'2011-10-29',19,13456877,'1',1428,'0000-00-00','0'),(61,'2011-10-29',19,16394696,'1',59500,'0000-00-00','0'),(62,'2011-10-29',19,13456879,'1',1428,'0000-00-00','0'),(63,'2011-10-29',19,123456,'1',1428,'0000-00-00','0'),(64,'2011-10-29',19,123456,'1',1428,'0000-00-00','0'),(65,'2011-10-29',19,16394698,'1',83300,'0000-00-00','0'),(66,'2011-10-29',19,123456,'1',59500,'0000-00-00','0'),(67,'2011-10-29',19,123456,'1',1464,'0000-00-00','0'),(68,'2011-10-29',19,16394698,'1',1428,'0000-00-00','0'),(69,'2011-10-29',19,5,'1',1428,'0000-00-00','0'),(70,'2011-10-29',19,1234566,'2',0,'0000-00-00','0'),(71,'2011-10-29',19,1234566,'2',1428,'0000-00-00','0'),(72,'2011-10-29',19,16394698,'2',1428,'0000-00-00','0'),(73,'2011-10-29',19,16394698,'2',59500,'0000-00-00','0'),(74,'2011-10-29',19,16394698,'2',59500,'0000-00-00','0'),(75,'2011-10-29',19,1234566,'1',1428,'0000-00-00','0'),(76,'2011-10-29',19,1,'1',1428,'0000-00-00','0'),(77,'2011-10-29',19,1234566,'1',1428,'0000-00-00','0'),(78,'2011-10-29',19,1234566,'1',83300,'0000-00-00','0'),(79,'2011-10-29',19,16394698,'1',59500,'0000-00-00','0'),(80,'2011-10-29',19,16394698,'1',1428,'0000-00-00','0'),(81,'2011-10-29',19,16394698,'1',1464,'0000-00-00','0'),(82,'2011-10-29',19,1234566,'1',1428,'0000-00-00','0'),(83,'2011-10-29',19,13456879,'1',595,'0000-00-00','0'),(84,'2011-10-29',19,13456879,'1',59500,'0000-00-00','0'),(85,'2011-10-29',19,13456879,'1',595,'0000-00-00','0'),(86,'2011-10-29',19,13456879,'1',1464,'0000-00-00','0'),(87,'2011-10-29',19,13456879,'1',1464,'0000-00-00','0'),(88,'2011-10-29',19,13456879,'1',1464,'0000-00-00','0'),(89,'2011-10-29',19,1234566,'1',59500,'0000-00-00','0'),(90,'2011-10-30',19,1,'1',1464,'0000-00-00','0'),(91,'2011-10-30',19,16394698,'1',1464,'0000-00-00','0'),(92,'2011-10-30',19,16394698,'1',83300,'0000-00-00','0'),(93,'2011-10-30',19,12345,'2',1464,'0000-00-00','0'),(94,'2011-10-30',19,1,'2',1428,'0000-00-00','0'),(95,'2011-10-30',19,7,'2',84764,'0000-00-00','0'),(96,'2011-11-10',19,1,'1',0,'0000-00-00','0'),(97,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(98,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(99,'2011-11-10',19,1,'1',83300,'0000-00-00','0'),(100,'2011-11-10',19,1,'1',83300,'0000-00-00','0'),(101,'2011-11-10',19,1,'1',595,'0000-00-00','0'),(102,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(103,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(104,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(105,'2011-11-10',19,1,'1',714000,'0000-00-00','0'),(106,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(107,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(108,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(109,'2011-11-10',19,1,'1',833000,'0000-00-00','0'),(110,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(111,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(112,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(113,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(114,'2011-11-10',19,1,'1',714000,'0000-00-00','0'),(115,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(116,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(117,'2011-11-10',19,1,'1',714000,'0000-00-00','0'),(118,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(119,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(120,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(121,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(122,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(123,'2011-11-10',19,1,'1',714000,'0000-00-00','0'),(124,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(125,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(126,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(127,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(128,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(129,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(130,'2011-11-10',19,1,'1',1082752,'0000-00-00','0'),(131,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(132,'2011-11-10',19,1,'1',833000,'0000-00-00','0'),(133,'2011-11-10',19,1,'1',714000,'0000-00-00','0'),(134,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(135,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(136,'2011-11-10',19,1,'1',714000,'0000-00-00','0'),(137,'2011-11-10',19,1,'1',107100,'0000-00-00','0'),(138,'2011-11-10',19,1,'1',83300,'0000-00-00','0'),(139,'2011-11-10',19,1,'1',595,'0000-00-00','0'),(140,'2011-11-10',19,1,'1',714000,'0000-00-00','0'),(141,'2011-11-10',19,1,'1',833000,'0000-00-00','0'),(142,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(143,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(144,'2011-11-10',19,1,'1',714000,'0000-00-00','0'),(145,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(146,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(147,'2011-11-10',19,1,'1',83300,'0000-00-00','0'),(148,'2011-11-10',19,1,'1',107100,'0000-00-00','0'),(149,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(150,'2011-11-10',19,1,'1',166600,'0000-00-00','0'),(151,'2011-11-10',19,1,'1',773500,'0000-00-00','0'),(152,'2011-11-10',19,1,'1',119000,'0000-00-00','0'),(153,'2011-11-10',19,1,'1',833000,'0000-00-00','0'),(154,'2011-11-10',19,1,'1',714000,'0000-00-00','0'),(155,'2011-11-10',19,1,'1',714000,'0000-00-00','0'),(156,'2011-11-10',19,1,'1',1082752,'0000-00-00','0'),(157,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(158,'2011-11-10',19,1,'1',59500,'0000-00-00','0'),(159,'2011-11-10',19,1,'1',0,'0000-00-00','0'),(160,'2011-11-11',19,1,'1',0,'0000-00-00','0'),(161,'2011-11-11',19,1,'1',714000,'0000-00-00','0'),(162,'2011-11-11',19,1,'1',0,'0000-00-00','0'),(163,'2011-11-11',19,1,'1',119000,'0000-00-00','0'),(164,'2011-11-11',19,1,'1',714000,'0000-00-00','0'),(165,'2011-11-11',19,1,'1',0,'0000-00-00','0'),(166,'2011-11-11',19,8,'2',226100,'0000-00-00','0'),(167,'2011-11-11',19,1,'1',0,'0000-00-00','0'),(168,'2011-11-11',19,1,'2',0,'0000-00-00','0'),(169,'2011-11-11',19,1,'2',714000,'0000-00-00','0'),(170,'2011-11-11',19,10,'2',714000,'0000-00-00','0'),(171,'2011-11-11',19,6,'2',119000,'0000-00-00','0'),(172,'2011-11-10',16,1,'1',696000,'0000-00-00','1'),(173,'2011-11-11',19,1,'2',59500,'0000-00-00','0'),(174,'2011-11-10',16,5,'1',0,'0000-00-00','0'),(175,'2011-11-11',19,1,'1',0,'0000-00-00','0'),(176,'2011-11-16',19,1,'1',60036,'0000-00-00','0'),(177,'2011-11-16',19,1,'2',119000,'0000-00-00','0'),(178,'2011-11-16',19,1,'1',59500,'0000-00-00','0'),(179,'2011-11-16',19,1,'1',59500,'0000-00-00','0'),(180,'2011-11-16',19,1,'1',119000,'0000-00-00','0'),(181,'2011-11-16',19,1,'1',0,'0000-00-00','0'),(182,'2011-11-16',19,1,'1',0,'0000-00-00','0'),(183,'2011-11-16',19,1,'1',59500,'0000-00-00','0'),(184,'2011-11-16',19,1,'1',59500,'0000-00-00','0'),(185,'2011-11-16',19,5,'1',59500,'0000-00-00','0'),(186,'2011-11-18',19,1,'1',59500,'0000-00-00','0'),(187,'2011-11-18',19,1,'1',0,'0000-00-00','0'),(188,'2011-11-18',19,1,'1',0,'0000-00-00','0'),(189,'2011-11-18',19,1,'1',59500,'0000-00-00','0'),(190,'2011-11-18',19,1,'1',0,'0000-00-00','0'),(191,'2011-11-18',19,1,'1',59500,'0000-00-00','0'),(192,'2011-11-21',19,1,'1',0,'0000-00-00','0'),(193,'2011-11-21',19,1,'1',0,'0000-00-00','0'),(194,'2011-11-21',19,1,'1',59500,'0000-00-00','0'),(195,'2011-11-21',19,1,'1',59500,'0000-00-00','0'),(196,'2011-11-21',19,1,'1',59500,'0000-00-00','0'),(197,'2011-11-21',19,1,'2',59500,'0000-00-00','0'),(198,'2011-11-21',19,1,'1',59500,'0000-00-00','0'),(199,'2011-11-21',19,1,'1',59500,'0000-00-00','0'),(200,'2011-11-21',19,1,'2',59500,'0000-00-00','0'),(201,'2011-11-21',16,5,'1',58000,'0000-00-00','0'),(202,'2011-11-21',19,1,'1',59500,'0000-00-00','0'),(203,'2011-11-21',19,1,'1',59500,'0000-00-00','0'),(204,'2011-11-21',19,1,'2',59500,'0000-00-00','1'),(205,'2011-11-21',19,1,'2',59500,'0000-00-00','0'),(206,'2011-11-21',19,1,'1',107100,'0000-00-00','0'),(207,'2011-11-21',19,1,'1',833000,'0000-00-00','0'),(208,'2011-11-21',19,1,'1',59500,'0000-00-00','0'),(209,'2011-11-21',19,1,'1',59500,'0000-00-00','0'),(210,'2011-11-21',16,1,'1',58000,'0000-00-00','0'),(211,'2011-11-21',19,1,'2',59500,'0000-00-00','0'),(212,'2011-11-22',19,1,'2',773500,'0000-00-00','0'),(213,'2011-11-22',19,1,'1',59500,'0000-00-00','0'),(214,'2011-11-22',19,13456879,'1',714000,'0000-00-00','0'),(215,'2011-11-23',19,13456879,'1',59500,'0000-00-00','0'),(216,'2011-11-23',19,13456879,'1',119000,'0000-00-00','0'),(217,'2011-11-23',19,6,'1',714000,'0000-00-00','0'),(218,'2011-11-23',19,7,'1',714000,'0000-00-00','0'),(219,'2011-11-23',19,0,'1',714000,'0000-00-00','0'),(220,'2011-11-23',16,6,'1',58000,'0000-00-00','0'),(221,'2011-11-23',19,1,'1',59500,'0000-00-00','0'),(222,'2011-11-23',19,13456879,'1',59500,'0000-00-00','0'),(223,'2011-11-23',19,8,'1',107100,'0000-00-00','0'),(224,'2011-11-23',19,10,'2',59500,'0000-00-00','0'),(225,'2011-11-23',19,10,'2',714000,'0000-00-00','0'),(226,'2011-11-23',19,47,'1',178500,'0000-00-00','0'),(227,'2011-11-23',19,47,'1',59500,'0000-00-00','0'),(228,'2011-11-23',19,1,'1',714000,'0000-00-00','0');
/*!40000 ALTER TABLE `facturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturasp`
--

DROP TABLE IF EXISTS `facturasp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facturasp` (
  `codfactura` varchar(20) NOT NULL default '',
  `codproveedor` int(5) NOT NULL,
  `fecha` date NOT NULL,
  `iva` int(3) NOT NULL default '0',
  `estado` varchar(1) NOT NULL default '0',
  `totalfactura` int(11) default NULL,
  `fechapago` date NOT NULL default '0000-00-00',
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codfactura`,`codproveedor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='facturas de compras a proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facturasp`
--

LOCK TABLES `facturasp` WRITE;
/*!40000 ALTER TABLE `facturasp` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturasp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturasptmp`
--

DROP TABLE IF EXISTS `facturasptmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facturasptmp` (
  `codfactura` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL,
  PRIMARY KEY  (`codfactura`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='temporal de facturas de proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facturasptmp`
--

LOCK TABLES `facturasptmp` WRITE;
/*!40000 ALTER TABLE `facturasptmp` DISABLE KEYS */;
INSERT INTO `facturasptmp` VALUES (1,'2011-10-18');
/*!40000 ALTER TABLE `facturasptmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturastmp`
--

DROP TABLE IF EXISTS `facturastmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facturastmp` (
  `codfactura` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL,
  PRIMARY KEY  (`codfactura`)
) ENGINE=MyISAM AUTO_INCREMENT=485 DEFAULT CHARSET=utf8 COMMENT='temporal de facturas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facturastmp`
--

LOCK TABLES `facturastmp` WRITE;
/*!40000 ALTER TABLE `facturastmp` DISABLE KEYS */;
INSERT INTO `facturastmp` VALUES (1,'0000-00-00'),(2,'0000-00-00'),(3,'0000-00-00'),(4,'0000-00-00'),(5,'0000-00-00'),(6,'0000-00-00'),(7,'0000-00-00'),(8,'2011-10-18'),(9,'2011-10-18'),(10,'2011-10-18'),(11,'0000-00-00'),(12,'0000-00-00'),(13,'0000-00-00'),(14,'0000-00-00'),(15,'0000-00-00'),(16,'2011-10-20'),(17,'2011-10-20'),(18,'0000-00-00'),(19,'0000-00-00'),(20,'0000-00-00'),(21,'2011-10-20'),(22,'0000-00-00'),(23,'0000-00-00'),(24,'0000-00-00'),(25,'0000-00-00'),(26,'0000-00-00'),(27,'0000-00-00'),(28,'0000-00-00'),(29,'0000-00-00'),(30,'0000-00-00'),(31,'0000-00-00'),(32,'0000-00-00'),(33,'0000-00-00'),(34,'0000-00-00'),(35,'0000-00-00'),(36,'0000-00-00'),(37,'0000-00-00'),(38,'0000-00-00'),(39,'0000-00-00'),(40,'0000-00-00'),(41,'0000-00-00'),(42,'0000-00-00'),(43,'0000-00-00'),(44,'0000-00-00'),(45,'0000-00-00'),(46,'0000-00-00'),(47,'2011-10-21'),(48,'0000-00-00'),(49,'0000-00-00'),(50,'0000-00-00'),(51,'2011-10-25'),(52,'0000-00-00'),(53,'0000-00-00'),(54,'0000-00-00'),(55,'0000-00-00'),(56,'0000-00-00'),(57,'0000-00-00'),(58,'0000-00-00'),(59,'0000-00-00'),(60,'0000-00-00'),(61,'0000-00-00'),(62,'0000-00-00'),(63,'0000-00-00'),(64,'0000-00-00'),(65,'2011-10-26'),(66,'0000-00-00'),(67,'0000-00-00'),(68,'0000-00-00'),(69,'0000-00-00'),(70,'0000-00-00'),(71,'0000-00-00'),(72,'0000-00-00'),(73,'0000-00-00'),(74,'0000-00-00'),(75,'0000-00-00'),(76,'2011-10-27'),(77,'0000-00-00'),(78,'0000-00-00'),(79,'0000-00-00'),(80,'0000-00-00'),(81,'0000-00-00'),(82,'0000-00-00'),(83,'0000-00-00'),(84,'0000-00-00'),(85,'0000-00-00'),(86,'0000-00-00'),(87,'0000-00-00'),(88,'0000-00-00'),(89,'0000-00-00'),(90,'0000-00-00'),(91,'0000-00-00'),(92,'0000-00-00'),(93,'0000-00-00'),(94,'0000-00-00'),(95,'0000-00-00'),(96,'0000-00-00'),(97,'0000-00-00'),(98,'0000-00-00'),(99,'0000-00-00'),(100,'0000-00-00'),(101,'0000-00-00'),(102,'0000-00-00'),(103,'0000-00-00'),(104,'0000-00-00'),(105,'2011-10-27'),(106,'2011-10-27'),(107,'0000-00-00'),(108,'2011-10-27'),(109,'2011-10-27'),(110,'2011-10-27'),(111,'2011-10-27'),(112,'2011-10-27'),(113,'2011-10-27'),(114,'2011-10-27'),(115,'0000-00-00'),(116,'0000-00-00'),(117,'2011-10-28'),(118,'2011-10-28'),(119,'2011-10-28'),(120,'0000-00-00'),(121,'2011-10-28'),(122,'0000-00-00'),(123,'2011-10-28'),(124,'2011-10-28'),(125,'2011-10-28'),(126,'2011-10-28'),(127,'2011-10-28'),(128,'0000-00-00'),(129,'2011-10-28'),(130,'2011-10-28'),(131,'2011-10-28'),(132,'2011-10-28'),(133,'2011-10-28'),(134,'2011-10-28'),(135,'2011-10-28'),(136,'2011-10-28'),(137,'2011-10-28'),(138,'2011-10-28'),(139,'2011-10-28'),(140,'0000-00-00'),(141,'2011-10-28'),(142,'2011-10-28'),(143,'2011-10-28'),(144,'2011-10-28'),(145,'2011-10-28'),(146,'2011-10-28'),(147,'2011-10-28'),(148,'2011-10-28'),(149,'2011-10-28'),(150,'2011-10-28'),(151,'2011-10-28'),(152,'0000-00-00'),(153,'0000-00-00'),(154,'0000-00-00'),(155,'2011-10-28'),(156,'0000-00-00'),(157,'0000-00-00'),(158,'0000-00-00'),(159,'0000-00-00'),(160,'0000-00-00'),(161,'0000-00-00'),(162,'0000-00-00'),(163,'0000-00-00'),(164,'0000-00-00'),(165,'0000-00-00'),(166,'0000-00-00'),(167,'0000-00-00'),(168,'0000-00-00'),(169,'0000-00-00'),(170,'0000-00-00'),(171,'0000-00-00'),(172,'0000-00-00'),(173,'0000-00-00'),(174,'0000-00-00'),(175,'0000-00-00'),(176,'0000-00-00'),(177,'0000-00-00'),(178,'0000-00-00'),(179,'0000-00-00'),(180,'0000-00-00'),(181,'2011-10-29'),(182,'0000-00-00'),(183,'0000-00-00'),(184,'0000-00-00'),(185,'0000-00-00'),(186,'0000-00-00'),(187,'0000-00-00'),(188,'0000-00-00'),(189,'0000-00-00'),(190,'0000-00-00'),(191,'0000-00-00'),(192,'0000-00-00'),(193,'0000-00-00'),(194,'0000-00-00'),(195,'0000-00-00'),(196,'0000-00-00'),(197,'0000-00-00'),(198,'0000-00-00'),(199,'2011-10-29'),(200,'2011-10-29'),(201,'0000-00-00'),(202,'0000-00-00'),(203,'0000-00-00'),(204,'2011-10-29'),(205,'0000-00-00'),(206,'0000-00-00'),(207,'0000-00-00'),(208,'0000-00-00'),(209,'0000-00-00'),(210,'0000-00-00'),(211,'0000-00-00'),(212,'0000-00-00'),(213,'0000-00-00'),(214,'0000-00-00'),(215,'0000-00-00'),(216,'0000-00-00'),(217,'0000-00-00'),(218,'0000-00-00'),(219,'0000-00-00'),(220,'0000-00-00'),(221,'0000-00-00'),(222,'0000-00-00'),(223,'0000-00-00'),(224,'0000-00-00'),(225,'0000-00-00'),(226,'0000-00-00'),(227,'0000-00-00'),(228,'0000-00-00'),(229,'0000-00-00'),(230,'0000-00-00'),(231,'0000-00-00'),(232,'0000-00-00'),(233,'0000-00-00'),(234,'0000-00-00'),(235,'0000-00-00'),(236,'0000-00-00'),(237,'0000-00-00'),(238,'0000-00-00'),(239,'0000-00-00'),(240,'0000-00-00'),(241,'0000-00-00'),(242,'0000-00-00'),(243,'0000-00-00'),(244,'0000-00-00'),(245,'0000-00-00'),(246,'0000-00-00'),(247,'0000-00-00'),(248,'0000-00-00'),(249,'0000-00-00'),(250,'0000-00-00'),(251,'0000-00-00'),(252,'0000-00-00'),(253,'0000-00-00'),(254,'0000-00-00'),(255,'0000-00-00'),(256,'0000-00-00'),(257,'0000-00-00'),(258,'0000-00-00'),(259,'0000-00-00'),(260,'0000-00-00'),(261,'0000-00-00'),(262,'0000-00-00'),(263,'0000-00-00'),(264,'0000-00-00'),(265,'0000-00-00'),(266,'0000-00-00'),(267,'0000-00-00'),(268,'0000-00-00'),(269,'0000-00-00'),(270,'0000-00-00'),(271,'0000-00-00'),(272,'0000-00-00'),(273,'0000-00-00'),(274,'0000-00-00'),(275,'0000-00-00'),(276,'0000-00-00'),(277,'0000-00-00'),(278,'0000-00-00'),(279,'0000-00-00'),(280,'0000-00-00'),(281,'0000-00-00'),(282,'0000-00-00'),(283,'0000-00-00'),(284,'0000-00-00'),(285,'0000-00-00'),(286,'0000-00-00'),(287,'0000-00-00'),(288,'0000-00-00'),(289,'0000-00-00'),(290,'0000-00-00'),(291,'0000-00-00'),(292,'0000-00-00'),(293,'0000-00-00'),(294,'0000-00-00'),(295,'0000-00-00'),(296,'0000-00-00'),(297,'0000-00-00'),(298,'0000-00-00'),(299,'0000-00-00'),(300,'0000-00-00'),(301,'0000-00-00'),(302,'0000-00-00'),(303,'0000-00-00'),(304,'0000-00-00'),(305,'0000-00-00'),(306,'0000-00-00'),(307,'0000-00-00'),(308,'0000-00-00'),(309,'0000-00-00'),(310,'0000-00-00'),(311,'0000-00-00'),(312,'0000-00-00'),(313,'0000-00-00'),(314,'0000-00-00'),(315,'0000-00-00'),(316,'0000-00-00'),(317,'0000-00-00'),(318,'0000-00-00'),(319,'0000-00-00'),(320,'0000-00-00'),(321,'0000-00-00'),(322,'0000-00-00'),(323,'0000-00-00'),(324,'0000-00-00'),(325,'0000-00-00'),(326,'0000-00-00'),(327,'0000-00-00'),(328,'0000-00-00'),(329,'0000-00-00'),(330,'0000-00-00'),(331,'0000-00-00'),(332,'2011-11-10'),(333,'2011-11-10'),(334,'2011-11-10'),(335,'2011-11-10'),(336,'2011-11-10'),(337,'2011-11-10'),(338,'2011-11-10'),(339,'0000-00-00'),(340,'2011-11-10'),(341,'0000-00-00'),(342,'0000-00-00'),(343,'0000-00-00'),(344,'0000-00-00'),(345,'0000-00-00'),(346,'0000-00-00'),(347,'0000-00-00'),(348,'0000-00-00'),(349,'0000-00-00'),(350,'0000-00-00'),(351,'0000-00-00'),(352,'0000-00-00'),(353,'0000-00-00'),(354,'0000-00-00'),(355,'0000-00-00'),(356,'0000-00-00'),(357,'0000-00-00'),(358,'0000-00-00'),(359,'0000-00-00'),(360,'0000-00-00'),(361,'0000-00-00'),(362,'0000-00-00'),(363,'0000-00-00'),(364,'0000-00-00'),(365,'0000-00-00'),(366,'0000-00-00'),(367,'0000-00-00'),(368,'0000-00-00'),(369,'0000-00-00'),(370,'0000-00-00'),(371,'0000-00-00'),(372,'0000-00-00'),(373,'0000-00-00'),(374,'0000-00-00'),(375,'0000-00-00'),(376,'0000-00-00'),(377,'0000-00-00'),(378,'0000-00-00'),(379,'0000-00-00'),(380,'0000-00-00'),(381,'0000-00-00'),(382,'0000-00-00'),(383,'0000-00-00'),(384,'0000-00-00'),(385,'0000-00-00'),(386,'0000-00-00'),(387,'0000-00-00'),(388,'0000-00-00'),(389,'0000-00-00'),(390,'0000-00-00'),(391,'0000-00-00'),(392,'0000-00-00'),(393,'0000-00-00'),(394,'0000-00-00'),(395,'0000-00-00'),(396,'0000-00-00'),(397,'0000-00-00'),(398,'0000-00-00'),(399,'0000-00-00'),(400,'0000-00-00'),(401,'0000-00-00'),(402,'0000-00-00'),(403,'0000-00-00'),(404,'2011-11-21'),(405,'0000-00-00'),(406,'0000-00-00'),(407,'0000-00-00'),(408,'0000-00-00'),(409,'0000-00-00'),(410,'0000-00-00'),(411,'0000-00-00'),(412,'0000-00-00'),(413,'0000-00-00'),(414,'0000-00-00'),(415,'0000-00-00'),(416,'0000-00-00'),(417,'0000-00-00'),(418,'0000-00-00'),(419,'2011-11-21'),(420,'2011-11-21'),(421,'0000-00-00'),(422,'0000-00-00'),(423,'0000-00-00'),(424,'0000-00-00'),(425,'0000-00-00'),(426,'0000-00-00'),(427,'0000-00-00'),(428,'0000-00-00'),(429,'0000-00-00'),(430,'0000-00-00'),(431,'0000-00-00'),(432,'0000-00-00'),(433,'0000-00-00'),(434,'0000-00-00'),(435,'0000-00-00'),(436,'0000-00-00'),(437,'0000-00-00'),(438,'0000-00-00'),(439,'0000-00-00'),(440,'0000-00-00'),(441,'0000-00-00'),(442,'0000-00-00'),(443,'0000-00-00'),(444,'0000-00-00'),(445,'2011-11-23'),(446,'2011-11-23'),(447,'0000-00-00'),(448,'0000-00-00'),(449,'0000-00-00'),(450,'2011-11-23'),(451,'0000-00-00'),(452,'0000-00-00'),(453,'2011-11-23'),(454,'0000-00-00'),(455,'0000-00-00'),(456,'0000-00-00'),(457,'0000-00-00'),(458,'0000-00-00'),(459,'0000-00-00'),(460,'0000-00-00'),(461,'0000-00-00'),(462,'0000-00-00'),(463,'0000-00-00'),(464,'0000-00-00'),(465,'0000-00-00'),(466,'0000-00-00'),(467,'0000-00-00'),(468,'0000-00-00'),(469,'0000-00-00'),(470,'0000-00-00'),(471,'2011-11-23'),(472,'2011-11-23'),(473,'2011-11-23'),(474,'0000-00-00'),(475,'0000-00-00'),(476,'0000-00-00'),(477,'0000-00-00'),(478,'0000-00-00'),(479,'0000-00-00'),(480,'0000-00-00'),(481,'0000-00-00'),(482,'0000-00-00'),(483,'0000-00-00'),(484,'0000-00-00');
/*!40000 ALTER TABLE `facturastmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familias`
--

DROP TABLE IF EXISTS `familias`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `familias` (
  `codfamilia` int(5) NOT NULL auto_increment,
  `nombre` varchar(20) default NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codfamilia`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='familia de articulos';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `familias`
--

LOCK TABLES `familias` WRITE;
/*!40000 ALTER TABLE `familias` DISABLE KEYS */;
INSERT INTO `familias` VALUES (1,'sector a','0'),(2,'sector b','0'),(3,'piso b','0'),(4,'mercedez','0'),(5,'marihuana','0'),(6,'topedrero','0');
/*!40000 ALTER TABLE `familias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formapago`
--

DROP TABLE IF EXISTS `formapago`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `formapago` (
  `codformapago` int(2) NOT NULL auto_increment,
  `nombrefp` varchar(40) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codformapago`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Forma de pago';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `formapago`
--

LOCK TABLES `formapago` WRITE;
/*!40000 ALTER TABLE `formapago` DISABLE KEYS */;
INSERT INTO `formapago` VALUES (1,'contado','0'),(2,'targetas','0'),(3,'cheque','0');
/*!40000 ALTER TABLE `formapago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `impuestos`
--

DROP TABLE IF EXISTS `impuestos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `impuestos` (
  `codimpuesto` int(3) NOT NULL auto_increment,
  `nombre` varchar(20) default NULL,
  `valor` int(11) default NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codimpuesto`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='tipos de impuestos';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `impuestos`
--

LOCK TABLES `impuestos` WRITE;
/*!40000 ALTER TABLE `impuestos` DISABLE KEYS */;
INSERT INTO `impuestos` VALUES (1,'loco',5,'0');
/*!40000 ALTER TABLE `impuestos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `librodiario`
--

DROP TABLE IF EXISTS `librodiario`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `librodiario` (
  `id` int(8) NOT NULL auto_increment,
  `fecha` date NOT NULL default '0000-00-00',
  `tipodocumento` varchar(1) NOT NULL,
  `coddocumento` varchar(20) NOT NULL,
  `codcomercial` int(5) NOT NULL,
  `codformapago` int(2) NOT NULL,
  `numpago` varchar(30) NOT NULL,
  `total` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='Movimientos diarios';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `librodiario`
--

LOCK TABLES `librodiario` WRITE;
/*!40000 ALTER TABLE `librodiario` DISABLE KEYS */;
INSERT INTO `librodiario` VALUES (1,'2011-10-18','2','2',1,1,'',6),(2,'2011-10-18','2','1',1,1,'',3000),(3,'2011-10-18','2','1',1,1,'',2000),(4,'2011-10-18','2','1',1,1,'',3000),(5,'0000-00-00','2','1',0,0,'',-3000),(6,'0000-00-00','2','1',0,0,'',-3000),(7,'0000-00-00','2','1',0,0,'',-3000),(8,'0000-00-00','2','1',0,0,'',-3000),(9,'2011-10-18','2','1',1,1,'',6967),(10,'2011-10-18','2','2',1,1,'',5),(11,'0000-00-00','2','2',0,0,'',-5),(12,'0000-00-00','2','2',0,0,'',-5),(13,'0000-00-00','2','2',0,0,'',-5),(14,'0000-00-00','2','2',0,0,'',-5),(15,'0000-00-00','2','2',0,0,'',-5),(16,'0000-00-00','2','2',0,0,'',-5),(17,'0000-00-00','2','2',0,0,'',-5),(18,'0000-00-00','2','2',0,0,'',-5),(19,'0000-00-00','2','2',0,0,'',-5),(20,'2011-10-20','2','4',1,1,'',139),(21,'2011-10-20','2','9',2,1,'2',400),(22,'2011-10-20','2','10',1,1,'',0),(23,'2011-10-21','2','13',1,1,'',0),(24,'2011-10-21','2','14',1,1,'',0),(25,'2011-10-21','2','3',1,1,'2',8120),(26,'2011-10-21','2','23',10,1,'',2790),(27,'2011-10-26','2','25',1,1,'',18),(28,'2011-10-26','2','26',7,1,'',8),(29,'2011-10-26','2','26',7,1,'',8156),(30,'2011-10-27','2','30',1,3,'',3),(31,'2011-10-27','2','31',1,1,'',580),(32,'2011-10-27','2','34',1,1,'',3),(33,'2011-10-27','2','46',1,1,'',6),(34,'2011-10-28','2','50',1,1,'',3),(35,'2011-10-28','2','50',1,1,'',0),(36,'2011-10-28','2','54',1,1,'',83304),(37,'2011-10-28','2','54',1,1,'',0),(38,'2011-10-28','2','58',1,3,'',119),(39,'2011-10-28','2','57',1,1,'',83300),(40,'2011-10-28','2','4',1,3,'',139805),(41,'2011-10-29','2','59',8,2,'',178),(42,'2011-10-29','2','56',1,3,'',4),(43,'2011-10-29','2','56',1,3,'',4),(44,'2011-10-29','2','70',1234563,1,'',0),(45,'2011-10-29','2','71',1234563,1,'',1),(46,'2011-10-29','2','72',16394696,1,'',1),(47,'2011-10-29','2','73',16394696,1,'',59),(48,'2011-10-29','2','74',16394696,1,'',59),(49,'2011-10-29','2','93',12345,1,'',1),(50,'2011-10-29','2','94',1,1,'',1),(51,'2011-10-30','2','95',7,1,'',84),(52,'2011-11-10','2','166',8,1,'',226),(53,'2011-11-17','2','168',1,1,'',0),(54,'2011-11-10','2','169',1,1,'',714),(55,'2011-11-10','2','170',10,1,'',714),(56,'2011-11-10','2','171',6,1,'',119),(57,'2011-11-10','2','171',6,1,'',118881),(58,'2011-11-10','2','172',1,3,'',0),(59,'2011-11-10','2','172',1,1,'',0),(60,'2011-11-10','2','173',1,3,'',59500),(61,'2011-11-16','2','177',1,1,'',119),(62,'2011-11-21','2','197',1,1,'',59),(63,'2011-11-21','2','201',5,3,'',58000),(64,'0000-00-00','2','201',0,0,'',-58000),(65,'0000-00-00','2','201',0,0,'',-58000),(66,'0000-00-00','2','201',0,0,'',-58000),(67,'0000-00-00','2','201',0,0,'',-58000),(68,'0000-00-00','2','201',0,0,'',-58000),(69,'0000-00-00','2','201',0,0,'',-58000),(70,'0000-00-00','2','201',0,0,'',-58000),(71,'0000-00-00','2','201',0,0,'',-58000),(72,'0000-00-00','2','201',0,0,'',-58000),(73,'0000-00-00','2','201',0,0,'',-58000),(74,'0000-00-00','2','201',0,0,'',-58000),(75,'0000-00-00','2','201',0,0,'',-58000),(76,'0000-00-00','2','201',0,0,'',-58000),(77,'0000-00-00','2','201',0,0,'',-58000),(78,'0000-00-00','2','201',0,0,'',-58000),(79,'0000-00-00','2','201',0,0,'',-58000),(80,'0000-00-00','2','201',0,0,'',-58000),(81,'0000-00-00','2','201',0,0,'',-58000),(82,'0000-00-00','2','201',0,0,'',-58000),(83,'0000-00-00','2','201',0,0,'',-58000),(84,'2011-11-21','2','200',1,3,'',59500),(85,'0000-00-00','2','201',0,0,'',-58000),(86,'0000-00-00','2','201',0,0,'',-58000),(87,'0000-00-00','2','201',0,0,'',-58000),(88,'2011-11-21','2','204',1,1,'',59),(89,'2011-11-21','2','205',1,1,'',59),(90,'2011-11-21','2','210',1,1,'',58000),(91,'0000-00-00','2','210',0,0,'',-58000),(92,'0000-00-00','2','210',0,0,'',-58000),(93,'0000-00-00','2','210',0,0,'',-58000),(94,'0000-00-00','2','210',0,0,'',-58000),(95,'0000-00-00','2','210',0,0,'',-58000),(96,'2011-11-21','2','211',1,3,'',59500),(97,'2011-11-21','2','212',1,1,'',773500),(98,'2011-11-23','2','224',0,1,'',59500),(99,'2011-11-23','2','225',10,1,'',714000);
/*!40000 ALTER TABLE `librodiario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagos`
--

DROP TABLE IF EXISTS `pagos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pagos` (
  `id` int(11) NOT NULL auto_increment,
  `codfactura` varchar(20) NOT NULL,
  `codproveedor` int(5) NOT NULL,
  `importe` int(11) default NULL,
  `codformapago` int(2) NOT NULL,
  `numdocumento` varchar(30) NOT NULL,
  `fechapago` date default '0000-00-00',
  `observaciones` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Pagos de facturas a proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pagos`
--

LOCK TABLES `pagos` WRITE;
/*!40000 ALTER TABLE `pagos` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `proveedores` (
  `codproveedor` int(5) NOT NULL auto_increment,
  `nombre` varchar(45) NOT NULL,
  `nif` varchar(12) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `codprovincia` int(2) NOT NULL,
  `localidad` varchar(35) NOT NULL,
  `codentidad` int(2) NOT NULL,
  `cuentabancaria` varchar(20) NOT NULL,
  `telefono` varchar(14) NOT NULL,
  `movil` varchar(14) NOT NULL,
  `email` varchar(35) NOT NULL,
  `web` varchar(45) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  `codpostal` varchar(255) default NULL,
  PRIMARY KEY  (`codproveedor`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='Proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES (1,'decfvgj','23456','dcfvgb',6,'c vbn',0,'123456','23456','123456','123456','123456','1',NULL),(2,'','','',0,'',0,'','','','','','1',NULL),(3,'sdsds','dsd','sdsd',15,'sdsd',0,'dsd','dsd','ds','sdsd','sdsd','1','sdsd'),(4,'sdsds','sdsd','dsd',0,'sdsd',0,'sdsd','sdsd','sdsd','sdsd','sdsdsd','0','dsds'),(5,'adentu','25433-7789','bnm,',15,'nmmmmmm',1,'12345','1234','1234','asdxedcedc','sdcddcdcdc','1','diseñ'),(6,'maracon','respaldo','dfbdgh',2,'sdfg',8,'123456','123456','123456','sdfg','dfgh','0','sdcf'),(7,'cdccccc','','',0,'',0,'','','','','','1',''),(8,'esrfdg','csbvb','dsfg',2,'dsf',1,'123456789789','sdfg','dsf','sadf','sdfg','1','sdf'),(9,'dfghbnj','fgjbnmk','dfghbjnk',2,'dfgvh',2,'dfgvhjn','dxfcgvjnmk','dfgvhbjn','dfghjbnkm','dxfgvhjnkm','1','dfghj'),(10,'maria alonso','23423','waseftgyuj',3,'esdrftyguh',2,'123456789','12345','12345','szxdcfvgbhnj','swedrftgyh','0','wsaed'),(11,'','','fvgbhnj',0,'cfvgbhnj',0,'','','','','','1',''),(12,'reberio de las mercedez','123456','',0,'',0,'','','','','','1',''),(13,'','','xdcfvgjn',0,'dcfvgbhnj',0,'','xcvgbhn','','','','1',''),(14,'','','',0,'',0,'','','','','','1',''),(15,'','','',0,'',0,'','','','','','1',''),(16,'','','',0,'',0,'','','','','','1',''),(17,'','','',0,'',0,'','','','','','1',''),(18,'','','',0,'',0,'','','','','','1',''),(19,'','','',0,'',0,'','','','','','1',''),(20,'','','',0,'',0,'','','','','','1',''),(21,'','','',0,'',0,'','','','','','1',''),(22,'jota mayuscula','15798296k','',0,'',0,'','','','','','0',''),(23,'jaime salazar','16396196-2','los quimbayas 428',8,'los angeles',8,'123456789','324567','234567','dfghbjnkm','derftgyujk','0','venta'),(24,'dfdf','','',0,'',0,'','','','','','0',''),(25,'adentu','sxdcfvg','c vb',0,'c vb',0,'','','','','','0',''),(26,'fdfdfdf','','',0,'',0,'','','','','','0',''),(27,'v bnm','','',0,'',0,'','','','','','0','');
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `provincias` (
  `codprovincia` int(2) NOT NULL auto_increment,
  `nombreprovincia` varchar(47) NOT NULL default '',
  PRIMARY KEY  (`codprovincia`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COMMENT='Provincias';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `provincias`
--

LOCK TABLES `provincias` WRITE;
/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
INSERT INTO `provincias` VALUES (1,'Region de Taparaca'),(2,'Region de Antofagasta'),(3,'Region de Atacama'),(4,'Region de Coquimbo'),(5,'Region de Valparaiso'),(6,'Region de Libertador Gral. Bernardo O\'Hoggins'),(7,'Region del Maule'),(8,'Region de Bio-Bio'),(9,'Region de la Araucania'),(10,'Region de los Lagos'),(11,'Region Aysen del Gral. Carlos Ibañez del Campo'),(12,'Region de Magallanes y La Antartica Chilena'),(13,'Region de Metropolitana'),(14,'Region de los Rios'),(15,'Region de Arica y Parinacota');
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ubicaciones`
--

DROP TABLE IF EXISTS `ubicaciones`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ubicaciones` (
  `codubicacion` int(3) NOT NULL auto_increment,
  `nombre` varchar(50) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codubicacion`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Ubicaciones';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ubicaciones`
--

LOCK TABLES `ubicaciones` WRITE;
/*!40000 ALTER TABLE `ubicaciones` DISABLE KEYS */;
INSERT INTO `ubicaciones` VALUES (1,'bandeja 1','0');
/*!40000 ALTER TABLE `ubicaciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-11-23 21:04:41
