-- MySQL dump 10.11
--
-- Host: localhost    Database: codeka
-- ------------------------------------------------------
-- Server version	5.0.51b-community-nt-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `albalinea`
--

DROP TABLE IF EXISTS `albalinea`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albalinea` (
  `codalbaran` int(11) NOT NULL default '0',
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) default NULL,
  `codigo` varchar(15) character set utf8 default NULL,
  `cantidad` float NOT NULL default '0',
  `precio` float NOT NULL default '0',
  `importe` float NOT NULL default '0',
  `dcto` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albalinea`
--

LOCK TABLES `albalinea` WRITE;
/*!40000 ALTER TABLE `albalinea` DISABLE KEYS */;
/*!40000 ALTER TABLE `albalinea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albalineap`
--

DROP TABLE IF EXISTS `albalineap`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albalineap` (
  `codalbaran` varchar(20) NOT NULL default '0',
  `codproveedor` int(5) NOT NULL default '0',
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) default NULL,
  `codigo` varchar(15) default NULL,
  `cantidad` float NOT NULL default '0',
  `precio` float NOT NULL default '0',
  `importe` float NOT NULL default '0',
  `dcto` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`codproveedor`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albalineap`
--

LOCK TABLES `albalineap` WRITE;
/*!40000 ALTER TABLE `albalineap` DISABLE KEYS */;
/*!40000 ALTER TABLE `albalineap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albalineaptmp`
--

DROP TABLE IF EXISTS `albalineaptmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albalineaptmp` (
  `codalbaran` int(11) NOT NULL default '0',
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) default NULL,
  `codigo` varchar(15) default NULL,
  `cantidad` float NOT NULL default '0',
  `precio` float NOT NULL default '0',
  `importe` float NOT NULL default '0',
  `dcto` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albalineaptmp`
--

LOCK TABLES `albalineaptmp` WRITE;
/*!40000 ALTER TABLE `albalineaptmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albalineaptmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albalineatmp`
--

DROP TABLE IF EXISTS `albalineatmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albalineatmp` (
  `codalbaran` int(11) NOT NULL default '0',
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) default NULL,
  `codigo` varchar(15) character set utf8 default NULL,
  `cantidad` float NOT NULL default '0',
  `precio` float NOT NULL default '0',
  `importe` float NOT NULL default '0',
  `dcto` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albalineatmp`
--

LOCK TABLES `albalineatmp` WRITE;
/*!40000 ALTER TABLE `albalineatmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albalineatmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albaranes`
--

DROP TABLE IF EXISTS `albaranes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albaranes` (
  `codalbaran` int(11) NOT NULL auto_increment,
  `codfactura` int(11) NOT NULL default '0',
  `fecha` date NOT NULL default '0000-00-00',
  `iva` tinyint(4) NOT NULL default '0',
  `codcliente` int(5) default '0',
  `estado` varchar(1) character set utf8 default '1',
  `totalalbaran` float NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albaranes`
--

LOCK TABLES `albaranes` WRITE;
/*!40000 ALTER TABLE `albaranes` DISABLE KEYS */;
/*!40000 ALTER TABLE `albaranes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albaranesp`
--

DROP TABLE IF EXISTS `albaranesp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albaranesp` (
  `codalbaran` varchar(20) NOT NULL default '0',
  `codproveedor` int(5) NOT NULL default '0',
  `codfactura` varchar(20) default NULL,
  `fecha` date NOT NULL default '0000-00-00',
  `iva` tinyint(4) NOT NULL default '0',
  `estado` varchar(1) default '1',
  `totalalbaran` float NOT NULL default '0',
  PRIMARY KEY  (`codalbaran`,`codproveedor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albaranesp`
--

LOCK TABLES `albaranesp` WRITE;
/*!40000 ALTER TABLE `albaranesp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albaranesp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albaranesptmp`
--

DROP TABLE IF EXISTS `albaranesptmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albaranesptmp` (
  `codalbaran` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`codalbaran`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Temporal de albaranes de proveedores para controlar acceso s';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albaranesptmp`
--

LOCK TABLES `albaranesptmp` WRITE;
/*!40000 ALTER TABLE `albaranesptmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `albaranesptmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albaranestmp`
--

DROP TABLE IF EXISTS `albaranestmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `albaranestmp` (
  `codalbaran` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`codalbaran`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Temporal de albaranes para controlar acceso simultaneo';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `albaranestmp`
--

LOCK TABLES `albaranestmp` WRITE;
/*!40000 ALTER TABLE `albaranestmp` DISABLE KEYS */;
INSERT INTO `albaranestmp` VALUES (1,'2011-10-20');
/*!40000 ALTER TABLE `albaranestmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articulos`
--

DROP TABLE IF EXISTS `articulos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `articulos` (
  `codarticulo` int(10) NOT NULL auto_increment,
  `codfamilia` int(5) NOT NULL,
  `referencia` varchar(20) NOT NULL,
  `descripcion` text NOT NULL,
  `impuesto` float NOT NULL,
  `codproveedor1` int(5) NOT NULL default '1',
  `codproveedor2` int(5) NOT NULL,
  `descripcion_corta` varchar(30) NOT NULL,
  `codubicacion` int(3) NOT NULL,
  `stock` int(10) NOT NULL,
  `stock_minimo` int(8) NOT NULL,
  `aviso_minimo` varchar(1) NOT NULL default '0',
  `datos_producto` varchar(200) NOT NULL,
  `fecha_alta` date NOT NULL default '0000-00-00',
  `codembalaje` int(3) NOT NULL,
  `unidades_caja` int(8) NOT NULL,
  `precio_ticket` varchar(1) NOT NULL default '0',
  `modificar_ticket` varchar(1) NOT NULL default '0',
  `observaciones` text NOT NULL,
  `precio_compra` float(10,2) default NULL,
  `precio_almacen` float(10,2) default NULL,
  `precio_tienda` float(10,2) default NULL,
  `precio_pvp` float(10,2) default NULL,
  `precio_iva` float(10,2) default NULL,
  `codigobarras` varchar(15) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codarticulo`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Articulos';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `articulos`
--

LOCK TABLES `articulos` WRITE;
/*!40000 ALTER TABLE `articulos` DISABLE KEYS */;
INSERT INTO `articulos` VALUES (1,1,'azsxdvfgb','dxcfvgbn',5,1,0,'dxcfvgjn',1,-3,1,'1','fcvgbn','2011-10-12',1,5,'0','','fcvgb',23.00,345.00,45.00,5000.00,89.00,'8400000000017','foto1.jpg','1'),(2,1,'ddd','dddd',5,0,0,'cccccc',1,0,0,'0','','0000-00-00',1,0,'0','0','',0.00,0.00,0.00,600.00,0.00,'8400000000024','foto2.jpg','1'),(3,1,'dfdf','sdfs',0,4,0,'',1,0,0,'0','','0000-00-00',0,0,'','','',NULL,NULL,50.00,50.00,0.00,'8400000000031','foto3.jpg','1'),(4,2,'dfdfd','fdfdfd',0,4,0,'',1,9,2,'1','','2011-10-20',0,0,'','','123',NULL,NULL,23.00,700.00,NULL,'8400000000048','foto4.jpg','1'),(5,2,'wef','efr',0,4,0,'',1,-7,5,'1','','2011-10-03',0,0,'','','sdfgdfgdfg',NULL,NULL,23.00,0.01,NULL,'8400000000055','foto5.jpg','1'),(6,1,'dffde','locura',0,6,0,'',1,-7,2,'0','','2011-10-20',0,0,'','','wert',NULL,NULL,35.00,3.01,0.00,'8400000000062','foto6.jpg','0'),(7,1,'12','qwdesdsd',0,4,0,'',1,4,2,'1','','2011-10-04',0,0,'','','wdswds',NULL,NULL,1213.00,2345.00,NULL,'8400000000079','foto7.jpg','0'),(8,3,'rodamiento','asdfg',0,6,0,'',1,0,3,'1','','2011-10-26',0,0,'','','kmjdhfdks',NULL,NULL,500.00,1200.00,NULL,'8400000000086','foto8.jpg','0'),(9,3,'dfgh','dsfdfsvsdv',0,8,0,'',1,0,2,'0','','2011-10-13',0,0,'','','dcsdcsdcsdc',NULL,NULL,50.00,500.00,NULL,'8400000000093','foto9.jpg','0'),(10,3,'azsxdvgjmk,','deftgyujk',0,8,0,'',1,8,12,'1','','2011-10-20',0,0,'','','wsecybhj',NULL,NULL,1213.00,1230.00,NULL,'8400000000109','foto10.jpg','0');
/*!40000 ALTER TABLE `articulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artpro`
--

DROP TABLE IF EXISTS `artpro`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `artpro` (
  `codarticulo` varchar(15) NOT NULL,
  `codfamilia` int(3) NOT NULL,
  `codproveedor` int(5) NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY  (`codarticulo`,`codfamilia`,`codproveedor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `artpro`
--

LOCK TABLES `artpro` WRITE;
/*!40000 ALTER TABLE `artpro` DISABLE KEYS */;
/*!40000 ALTER TABLE `artpro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `clientes` (
  `codcliente` int(5) NOT NULL auto_increment,
  `nombre` varchar(45) NOT NULL,
  `nif` varchar(12) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `codprovincia` int(2) NOT NULL default '0',
  `localidad` varchar(35) NOT NULL,
  `codformapago` int(2) NOT NULL default '0',
  `codentidad` int(2) NOT NULL default '0',
  `cuentabancaria` varchar(20) NOT NULL,
  `codpostal` varchar(5) NOT NULL,
  `telefono` varchar(14) NOT NULL,
  `movil` varchar(14) NOT NULL,
  `email` varchar(35) NOT NULL,
  `web` varchar(45) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codcliente`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='Clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'jaime','1234566-3','frgbhnj',1,'sxdvfg',0,0,'12345','2345','1234','1234','12345','1234','0'),(2,'raja','12345','wsetfrgvybhnj',3,'xdcfvg',0,0,'123456','12345','123456','1234','123456','123456','0'),(3,'dcc','cdc','dcdc',3,'dcdc',0,0,'','dcdc','cdc','cdc','cdc','cdc','1'),(4,'sdsd','sdsd','sdsd',2,'dsdsd',0,0,'','dsd','12345','2345','sdsds','dsdsd','1'),(5,'marihuana','16394698-2','bbbj',0,'nnnnn',0,0,'','nbnbn','jjjj','jjjjjj','fffff','ffffff','0'),(6,'armonil','1234566-3','fvfv',0,'vfv',0,0,'','vfvfv','vfvf','vfvffv','vfvfv','vfvfvfv','0'),(7,'zetapon','lococ','ccc',0,'xcxc',0,0,'','xcxc','cxcx','cxcxc','xcxc','cxcc','0'),(8,'alberto xapaspurri demaisaod esteril','13456879-2','los quimbayas 428',0,'los angeles',0,0,'','gbbbh','bbhbh','adentu','vgvgvgvg','vvgggvgvggv','0'),(9,'ramiro arias','q2345678','wsedfvtgbj',8,'derfgvbnjk',0,0,'','diseñ','234567','adentu',' 0','dfghj','1'),(10,'marcelo sakjdjhskjd','1234567','defghj',3,'sdfghjn',0,0,'','looka','23456','fgh','esdrftygy','dfghj','0'),(11,'ddfgfd','cvxcvx','cvcvxv',15,'xvxv',0,0,'','cvxcv','xcvxcv','cvxv','cvxv','cxvvxvx','0'),(12,'dcsc','sfzcz','xczc',0,'zxczcz',0,0,'','xczc','xzczc','zxczc','xczc','zxczczczcczczcz','0'),(13,'aswdrftgy','edfgbhnj','edfgyh',0,'dxcfvgbj',0,0,'','dfcvg','dfgvbhn','dfgh','dfgvhb','dcfgvbnj','0'),(14,'dxcfbhnjm','fdghbnj','1234567',2,'wsedrfg',0,0,'','sdcfv','1234567','sdxcfvgbjnhkm','dfgvjkmn,','dcfvgbnhjm','0'),(15,'szdfvjn','dxfghbjn','fghjnmk,',2,'fghjnkm',0,0,'','dxfgh','fcghjnmk','fghjmk','dfghjkm','dfghjk,','0'),(16,'dxcfgjn','dfghjkl','cdfvgjnmk',11,'fdghjklñ',0,0,'','dfghj','dfghjnkm','fcghjnkm','fghjkl','dxfghjkl','0'),(17,'swdrtgyuk','drftgyuk','drftgyujk',2,'rftgyuhijk',0,0,'','fdgbh','rfghujk','dfghujk','dcfvgnjmk','drftgjk','0');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cobros`
--

DROP TABLE IF EXISTS `cobros`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `cobros` (
  `id` int(11) NOT NULL auto_increment,
  `codfactura` int(11) NOT NULL,
  `codcliente` int(5) NOT NULL,
  `importe` float NOT NULL,
  `codformapago` int(2) NOT NULL,
  `numdocumento` varchar(30) NOT NULL,
  `fechacobro` date NOT NULL default '0000-00-00',
  `observaciones` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='Cobros de facturas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `cobros`
--

LOCK TABLES `cobros` WRITE;
/*!40000 ALTER TABLE `cobros` DISABLE KEYS */;
INSERT INTO `cobros` VALUES (1,2,1,6,1,'','2011-10-18',''),(2,1,1,3000,1,'','2011-10-18',''),(3,1,1,2000,1,'','2011-10-18',''),(4,1,1,3000,1,'','2011-10-18',''),(5,1,1,6967,1,'','2011-10-18',''),(6,2,1,5,1,'','2011-10-18',''),(7,4,1,139,1,'','2011-10-20',''),(8,9,2,400.2,1,'2','2011-10-20','sdsd'),(9,10,1,0,1,'','2011-10-20',''),(10,13,1,0,1,'','2011-10-21',''),(11,14,1,0,1,'','2011-10-21',''),(12,3,1,8120,1,'2','2011-10-21',''),(13,23,10,2790,1,'','2011-10-21',''),(14,25,1,17.54,1,'','2011-10-26',''),(15,26,7,8,1,'','2011-10-26',''),(16,26,7,8156.09,1,'','2011-10-26',''),(17,30,1,3.49,3,'','2011-10-27',''),(18,31,1,580,1,'','2011-10-27',''),(19,34,1,3.49,1,'','2011-10-27',''),(20,46,1,6,1,'','2011-10-27','');
/*!40000 ALTER TABLE `cobros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `embalajes`
--

DROP TABLE IF EXISTS `embalajes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `embalajes` (
  `codembalaje` int(3) NOT NULL auto_increment,
  `nombre` varchar(30) default NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codembalaje`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Embalajes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `embalajes`
--

LOCK TABLES `embalajes` WRITE;
/*!40000 ALTER TABLE `embalajes` DISABLE KEYS */;
INSERT INTO `embalajes` VALUES (1,'cincuenta','0');
/*!40000 ALTER TABLE `embalajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entidades`
--

DROP TABLE IF EXISTS `entidades`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `entidades` (
  `codentidad` int(2) NOT NULL auto_increment,
  `nombreentidad` varchar(50) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codentidad`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Entidades Bancarias';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `entidades`
--

LOCK TABLES `entidades` WRITE;
/*!40000 ALTER TABLE `entidades` DISABLE KEYS */;
INSERT INTO `entidades` VALUES (5,'Banco Credito Inversiones','0'),(1,'Banco Chile','0'),(2,'Banco Bice','0'),(3,'Banco Santander','0'),(6,'Banco Itaú','0'),(7,'Banco Estado','0'),(8,'Banaco del Desarrollo','0');
/*!40000 ALTER TABLE `entidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factulinea`
--

DROP TABLE IF EXISTS `factulinea`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `factulinea` (
  `codfactura` int(11) NOT NULL,
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  `importe` float NOT NULL,
  `dcto` tinyint(4) NOT NULL,
  PRIMARY KEY  (`codfactura`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='lineas de facturas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `factulinea`
--

LOCK TABLES `factulinea` WRITE;
/*!40000 ALTER TABLE `factulinea` DISABLE KEYS */;
INSERT INTO `factulinea` VALUES (1,1,1,'1',1,890,890,0),(2,1,1,'1',1,6000,6000,0),(3,1,1,'1',1,7000,7000,0),(4,1,1,'1',1,58800,58800,0),(4,2,1,'1',1,58800,58800,0),(9,1,1,'1',1,345,345,0),(11,1,2,'4',1,0,0,0),(14,1,2,'5',1,0,0,0),(15,1,1,'6',176,3,528,0),(16,1,1,'6',1,3,3,0),(16,2,1,'6',3,3,9,0),(18,1,2,'5',1,0,0,0),(23,1,2,'5',1,0,0,0),(23,2,1,'7',1,2345,2345,0),(25,1,1,'6',5,3.01,15.05,0),(25,2,2,'5',7,0.01,0.07,0),(26,1,1,'6',1,3.01,3.01,0),(26,2,1,'7',3,2345,7035,0),(27,1,3,'8',8,1200,9600,0),(28,1,1,'6',1,3.01,3.01,0),(29,1,1,'6',1,3.01,3.01,0),(30,1,1,'6',1,3.01,3.01,0),(31,1,3,'9',1,500,500,0),(32,1,3,'8',1,1200,1200,0),(33,1,1,'6',1,3.01,3.01,0),(34,1,1,'6',1,3.01,3.01,0),(35,1,1,'6',1,3.01,3.01,0),(36,1,3,'10',1,1230,1230,0),(37,1,1,'6',1,3.01,3.01,0),(37,2,3,'10',1,1230,1230,0),(37,3,3,'8',1,1200,1200,0),(38,1,1,'6',1,3.01,3.01,0),(39,1,1,'6',1,3.01,3.01,0),(40,1,1,'6',1,3.01,3.01,0),(41,1,1,'7',1,2345,2345,0),(41,2,1,'7',1,2345,2345,0),(41,3,3,'9',1,500,500,0),(41,4,3,'9',1,500,500,0),(42,1,1,'6',1,3.01,3.01,0),(43,1,1,'6',1,3.01,3.01,0),(46,1,1,'6',1,3.01,3.01,0),(46,2,1,'7',1,2345,2345,0),(46,3,3,'8',1,1200,1200,0),(46,4,3,'10',1,1230,1230,0),(46,5,3,'9',1,500,500,0);
/*!40000 ALTER TABLE `factulinea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factulineap`
--

DROP TABLE IF EXISTS `factulineap`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `factulineap` (
  `codfactura` varchar(20) NOT NULL default '',
  `codproveedor` int(5) NOT NULL,
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  `importe` float NOT NULL,
  `dcto` tinyint(4) NOT NULL,
  PRIMARY KEY  (`codfactura`,`codproveedor`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='lineas de facturas de proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `factulineap`
--

LOCK TABLES `factulineap` WRITE;
/*!40000 ALTER TABLE `factulineap` DISABLE KEYS */;
/*!40000 ALTER TABLE `factulineap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factulineaptmp`
--

DROP TABLE IF EXISTS `factulineaptmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `factulineaptmp` (
  `codfactura` int(11) NOT NULL,
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  `importe` float NOT NULL,
  `dcto` tinyint(4) NOT NULL,
  PRIMARY KEY  (`codfactura`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='lineas de facturas de proveedores temporal';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `factulineaptmp`
--

LOCK TABLES `factulineaptmp` WRITE;
/*!40000 ALTER TABLE `factulineaptmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `factulineaptmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factulineatmp`
--

DROP TABLE IF EXISTS `factulineatmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `factulineatmp` (
  `codfactura` int(11) NOT NULL,
  `numlinea` int(4) NOT NULL auto_increment,
  `codfamilia` int(3) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  `importe` float NOT NULL,
  `dcto` tinyint(4) NOT NULL,
  PRIMARY KEY  (`codfactura`,`numlinea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Temporal de linea de facturas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `factulineatmp`
--

LOCK TABLES `factulineatmp` WRITE;
/*!40000 ALTER TABLE `factulineatmp` DISABLE KEYS */;
INSERT INTO `factulineatmp` VALUES (4,1,1,'1',1,890,890,0),(5,1,1,'1',1,6000,6000,0),(8,1,1,'1',1,890,890,0),(9,1,1,'1',1,890,890,0),(10,1,1,'1',1,890,890,0),(13,1,1,'1',1,7000,7000,0),(14,2,1,'1',1,58800,58800,0),(14,1,1,'1',1,58800,58800,0),(21,1,1,'1',1,345,345,0),(25,1,1,'2',1,600,600,0),(25,2,1,'1',2,5000,10000,0),(26,1,1,'3',1,0,0,0),(27,1,2,'4',1,0,0,0),(28,1,1,'6',1,2,2,0),(29,1,1,'2',1,600,600,0),(33,1,2,'5',1,0,0,0),(32,1,1,'6',176,3,528,0),(37,1,1,'6',1,3,3,0),(37,2,1,'6',3,3,9,0),(40,1,2,'5',1,0,0,0),(45,1,2,'5',1,0,0,0),(45,2,1,'7',1,2345,2345,0),(62,1,1,'6',1,3.01,3.01,0),(63,1,1,'6',5,3.01,15.05,0),(63,2,2,'5',7,0.01,0.07,0),(64,1,1,'6',1,3.01,3.01,0),(64,2,1,'7',3,2345,7035,0),(66,1,3,'8',8,1200,9600,0),(67,1,3,'8',1,1200,1200,0),(69,1,1,'6',1,3.01,3.01,0),(70,1,1,'6',1,3.01,3.01,0),(73,1,1,'6',1,3.01,3.01,0),(76,1,3,'8',8,1200,9600,0),(77,1,1,'6',1,3.01,3.01,0),(78,1,3,'9',1,500,500,0),(79,1,3,'8',1,1200,1200,0),(81,1,1,'6',1,3.01,3.01,0),(80,1,1,'6',1,3.01,3.01,0),(82,1,1,'6',1,3.01,3.01,0),(84,1,3,'10',1,1230,1230,0),(85,1,1,'6',1,3.01,3.01,0),(85,2,3,'10',1,1230,1230,0),(85,3,3,'8',1,1200,1200,0),(86,1,1,'6',1,3.01,3.01,0),(87,1,1,'6',1,3.01,3.01,0),(88,1,1,'6',1,3.01,3.01,0),(89,1,1,'7',1,2345,2345,0),(89,2,1,'7',1,2345,2345,0),(89,3,3,'9',1,500,500,0),(89,4,3,'9',1,500,500,0),(90,1,1,'6',1,3.01,3.01,0),(91,1,1,'6',1,3.01,3.01,0),(99,1,1,'6',1,3.01,3.01,0),(99,2,1,'7',1,2345,2345,0),(99,3,3,'8',1,1200,1200,0),(99,4,3,'10',1,1230,1230,0),(99,5,3,'9',1,500,500,0);
/*!40000 ALTER TABLE `factulineatmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturas`
--

DROP TABLE IF EXISTS `facturas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facturas` (
  `codfactura` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL,
  `iva` tinyint(4) NOT NULL,
  `codcliente` int(5) NOT NULL,
  `estado` varchar(1) NOT NULL default '0',
  `totalfactura` float NOT NULL,
  `fechavencimiento` date NOT NULL default '0000-00-00',
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codfactura`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='facturas de ventas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facturas`
--

LOCK TABLES `facturas` WRITE;
/*!40000 ALTER TABLE `facturas` DISABLE KEYS */;
INSERT INTO `facturas` VALUES (1,'2011-10-18',16,1,'2',1032.4,'0000-00-00','0'),(2,'2011-10-18',16,1,'2',6960,'0000-00-00','0'),(3,'2011-10-20',16,1,'2',8120,'0000-00-00','0'),(4,'2011-10-20',19,1,'2',139944,'0000-00-00','0'),(5,'2011-10-20',16,1,'1',0,'0000-00-00','0'),(6,'2011-10-20',16,1,'1',0,'0000-00-00','0'),(7,'2011-10-20',16,1,'1',0,'0000-00-00','0'),(8,'2011-10-20',16,1,'1',0,'0000-00-00','0'),(9,'2011-10-20',16,2,'2',400.2,'0000-00-00','0'),(10,'2011-10-20',16,1,'2',0,'0000-00-00','0'),(11,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(12,'2011-10-21',16,7,'1',0,'0000-00-00','0'),(13,'2011-10-21',16,1,'2',0,'0000-00-00','0'),(14,'2011-10-21',16,1,'2',0,'0000-00-00','0'),(15,'2011-10-21',16,1,'1',612.48,'0000-00-00','0'),(16,'2011-10-21',16,2,'1',13.92,'0000-00-00','0'),(17,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(18,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(19,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(20,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(21,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(22,'2011-10-21',16,1,'1',0,'0000-00-00','0'),(23,'2011-10-21',19,10,'2',2790.55,'0000-00-00','0'),(24,'2011-10-25',16,1,'1',0,'0000-00-00','0'),(25,'2011-10-26',16,1,'2',17.5392,'0000-00-00','0'),(26,'2011-10-26',16,7,'2',8164.09,'0000-00-00','0'),(27,'2011-10-26',16,7,'1',11136,'0000-00-00','0'),(28,'2011-10-27',16,1,'1',3.4916,'0000-00-00','0'),(29,'2011-10-27',16,1,'1',3.4916,'0000-00-00','0'),(30,'2011-10-27',16,1,'2',3.4916,'0000-00-00','0'),(31,'2011-10-27',16,1,'2',580,'0000-00-00','0'),(32,'2011-10-27',16,1,'1',1392,'0000-00-00','0'),(33,'2011-10-27',16,1,'1',3.4916,'0000-00-00','0'),(34,'2011-10-27',16,1,'2',3.4916,'0000-00-00','0'),(35,'2011-10-27',16,1,'1',3.4916,'0000-00-00','0'),(36,'2011-10-27',16,1,'1',1426.8,'0000-00-00','0'),(37,'2011-10-27',16,1,'1',2822.29,'0000-00-00','0'),(38,'2011-10-27',16,1,'1',3.4916,'0000-00-00','0'),(39,'2011-10-27',16,1,'1',3.4916,'0000-00-00','0'),(40,'2011-10-27',16,1,'1',3.4916,'0000-00-00','0'),(41,'2011-10-27',16,1,'1',6600.4,'0000-00-00','0'),(42,'2011-10-27',16,1,'1',3.4916,'0000-00-00','0'),(43,'2011-10-27',16,1,'1',3.4916,'0000-00-00','0'),(44,'2011-10-27',19,1,'1',0,'0000-00-00','0'),(45,'2011-10-27',19,1,'1',0,'0000-00-00','0'),(46,'2011-10-27',19,1,'2',6280.83,'0000-00-00','1');
/*!40000 ALTER TABLE `facturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturasp`
--

DROP TABLE IF EXISTS `facturasp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facturasp` (
  `codfactura` varchar(20) NOT NULL default '',
  `codproveedor` int(5) NOT NULL,
  `fecha` date NOT NULL,
  `iva` tinyint(4) NOT NULL,
  `estado` varchar(1) NOT NULL default '0',
  `totalfactura` float NOT NULL,
  `fechapago` date NOT NULL default '0000-00-00',
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codfactura`,`codproveedor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='facturas de compras a proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facturasp`
--

LOCK TABLES `facturasp` WRITE;
/*!40000 ALTER TABLE `facturasp` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturasp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturasptmp`
--

DROP TABLE IF EXISTS `facturasptmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facturasptmp` (
  `codfactura` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL,
  PRIMARY KEY  (`codfactura`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='temporal de facturas de proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facturasptmp`
--

LOCK TABLES `facturasptmp` WRITE;
/*!40000 ALTER TABLE `facturasptmp` DISABLE KEYS */;
INSERT INTO `facturasptmp` VALUES (1,'2011-10-18');
/*!40000 ALTER TABLE `facturasptmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturastmp`
--

DROP TABLE IF EXISTS `facturastmp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `facturastmp` (
  `codfactura` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL,
  PRIMARY KEY  (`codfactura`)
) ENGINE=MyISAM AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COMMENT='temporal de facturas a clientes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `facturastmp`
--

LOCK TABLES `facturastmp` WRITE;
/*!40000 ALTER TABLE `facturastmp` DISABLE KEYS */;
INSERT INTO `facturastmp` VALUES (1,'0000-00-00'),(2,'0000-00-00'),(3,'0000-00-00'),(4,'0000-00-00'),(5,'0000-00-00'),(6,'0000-00-00'),(7,'0000-00-00'),(8,'2011-10-18'),(9,'2011-10-18'),(10,'2011-10-18'),(11,'0000-00-00'),(12,'0000-00-00'),(13,'0000-00-00'),(14,'0000-00-00'),(15,'0000-00-00'),(16,'2011-10-20'),(17,'2011-10-20'),(18,'0000-00-00'),(19,'0000-00-00'),(20,'0000-00-00'),(21,'2011-10-20'),(22,'0000-00-00'),(23,'0000-00-00'),(24,'0000-00-00'),(25,'0000-00-00'),(26,'0000-00-00'),(27,'0000-00-00'),(28,'0000-00-00'),(29,'0000-00-00'),(30,'0000-00-00'),(31,'0000-00-00'),(32,'0000-00-00'),(33,'0000-00-00'),(34,'0000-00-00'),(35,'0000-00-00'),(36,'0000-00-00'),(37,'0000-00-00'),(38,'0000-00-00'),(39,'0000-00-00'),(40,'0000-00-00'),(41,'0000-00-00'),(42,'0000-00-00'),(43,'0000-00-00'),(44,'0000-00-00'),(45,'0000-00-00'),(46,'0000-00-00'),(47,'2011-10-21'),(48,'0000-00-00'),(49,'0000-00-00'),(50,'0000-00-00'),(51,'2011-10-25'),(52,'0000-00-00'),(53,'0000-00-00'),(54,'0000-00-00'),(55,'0000-00-00'),(56,'0000-00-00'),(57,'0000-00-00'),(58,'0000-00-00'),(59,'0000-00-00'),(60,'0000-00-00'),(61,'0000-00-00'),(62,'0000-00-00'),(63,'0000-00-00'),(64,'0000-00-00'),(65,'2011-10-26'),(66,'0000-00-00'),(67,'0000-00-00'),(68,'0000-00-00'),(69,'0000-00-00'),(70,'0000-00-00'),(71,'0000-00-00'),(72,'0000-00-00'),(73,'0000-00-00'),(74,'0000-00-00'),(75,'0000-00-00'),(76,'2011-10-27'),(77,'0000-00-00'),(78,'0000-00-00'),(79,'0000-00-00'),(80,'0000-00-00'),(81,'0000-00-00'),(82,'0000-00-00'),(83,'0000-00-00'),(84,'0000-00-00'),(85,'0000-00-00'),(86,'0000-00-00'),(87,'0000-00-00'),(88,'0000-00-00'),(89,'0000-00-00'),(90,'0000-00-00'),(91,'0000-00-00'),(92,'0000-00-00'),(93,'0000-00-00'),(94,'0000-00-00'),(95,'0000-00-00'),(96,'0000-00-00'),(97,'0000-00-00'),(98,'0000-00-00'),(99,'0000-00-00'),(100,'0000-00-00'),(101,'0000-00-00'),(102,'0000-00-00'),(103,'0000-00-00'),(104,'0000-00-00');
/*!40000 ALTER TABLE `facturastmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familias`
--

DROP TABLE IF EXISTS `familias`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `familias` (
  `codfamilia` int(5) NOT NULL auto_increment,
  `nombre` varchar(20) default NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codfamilia`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='familia de articulos';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `familias`
--

LOCK TABLES `familias` WRITE;
/*!40000 ALTER TABLE `familias` DISABLE KEYS */;
INSERT INTO `familias` VALUES (1,'sector a','0'),(2,'sector b','0'),(3,'segundo piso','0');
/*!40000 ALTER TABLE `familias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formapago`
--

DROP TABLE IF EXISTS `formapago`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `formapago` (
  `codformapago` int(2) NOT NULL auto_increment,
  `nombrefp` varchar(40) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codformapago`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Forma de pago';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `formapago`
--

LOCK TABLES `formapago` WRITE;
/*!40000 ALTER TABLE `formapago` DISABLE KEYS */;
INSERT INTO `formapago` VALUES (1,'contado','0'),(2,'targetas','0'),(3,'cheque','0');
/*!40000 ALTER TABLE `formapago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `impuestos`
--

DROP TABLE IF EXISTS `impuestos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `impuestos` (
  `codimpuesto` int(3) NOT NULL auto_increment,
  `nombre` varchar(20) default NULL,
  `valor` float NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codimpuesto`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='tipos de impuestos';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `impuestos`
--

LOCK TABLES `impuestos` WRITE;
/*!40000 ALTER TABLE `impuestos` DISABLE KEYS */;
INSERT INTO `impuestos` VALUES (1,'loco',5,'0');
/*!40000 ALTER TABLE `impuestos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `librodiario`
--

DROP TABLE IF EXISTS `librodiario`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `librodiario` (
  `id` int(8) NOT NULL auto_increment,
  `fecha` date NOT NULL default '0000-00-00',
  `tipodocumento` varchar(1) NOT NULL,
  `coddocumento` varchar(20) NOT NULL,
  `codcomercial` int(5) NOT NULL,
  `codformapago` int(2) NOT NULL,
  `numpago` varchar(30) NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='Movimientos diarios';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `librodiario`
--

LOCK TABLES `librodiario` WRITE;
/*!40000 ALTER TABLE `librodiario` DISABLE KEYS */;
INSERT INTO `librodiario` VALUES (1,'2011-10-18','2','2',1,1,'',6),(2,'2011-10-18','2','1',1,1,'',3000),(3,'2011-10-18','2','1',1,1,'',2000),(4,'2011-10-18','2','1',1,1,'',3000),(5,'0000-00-00','2','1',0,0,'',-3000),(6,'0000-00-00','2','1',0,0,'',-3000),(7,'0000-00-00','2','1',0,0,'',-3000),(8,'0000-00-00','2','1',0,0,'',-3000),(9,'2011-10-18','2','1',1,1,'',6967),(10,'2011-10-18','2','2',1,1,'',5),(11,'0000-00-00','2','2',0,0,'',-5),(12,'0000-00-00','2','2',0,0,'',-5),(13,'0000-00-00','2','2',0,0,'',-5),(14,'0000-00-00','2','2',0,0,'',-5),(15,'0000-00-00','2','2',0,0,'',-5),(16,'0000-00-00','2','2',0,0,'',-5),(17,'0000-00-00','2','2',0,0,'',-5),(18,'0000-00-00','2','2',0,0,'',-5),(19,'0000-00-00','2','2',0,0,'',-5),(20,'2011-10-20','2','4',1,1,'',139),(21,'2011-10-20','2','9',2,1,'2',400.2),(22,'2011-10-20','2','10',1,1,'',0),(23,'2011-10-21','2','13',1,1,'',0),(24,'2011-10-21','2','14',1,1,'',0),(25,'2011-10-21','2','3',1,1,'2',8120),(26,'2011-10-21','2','23',10,1,'',2790),(27,'2011-10-26','2','25',1,1,'',17.54),(28,'2011-10-26','2','26',7,1,'',8),(29,'2011-10-26','2','26',7,1,'',8156.09),(30,'2011-10-27','2','30',1,3,'',3.49),(31,'2011-10-27','2','31',1,1,'',580),(32,'2011-10-27','2','34',1,1,'',3.49),(33,'2011-10-27','2','46',1,1,'',6);
/*!40000 ALTER TABLE `librodiario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagos`
--

DROP TABLE IF EXISTS `pagos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pagos` (
  `id` int(11) NOT NULL auto_increment,
  `codfactura` varchar(20) NOT NULL,
  `codproveedor` int(5) NOT NULL,
  `importe` float NOT NULL,
  `codformapago` int(2) NOT NULL,
  `numdocumento` varchar(30) NOT NULL,
  `fechapago` date default '0000-00-00',
  `observaciones` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Pagos de facturas a proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pagos`
--

LOCK TABLES `pagos` WRITE;
/*!40000 ALTER TABLE `pagos` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `proveedores` (
  `codproveedor` int(5) NOT NULL auto_increment,
  `nombre` varchar(45) NOT NULL,
  `nif` varchar(12) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `codprovincia` int(2) NOT NULL,
  `localidad` varchar(35) NOT NULL,
  `codentidad` int(2) NOT NULL,
  `cuentabancaria` varchar(20) NOT NULL,
  `telefono` varchar(14) NOT NULL,
  `movil` varchar(14) NOT NULL,
  `email` varchar(35) NOT NULL,
  `web` varchar(45) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  `codpostal` varchar(255) default NULL,
  PRIMARY KEY  (`codproveedor`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Proveedores';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES (1,'decfvgj','23456','dcfvgb',6,'c vbn',0,'123456','23456','123456','123456','123456','1',NULL),(2,'','','',0,'',0,'','','','','','1',NULL),(3,'sdsds','dsd','sdsd',15,'sdsd',0,'dsd','dsd','ds','sdsd','sdsd','1','sdsd'),(4,'sdsds','sdsd','dsd',0,'sdsd',0,'sdsd','sdsd','sdsd','sdsd','sdsdsd','0','dsds'),(5,'adentu','25433-7789','bnm,',15,'nmmmmmm',1,'12345','1234','1234','asdxedcedc','sdcddcdcdc','1','diseñ'),(6,'jaime de las mercedez','1234563-2','dfbdgh',2,'sdfg',8,'123456','123456','123456','sdfg','dfgh','0','sdcf'),(7,'cdccccc','','',0,'',0,'','','','','','1',''),(8,'esrfdg','csbvb','dsfg',2,'dsf',1,'123456789789','sdfg','dsf','sadf','sdfg','0','sdf'),(9,'dfghbnj','fgjbnmk','dfghbjnk',2,'dfgvh',2,'dfgvhjn','dxfcgvjnmk','dfgvhbjn','dfghjbnkm','dxfgvhjnkm','1','dfghj');
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `provincias` (
  `codprovincia` int(2) NOT NULL auto_increment,
  `nombreprovincia` varchar(47) NOT NULL default '',
  PRIMARY KEY  (`codprovincia`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COMMENT='Provincias';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `provincias`
--

LOCK TABLES `provincias` WRITE;
/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
INSERT INTO `provincias` VALUES (1,'Region de Taparaca'),(2,'Region de Antofagasta'),(3,'Region de Atacama'),(4,'Region de Coquimbo'),(5,'Region de Valparaiso'),(6,'Region de Libertador Gral. Bernardo O\'Hoggins'),(7,'Region del Maule'),(8,'Region de Bio-Bio'),(9,'Region de la Araucania'),(10,'Region de los Lagos'),(11,'Region Aysen del Gral. Carlos Ibañez del Campo'),(12,'Region de Magallanes y La Antartica Chilena'),(13,'Region de Metropolitana'),(14,'Region de los Rios'),(15,'Region de Arica y Parinacota');
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ubicaciones`
--

DROP TABLE IF EXISTS `ubicaciones`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ubicaciones` (
  `codubicacion` int(3) NOT NULL auto_increment,
  `nombre` varchar(50) NOT NULL,
  `borrado` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`codubicacion`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Ubicaciones';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ubicaciones`
--

LOCK TABLES `ubicaciones` WRITE;
/*!40000 ALTER TABLE `ubicaciones` DISABLE KEYS */;
INSERT INTO `ubicaciones` VALUES (1,'bandeja 1','0');
/*!40000 ALTER TABLE `ubicaciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-10-27 22:13:38
