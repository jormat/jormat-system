<?php include ("../conectar.php"); ?>
<html>
	<head>
		<title>Principal</title>
		<link href="../estilos/estilos.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" src="../funciones/validar.js"></script>
		<script language="javascript">
		
		function cancelar() {
			location.href="index.php";
		}
		
		var cursor;
		if (document.all) {
		// Está utilizando EXPLORER
		cursor='hand';
		} else {
		// Está utilizando MOZILLA/NETSCAPE
		cursor='pointer';
		}
		
		function limpiar() {
			document.getElementById("formulario").reset();
		}
			
		</script>
	</head>
	<body>
		<div id="pagina">
			<div id="zonaContenido">
				<div align="center">
				<div id="tituloForm" class="header">INSERTAR FORMA DE PAGO </div>
				<div id="frmBusqueda">
				<form id="formulario" name="formulario" method="post" action="guardar_fp.php">
					<table class="fuente8" width="98%" cellspacing=0 cellpadding=3 border=0>
						<tr>
							<td width="14%" class="Menu">Nombre</td>
						    <td width="36%"><input NAME="Anombrefp" type="text" class="cajaGrande" id="nombrefp" size="45" maxlength="45"></td>
				            <td width="50%"><ul id="lista-errores"></ul></td>
						</tr>						
					</table>
			  </div>
				<div id="botonBusqueda">
					<input type="hidden" name="id" id="id" value="">
					<img src="../img/notification_done.png" width="62" height="50" onClick="validar(formulario,true)" title="Aceptar" onMouseOver="style.cursor=cursor">
					<img src="../img/file.png" width="62" height="50" onClick="limpiar()" title="Limpiar" onMouseOver="style.cursor=cursor">
					<img src="../img/notification_error.png" width="62" height="50" onClick="cancelar()" title="Cancelar" onMouseOver="style.cursor=cursor">
					<input id="accion" name="accion" value="alta" type="hidden">
			  </div>
			  </form>
			 </div>
		  </div>
		</div>
	</body>
</html>
