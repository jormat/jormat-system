<?php 

session_start();
if($_SESSION["usuario"]){
	
header('Cache-Control: no-cache');
header('Pragma: no-cache'); 

include ("../conectar.php"); ?>
<html>
	<head>
		<title>Principal</title>
		<link href="../estilos/estilos.css" type="text/css" rel="stylesheet">
		<link href="../calendario/calendar-blue.css" rel="stylesheet" type="text/css">
		<script type="text/JavaScript" language="javascript" src="../calendario/calendar.js"></script>
		<script type="text/JavaScript" language="javascript" src="../calendario/lang/calendar-sp.js"></script>
		<script type="text/JavaScript" language="javascript" src="../calendario/calendar-setup.js"></script>
		<script type="text/javascript" src="../funciones/validar.js"></script>
		<script language="javascript">
function cancelar() {
			location.href="index.php";
		}
		
		var cursor;
		if (document.all) {
		// Está utilizando EXPLORER
		cursor='hand';
		} else {
		// Está utilizando MOZILLA/NETSCAPE
		cursor='pointer';
		}
		
		function limpiar() {
			document.getElementById("referencia").value="";
			document.getElementById("descripcion").value="";
			document.getElementById("descripcion_corta").value="";
			document.getElementById("stock_minimo").value="";
			document.getElementById("stock").value="";
			document.getElementById("datos").value="";
			document.getElementById("fecha").value="";
			document.getElementById("unidades_caja").value="";
			document.getElementById("observaciones").value="";
			document.getElementById("precio_compra").value="";
			document.getElementById("precio_almacen").value="";
			document.getElementById("precio_tienda").value="";
			//document.getElementById("pvp").value="";
			document.getElementById("precio_iva").value="";
			document.getElementById("foto").value="";
			document.formulario.cboFamilias.options[0].selected = true;
			document.formulario.cboImpuestos.options[0].selected = true;
			document.formulario.cboProveedores1.options[0].selected = true;
			document.formulario.cboProveedores2.options[0].selected = true;
			document.formulario.cboUbicacion.options[0].selected = true;
			document.formulario.cboEmbalaje.options[0].selected = true;
			document.formulario.Aaviso_minimo.options[0].selected = true;
			document.formulario.Aprecio_ticket.options[0].selected = true;
			document.formulario.Amodif_descrip.options[0].selected = true;
		}
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' El valor debe ser numerico.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' Es necesario.\n'; }
    } if (errors) alert('Complete los campos obligatorios:\n\n'+errors);
    document.MM_returnValue = (errors == '');
} }
        </script>
	</head>
	<body>
		<div id="pagina">
			<div id="zonaContenido">
				<div align="center">
				<div id="tituloForm" class="header">INSERTAR ARTICULO CHILLAN</div>
				<div id="frmBusqueda">
				<form id="formulario" name="formulario" method="post" action="guardar_articulo.php" enctype="multipart/form-data">
				<input id="accion" name="accion" value="alta" type="hidden">
					<table class="fuente8" width="98%" cellspacing=0 cellpadding=3 border=0>
						<tr>
						<td class="aleft">Referencia</td>
					      <td ><input name="Referencia" id="referencia" value="" maxlength="15" class="cajaGrande" type="text"></td>
				          <td width="55%" rowspan="9" align="left" valign="top"><ul id="lista-errores"></ul></td>
						</tr>
                        <tr>
						<td class="aleft">Referencia2</td>
					      <td ><input name="Referencia2" id="referencia2" value="" maxlength="15" class="cajaGrande" type="text"></td>
				          <td width="55%" rowspan="9" align="left" valign="top"><ul id="lista-errores"></ul></td>
						</tr>
                        <tr>
						<td class="aleft">Referencia3</td>
					      <td ><input name="Referencia3" id="referencia3" value="" maxlength="15" class="cajaGrande" type="text"></td>
				          <td width="55%" rowspan="9" align="left" valign="top"><ul id="lista-errores"></ul></td>
						</tr>
                        <tr>
						<td class="aleft">Referencia4</td>
					      <td ><input name="Referencia4" id="referencia4" value="" maxlength="15" class="cajaGrande" type="text"></td>
				          <td width="55%" rowspan="9" align="left" valign="top"><ul id="lista-errores"></ul></td>
						</tr>
                        <tr>
						<td class="aleft">Referencia5</td>
					      <td ><input name="Referencia5" id="referencia5" value="" maxlength="15" class="cajaGrande" type="text"></td>
				          <td width="55%" rowspan="9" align="left" valign="top"><ul id="lista-errores"></ul></td>
						</tr>
                        <tr>
						<td class="aleft">Referencia6</td>
					      <td ><input name="Referencia6" id="referencia6" value="" maxlength="15" class="cajaGrande" type="text"></td>
				          <td width="55%" rowspan="9" align="left" valign="top"><ul id="lista-errores"></ul></td>
						</tr>
						<?php
					  	$query_familias="SELECT * FROM familias WHERE borrado=0 ORDER BY nombre ASC";
						$res_familias=mysql_query($query_familias);
						$contador=0;
					  ?>
						<tr>
							<td width="17%" class="aleft">Sector</td>
							<td><select id="cboFamilias" name="AcboFamilias" class="comboGrande">
							
								<option value="0">Seleccione Sector</option>
								<?php
								while ($contador < mysql_num_rows($res_familias)) { ?>
								<option value="<?php echo mysql_result($res_familias,$contador,"codfamilia")?>"><?php echo mysql_result($res_familias,$contador,"nombre")?></option>
								<?php $contador++;
								} ?>				
								</select>							</td>
				        </tr>
						<tr>
							<td  class="aleft">Descripci&oacute;n</td>
						    <td>
                            <input name="Descripcion" id="descripcion" value="" class="cajaGrande2" type="text"></td>
				        </tr>
						<?php
					  	$query_impuesto="SELECT codimpuesto,valor FROM impuestos WHERE borrado=0 ORDER BY nombre ASC";
						$res_impuesto=mysql_query($query_impuesto);
						$contador=0;
					  ?>
						<?php
					  	$query_proveedores="SELECT codproveedor,nombre,nif FROM proveedores WHERE borrado=0 ORDER BY nombre ASC";
						$res_proveedores=mysql_query($query_proveedores);
						$contador=0;
					  ?>
						<tr>
							<td class="aleft">Proveedor </td>
							<td><select id="cboProveedores1" name="acboProveedores1" class="comboGrande">
							<option value="0">Todos los proveedores</option>
								<?php
								while ($contador < mysql_num_rows($res_proveedores)) { 
									if ( mysql_result($res_proveedores,$contador,"codproveedor") == $proveedor) { ?>
								<option value="<?php echo mysql_result($res_proveedores,$contador,"codproveedor")?>" selected><?php echo mysql_result($res_proveedores,$contador,"nif")?> -- <?php echo mysql_result($res_proveedores,$contador,"nombre")?></option>
								<?php } else { ?> 
								<option value="<?php echo mysql_result($res_proveedores,$contador,"codproveedor")?>"><?php echo mysql_result($res_proveedores,$contador,"nif")?> -- <?php echo mysql_result($res_proveedores,$contador,"nombre")?></option>
								<?php }
								$contador++;
								} ?>				
								</select>							</td>
					    </tr>
					<?php
					  	$query_proveedores="SELECT codproveedor,nombre,nif FROM proveedores WHERE borrado=0 ORDER BY nombre ASC";
						$res_proveedores=mysql_query($query_proveedores);
						$contador=0;
					  ?>
					  <?php
					  	$query_ubicacion="SELECT codubicacion,nombre FROM ubicaciones WHERE borrado=0 ORDER BY nombre ASC";
						$res_ubicacion=mysql_query($query_ubicacion);
						$contador=0;
					  ?>
						<tr>
							<td class="aleft">Ubicaci&oacute;n</td>
							<td><select id="cboUbicacion" name="AcboUbicacion" class="comboGrande">
							<option value="0">Todas las ubicaciones</option>
								<?php
								while ($contador < mysql_num_rows($res_ubicacion)) { 
									if ( mysql_result($res_ubicacion,$contador,"codubicacion") == $ubicacion) { ?>
								<option value="<?php echo mysql_result($res_ubicacion,$contador,"codubicacion")?>" selected><?php echo mysql_result($res_ubicacion,$contador,"nombre")?></option>
								<?php } else { ?> 
								<option value="<?php echo mysql_result($res_ubicacion,$contador,"codubicacion")?>"><?php echo mysql_result($res_ubicacion,$contador,"nombre")?></option>
								<?php }
								$contador++;
								} ?>				
								</select>							</td>
					    </tr>
						<tr>
						 <td class="aleft">Stock</td>
						  <td class="Menu2"><input NAME="Stock" type="text" class="cajaPequena" id="stock" size="10" maxlength="10"> unidades</td>
				      </tr>
					  	<tr>
						 <td class="aleft">Stock m&iacute;nimo</td>
						  <td class="Menu2"><input NAME="Stock_minimo" type="text" class="cajaPequena" id="stock_minimo" size="8" maxlength="8"> unidades</td>
				      </tr>
					  	<tr>
						 <td class="aleft">Aviso m&iacute;nimo</td>
						  <td><select name="aaviso_minimo" id="aviso_minimo" class="comboPequeno2">
						  <option value="0">No</option>
						  <option value="1" selected>Si</option>
						  </select></td>
				      </tr>
					  <tr>
						  <td class="aleft">Fecha de ingreso</td>
						  <td><input NAME="fecha" type="text" class="cajaPequena" id="fecha" size="10" maxlength="10" readonly> <img src="../img/calendario.png" name="Image1" id="Image1" width="16" height="16" border="0"  onMouseOver="this.style.cursor='pointer'">
        <script type="text/javascript">
					Calendar.setup(
					  {
					inputField : "fecha",
					ifFormat   : "%d/%m/%Y",
					button     : "Image1"
					  }
					);
		</script></td>
					    </tr>
						 <?php
					  	$query_embalaje="SELECT codembalaje,nombre FROM embalajes WHERE borrado=0 ORDER BY nombre ASC";
						$res_embalaje=mysql_query($query_embalaje);
						$contador=0;
					  ?>
					  <tr>
						 <td width="17%" class="aleft">Observaciones</td>
					     <td><textarea name="aobservaciones" cols="41" rows="2" id="observaciones" class="areaTexto"></textarea></td>
				        </tr>
						<tr>
						  <td class="aleft">Precio de compra</td>
						  <td class="Menu2">$
						    <input name="Precio_compra" type="text" class="cajaPequena" id="precio_compra" size="10" maxlength="10"></td>
				      </tr>
					  	<tr>
						  <td class="aleft">Precio Venta Neto</td>
						  <td class="Menu2"><input NAME="Precio_venta" type="text" class="cajaPequena" id="precio_almacen" size="10" maxlength="10">
						    $</td>
				      </tr>
						<!--<tr>
						  <td>Pvp</td>
						  <td><input NAME="qpvp" type="text" class="cajaPequena" id="pvp" size="10" maxlength="10"> &#8364;</td>
				      </tr>-->
					  <tr>
						  <td class="aleft">Imagen [Formato jpg] [200x200]</td>
						  <td><input type="file" name="foto" id="foto" class="cajaPequena" accept="image/jpg" /></td>
				      </tr>
					</table>
			  </div>
				<div id="botonBusqueda">
				<input title="Aceptar" alt=" conton comprar " src="../img/notification_done.png" type="image" onClick="MM_validateForm('referencia','','R','descripcion','','R','stock','','RisNum','fecha','','R','precio_compra','','RisNum','precio_almacen','','RisNum');return document.MM_returnValue" value="Enviar" width="62" height="50"/>
 
					<img src="../img/file.png" width="62" height="50" onClick="limpiar()" title="Limpiar" onMouseOver="style.cursor=cursor">
					<img src="../img/notification_error.png" width="62" height="50" onClick="cancelar()" title="Cancelar" onMouseOver="style.cursor=cursor">
					<input type="hidden" name="id" id="id" value="">					
			  </div>
			  </form>	
			 </div>			
		  </div>
		</div>
	</body>
</html>
<?php

}else
{
	echo "<script type='text/javascript'>
		alert('Usted no posee permisos suficientes');
		window.location='http://importadorajormat.cl/jormat/';
	</script>";
}
?>