
<!DOCTYPE html>
<html>
<title>Sistema JORMAT To Factura Chile</title>
<body bgcolor="#F2F2F2">

<h1><font face="Cambria">Exito!</font></h1>

<p><font face="Cambria">La Nota de credito <strong>N° <?php print $_GET["noteId"] ?> </strong> ha sido enviada a plataforma Factura Chile.</font></p>
<p><font face="Cambria">Para validar Visite <a href="https://www.facturachile.cl/" target="_blank">Facturachile.cl</a></font></p>
<div>
  <form name="form1" target="_blank" method="post">
    <input type="button" onclick="window.close();" value="Cerrar" class="boton">
  </form>
</div>
</body>
<style type="text/css">
  .boton{
        font-size:10px;
        font-family:Verdana,Helvetica;
        font-weight:bold;
        color:white;
        background:#04B431;
        border:0px;
        width:80px;
        height:19px;
       }
</style>

</html>
<?php

include ("conection.php");

$noteId=$_GET["noteId"];

$queryNote = "SELECT DISTINCT
                        i.noteId as noteId,
                        c.dsName AS clientName,
                        c.dsCode AS rut,
                        i.invoiceId,
                        c.id AS clientId,
                        i.total,
                        ROUND(i.total/1.19,0) as netPrice,
                        i.total - ROUND(i.total/1.19)  as priceVAT,
                        w.dsCode AS origin,
                        DATE(i.date) as date,
                        u.usName AS userCreation,
                        if(i.status=1, 'Sin Pagar','Pagado') AS statusName,
                        i.commentary AS comment
                        
                    FROM creditnotes i
                    inner JOIN  clients c
                    ON  i.clientId = c.id
                    left JOIN  warehouse w
                    ON  i.origin = w.id
                    left JOIN  users u
                    ON  i.usId = u.usId
                    WHERE i.noteId = '$noteId'";

$noteResult = mysql_query($queryNote);
$note=mysql_fetch_array($noteResult);
$xml= '<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?> ';

$xml .= ' <DTE version="1.0">';

$xml .= ' <Documento ID="R77712600-8T33F35"> ';

$xml .= "\t <Encabezado>\n";

$xml .= "\t <IdDoc>\n";
$xml .= "\t <TipoDTE>61</TipoDTE>\n";
$xml .= "\t <Folio>" .$noteId ."</Folio>\n";
$xml .= "\t <FchEmis>" .$note["date"] ."</FchEmis>\n";
$xml .= "\t <FchVenc>" .$note["date"] ."</FchVenc>\n";
$xml .= "\t </IdDoc>\n";

$xml .= "\t <Emisor> \n";
$xml .= "\t <RUTEmisor>76193704-9</RUTEmisor> \n";
$xml .= "\t <RznSoc>IMPORTADORA Y REPUESTOS JORMAT LIMITADA.</RznSoc> \n";
$xml .= "\t <GiroEmis>VENTA AMBULANTE DE REPUESTOS Y ACCESORIOS PARA VEHICULOS</GiroEmis> \n";
$xml .= "\t <Acteco>503000</Acteco> \n";
$xml .= "\t <DirOrigen>VALDIVIA #906</DirOrigen> \n";
$xml .= "\t <CmnaOrigen>LOS ANGELES</CmnaOrigen> \n";
$xml .= "\t <CiudadOrigen>LOS ANGELES</CiudadOrigen> \n";
$xml .= "\t </Emisor> \n";

$clientId=$note["clientId"];
$queryClients = "SELECT DISTINCT
                    id as clientId,
                    dsName as fullName,
                    dsCode as  rut,
                    dsPhoneNumber as phone,
                    dsAddress as address,
                    dsMobileNumber as mobile,
                    dsEmail as  email,
                    dsCity as  cityName,
                    dsDesc as  commercialBusiness,
                    dsUrl as  web,
                    blockingReason as  blockingReason,
                    if(locked=0, 'Activo','Bloqueado') AS isLocked
                    FROM clients where id = '$clientId'";

$clientResult = mysql_query($queryClients);
$client=mysql_fetch_array($clientResult);


$xml .= "\t <Receptor> \n";
$xml .= "\t <RUTRecep>" .$note["rut"] ."</RUTRecep> \n";
$xml .= "\t <RznSocRecep>" .strtoupper($note["clientName"])."</RznSocRecep> \n";
$xml .= "\t <GiroRecep>".strtoupper($client["commercialBusiness"]). "</GiroRecep> \n";
$xml .= "\t <Contacto>".strtoupper($client["phone"]). "</Contacto> \n";
$xml .= "\t <CorreoRecep>" .strtoupper($client["email"])."</CorreoRecep> \n";
$xml .= "\t <DirRecep>" .strtoupper($client["address"]). "</DirRecep> \n";

$xml .= "\t <CmnaRecep>".strtoupper($client["cityName"])."</CmnaRecep> \n";
$xml .= "\t <CiudadRecep>".strtoupper($client["cityName"])."</CiudadRecep> \n";
$xml .= "\t </Receptor> \n";


$xml .= "\t <Totales> \n";						
$xml .= "\t <MntNeto>".$note["netPrice"]."</MntNeto> \n";
$xml .= "\t <TasaIVA>19</TasaIVA> \n";
$xml .= "\t <IVA>".$note["priceVAT"] ."</IVA> \n";
$xml .= "\t <MntTotal>".$note["total"]."</MntTotal> \n";
$xml .= "\t </Totales>\n";
$xml .= "\t </Encabezado> \n";

 
$queryItems="SELECT     itemId,
                        i.dsName AS itemDescription,
                        pi.nmUnities as quantityItems,
                        pi.nmPrice AS price,
                        pi.discount AS discount,
                        pi.total
                FROM creditnotesitems pi
                INNER JOIN items i
                ON  i.id = pi.itemId
                WHERE noteId ='$noteId'";

$itemsList=mysql_query($queryItems);

	for ($i = 0; $i < mysql_num_rows($itemsList); $i++) {
		$num=$i+1;
		$itemId=mysql_result($itemsList,$i,"itemId");
		$itemDescription=mysql_result($itemsList,$i,"itemDescription");
		$quantityItems=mysql_result($itemsList,$i,"quantityItems");
		$price=mysql_result($itemsList,$i,"price");
		$discount=mysql_result($itemsList,$i,"discount");
		$total=mysql_result($itemsList,$i,"total");

		$xml .= "\t <Detalle>\n";
		$xml .= "\t <NroLinDet>" .$num. "</NroLinDet>\n";
		$xml .= "\t <CdgItem>\n";
		$xml .= "\t <TpoCodigo></TpoCodigo>\n";
		$xml .= "\t <VlrCodigo>".$itemId."</VlrCodigo>\n";
		$xml .= "\t </CdgItem>\n";
		$xml .= "\t <NmbItem>" .strtoupper($itemDescription)." COD:(".$itemId.")</NmbItem>\n";
		
		$xml .= "\t <DscItem></DscItem>\n";
		$xml .= "\t <QtyItem>".$quantityItems."</QtyItem>\n";
		$xml .= "\t <PrcItem>".$price."</PrcItem>\n";
		$xml .= "\t <MontoItem>".$total."</MontoItem>\n";
		$xml .= "\t </Detalle>\n";

	}
						
// referencia codigo 33 para indicar codigo factura
$xml .= "<Referencia>";
$xml .= "<NroLinRef>1</NroLinRef>";
$xml .= "<TpoDocRef>33</TpoDocRef>";
$xml .= "<FolioRef>".$note["invoiceId"]."</FolioRef>";
$xml .= "<FchRef>" .$note["date"] ."</FchRef>";
$xml .= "<CodRef>1</CodRef>";
$xml .= "</Referencia>";

$xml .= "\t </Documento> \n";
$xml .= "\t </DTE> \n";

// $fh = fopen("d:/nc".$noteId.".xml", 'w');
$fh = fopen($urlXML."/nc".$noteId.".xml", 'w');
// $fh = fopen("/home/jormat/publicc1/nc".$noteId.".xml", 'w'); 
fwrite($fh,$xml);
fclose($fh); 

?>


